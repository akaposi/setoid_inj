\documentclass{easychair}
%\documentclass[EPiC]{easychair}
%\documentclass[EPiCempty]{easychair}
%\documentclass[debug]{easychair}
%\documentclass[verbose]{easychair}
%\documentclass[notimes]{easychair}
%\documentclass[withtimes]{easychair}
%\documentclass[a4paper]{easychair}
%\documentclass[letterpaper]{easychair}

\usepackage{doc}
\usepackage{amssymb}
\usepackage{cancel}
\usepackage{hyperref}

\usepackage{tikz}
\usetikzlibrary{decorations.pathreplacing}
\usepackage{stmaryrd}

% use this if you have a long article and want to create an index
% \usepackage{makeidx}

% In order to save space or manage large tables or figures in a
% landcape-like text, you can use the rotating and pdflscape
% packages. Uncomment the desired from the below.
%
% \usepackage{rotating}
% \usepackage{pdflscape}

% Some of our commands for this guide.
%

%\makeindex

%% Front Matter
%%
% Regular title as in the article class.
%
\title{Canonicity and decidability of equality for setoid type theory}

% Authors are joined by \and. Their affiliations are given by \inst, which indexes
% into the list defined using \institute
%
\author{
  István Donkó\inst{1}\thanks{Supported by the ÚNKP-21-3 New National
  Excellence Program of the Ministry for Innovation and Technology from the
  source of the National Research, Development and Innovation Fund.}
  \and
  Ambrus Kaposi\inst{1}\thanks{Ambrus Kaposi was supported by the ``Application Domain Specific Highly Reliable IT Solutions''
    project which has been implemented with support from the
    National Research, Development and Innovation Fund of Hungary,
    financed under the Thematic Excellence Programme TKP2020-NKA-06
    (National Challenges Subprogramme) funding scheme, and by Bolyai Scholarship BO/00659/19/3.}
}

% Institutes for affiliations are also joined by \and,
\institute{
  E{\"o}tv{\"o}s Lor{\'a}nd University, Budapest, Hungary\\
  \email{\{isti115,akaposi\}@inf.elte.hu}
 }

%  \authorrunning{} has to be set for the shorter version of the authors' names;
% otherwise a warning will be rendered in the running heads. When processed by
% EasyChair, this command is mandatory: a document without \authorrunning
% will be rejected by EasyChair

\authorrunning{Donkó, Kaposi}

% \titlerunning{} has to be set to either the main title or its shorter
% version for the running heads. When processed by
% EasyChair, this command is mandatory: a document without \titlerunning
% will be rejected by EasyChair
\titlerunning{Canonicity and decidability of equality for setoid type theory}

\begin{document}

\maketitle

\begin{abstract}
A proof assistant based on Setoid Type Theory would be practical for dealing with quotient inductive types. In order for such a system to be implemented in a proof assistant, we have to prove that it has decidability of equality. Our aim is to verify this property by constructing a syntactic translation from the syntax of Martin-Löf Type Theory and showing that this translation is injective.
\end{abstract}

% The table of contents below is added for your convenience. Please do not use
% the table of contents if you are preparing your paper for publication in the
% EPiC Series or Kalpa Publications series

% \setcounter{tocdepth}{2}
% {\small
% \tableofcontents}

%\section{To mention}
%
%Processing in EasyChair - number of pages.
%
%Examples of how EasyChair processes papers. Caveats (replacement of EC
%class, errors).

\newcommand{\blank}{\mathord{\hspace{1pt}\text{--}\hspace{1pt}}} %from the book
\newcommand{\Bool}{\mathsf{Bool}}
\newcommand{\true}{\mathsf{true}}
\newcommand{\Type}{\mathsf{Type}}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\RaK}{\Rightarrow_{\mathsf{K}}}
\newcommand{\RaU}{\Rightarrow\hspace{-0.5em}\mathsf{U}}
\newcommand{\Con}{\mathsf{Con}}
\newcommand{\Sub}{\mathsf{Sub}}
\newcommand{\Ty}{\mathsf{Ty}}
\newcommand{\Tm}{\mathsf{Tm}}
\newcommand{\U}{\mathsf{U}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\p}{\mathsf{p}}
\newcommand{\q}{\mathsf{q}}
\newcommand{\app}{\mathbin{\$}}
\renewcommand{\S}{\mathsf{S}}
\newcommand{\K}{\mathsf{K}}
\newcommand{\Kf}{\mathsf{K}_{\mathsf{f}}}

%------------------------------------------------------------------------------

\section{Motivation}

Type theory has proven to be an indispensable tool for precisely formalizing statements as well as constructing and validating proofs for them. In its current implementations handling some problems is quite cumbersome though. For example dealing with quotient types involves either lots of extra manual work, because the added equalities need to be eliminated manually (so called "transport hell") or the other option is to enable certain language features (e.g. rewrite rules in Agda \cite{agda}) which in turn deteriorate the computational properties of the type theory.

One possible alleviation of this problem could be basing proof assistants on Setoid Type Theory (SeTT, \cite{setoid,DBLP:conf/fossacs/AltenkirchBKSS21}), also called observational type theory (\cite{DBLP:conf/plpv/AltenkirchMS07,DBLP:journals/pacmpl/PujetT22}), which would natively enable the usage of quotient inductive types. SeTT is based on the setoid model where the equality relation for any type can be specified arbitrarily.

\section{Requirements}

For SeTT to be usable for such purposes, it needs to have certain properties such as canonicity and decidable equality which is necessary for type checking. Instead of proving these properties using usual methods such as logical relations \cite{DBLP:journals/pacmpl/PujetT22}, we simplify the procedure using a model construction. We call the model construction \emph{setoidification}. We use the variant of the model construction described in \cite{setoid}. Any model of Martin-Löf type theory with strict propositions (MLTTP) can be turned into a model of SeTT. We aim to transport the necessary properties of the model of MLTTP to its setoidified version. In particular, we want to prove that the interpretation of the syntax of SeTT into the setoidified syntax of MLTTP is injective. Injectivity can also be called completeness: every equality that holds in the setoidification of MLTTP is reflected in the syntax of SeTT.

A term in the setoidified model $t$ is a term in the original model $|t|$ together with a proof that it respect the equivalence relation belonging to the type of the term. We call the projection $|\_|$ the \emph{evaluation} function.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}[scale=1.1]
        \node[fill=black, shape=circle, inner sep=2pt, label=below:$\mathsf{MLTTP-syntax}$] (MLSyn) at (3,0.2) {};
        \node[fill=black, shape=circle, inner sep=2pt, label=above:$\mathsf{SeTT-syntax}$] (SetSyn) at (-3,2.5) {};
        \node[fill=black, shape=circle, inner sep=2pt, label=above:$\mathsf{Setoidify}(\mathsf{MLTTP-syntax})$] (SetML) at (3,2.5) {};
        \draw[->, line width=1pt] (SetSyn) edge node[midway, below] {Interpretation $(\llbracket\_\rrbracket)$} (SetML);
        \draw[->, line width=1pt] (MLSyn) edge[bend right] node[midway, below right] {Setoidification} (SetML);
        \draw[<-, line width=1pt] (MLSyn) edge[bend left] node[midway, below left] {Evaluation $(|\_|)$} (SetML);
        \draw (0.5,2.7) ellipse (5.25cm and 1cm);
        \draw (3,0) ellipse (2cm and 0.5cm);

        \node[draw] at (4.5,4) {Setoid Type Theory};
        \node[draw] at (4.5,-0.9) {Martin-Löf Type Theory};

        \draw [decorate,decoration={brace,amplitude=10pt},xshift=-4pt,yshift=0pt] (1,-1.2) -- (1,0.5)
         node [black,midway,xshift=-2.5cm,text width=4cm,align=right]  {Decidable Equality\\Canonicity};
    \end{tikzpicture}
    \caption{Setoidification illustrated}
    \label{fig:setoidification}
\end{figure}

Let the $\llbracket \_ \rrbracket$ operation be the interpretation from the syntax of SeTT to the setoidification of the syntax of MLTTP. We assume injectivity of the interpretation function, that is, for any $t$, $t'$ terms in SeTT of the same type, $\llbracket t \rrbracket = \llbracket t' \rrbracket \Rightarrow t = t'$.
\begin{itemize}
    \item \textit{Decidability of equality} \\
      Decidability of equality states that either $t = t'$ or $t \neq t'$ holds. Through interpretation and evaluation, $|\llbracket t \rrbracket| = |\llbracket t' \rrbracket|$ is an equality in the base model.
          If we have decidable equality there, we have two cases:
          \begin{itemize}
              \item $|\llbracket t \rrbracket\ = |\llbracket t' \rrbracket|$ implies $\llbracket t \rrbracket = \llbracket t' \rrbracket$ which implies $t = t'$ by injectivity
              \item $|\llbracket t \rrbracket| \neq |\llbracket t' \rrbracket|$ implies $t \neq t'$ by contradiction
          \end{itemize}

    \item \textit{Canonicity} \\
          Canonicity means that every closed term can be equated to one that is only built using the constructors of the given type. For example, the type Bool has two canonical forms in the base model:
          \begin{itemize}
              \item $|\llbracket t \rrbracket| = \mathsf{false}_{\mathsf{MLTTP-syntax}}$ implies $t = \mathsf{false}_{\mathsf{SeTT-syntax}}$ by injectivity
              \item $|\llbracket t \rrbracket| = \mathsf{true}_{\mathsf{MLTTP-syntax}}$ implies $t = \mathsf{true}_{\mathsf{SeTT-syntax}}$ by injectivity
          \end{itemize}
\end{itemize}

We are in the process of proving injectivity of interpretation into
setoidification following the analogous proof of injectivity of
interpretation into termification \cite{DBLP:conf/mpc/KaposiKK19}.

% \cite{DBLP:journals/corr/abs-1904-00827}.

%------------------------------------------------------------------------------

\label{sect:bib}
\bibliographystyle{plain}
%\bibliographystyle{alpha}
%\bibliographystyle{unsrt}
%\bibliographystyle{abbrv}
\bibliography{references}

%------------------------------------------------------------------------------

\end{document}
