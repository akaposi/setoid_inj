all: p.pdf

p.pdf : p.tex abbrevs.tex b.bib
	latexmk -pdf p.tex

clean:
	rm -f *.aux *.bbl *.blg *.fdb_latexmk *.fls *.log *.out *.nav *.snm *.toc *.vtc *.ptb *~ *.pdf
