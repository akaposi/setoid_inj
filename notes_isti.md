setoid modellben igaz a funext. a setoid modell megadhato Agda-ban funext nelkul.

MLTTP: Con,Ty,Sub,Tm, ∙,▷, _[_], ..., Π, Σ, Bool, ⊥, ⊤, es meg:

Ty  : Con → Set                           Set
TyP : Con → Set                           Prop
_[_] : TyP Δ → Sub Γ Δ → TyP Γ
↑   : TyP Γ → Ty Γ                        ↑p
irr : (u v : Tm Γ (↑ A)) → u = v          proof irrelevance refl : {A:Prop}{u v : A} → u ≡ v

↓   : Ty Γ  → TyP Γ                       truncation type former
↓↑  : ↓ (↑ A) = A                         (↑ (↓ A) ≠ A)
mk↓ : Tm Γ A → Tm Γ (↓ A)                 truncation constructor
un↓ : Tm (Γ ▷ A) (↑ B) →                  (A : Set)(B : Prop) → (A → B) → (↓ A) → B
      Tm (Γ ▷ ↑ (↓ A)) (↑ B)
⊥P  : TyP Γ
exfalsoP : Tm (Γ ▷ ↑ ⊥P) A                (⊥P-bol el tudok menni tetszoleges tipusba)

roviditesek:
ΠP : (A : Ty Γ) → TyP (Γ ▷ A) → TyP Γ     ((x : _) → _ x) : (A : Set) → (A → Prop) → Prop
ΠP A B := ↓ (Π A (↑ B))
lamP : Tm (Γ ▷ A) (↑ B) → Tm Γ (↓ (Π A (↑ B)))
lamP t := mk↓ (lam t)
appP : Tm Γ (↓ (Π A (↑ B))) → Tm (Γ ▷ A) (↑ B)
appP t := ? (un↓ segitsegvel megadhato)

ΣP A B := ↓ (Σ (↑ A) (↑ B))

⊤P := ↓ ⊤

⊥P' := ↓ ⊥  (ebbol viszont az exfalsoP' csak Prop-ba tudna eliminalni)


{-
data ↓ (A : Set) where
  mk↓ : A → ↓ A
  irr : {u v : ↓ A} → u ≡ v

f : {B : Prop} → ↓ A → B
f (mk↓ a) := g b   (g : A → B)
f (irr u v) : u ≡ v := refl
-}


SeTT := MLTTP + az alabbi szabalyok:

Id A u v        A~ ? u v         (A~) : (Δ~ σ₀ σ₁) → Tm Γ (A[σ₀]) → Tm Γ (A[σ₁]) → TyP Γ
A~ az heterogen egyenloseg
ap  : (f : A       → B)     → a₀ ≡ a₁  →                f a₀  ≡ f a₁
apd : (f : (x : A) → B x)(e : a₀ ≡ a₁) → transport B e (f a₀) ≡ f a₁
                                                       ^:B a₀   ^:B a₁
apd : (f : (x : A) → B x)(e : a₀ ≡ a₁) → f a₀ ≡ transport B (e ⁻¹) (f a₁)

van egy regebbi fajta heterogen egyenloseg: John Major equality


Kérdés: igaz-e pl. a kanonicitás a setoid típuselméletre?

(t : Tm ∙ Bool) → (t = true) + (t = false)

------------------------------------------------------------

MLTTP, SeTT, SeTT-nek van egy setoid modellje (Agdában formalizálható), DE:

setoidification: MLTTP modell -> SeTT modell
ha setoidifikálod a standard modellt, akkor megkapod a setoid modellt
(le van írva a setoid cikkben "Setoid type theory: a syntactic translation")

------------------------------------------------------------

kanonicitas bizonyitasa: injektivitast bizonyitunk:

MLTTP.I : MLTTP szintaxisa
setoidify MLTTP.I : SeTT modell
(t t' : {SeTT.I}.Tm Γ A)

ha
 ∣ {setoidify MLTTP.I}.⟦ t  ⟧ ∣ : {MLTTP.I}.Tm ∣⟦Γ⟧∣ ∣⟦A⟧∣ =
 ∣ {setoidify MLTTP.I}.⟦ t' ⟧ ∣ : {MLTTP.I}.Tm ∣⟦Γ⟧∣ ∣⟦A⟧∣, akkor t = t'

∣⟦_⟧∣ <- ezen fgv injektivitasat akarom bizonyitani


{setoidify M}.Con    := (∣ Γ ∣ : M.Con)        × (Γ ~ : Sub Ω ∣Γ∣ → Sub Ω ∣Γ∣ → Prop)  × ...
{setoidify M}.Tm Γ A := (∣ t ∣ : M.Tm ∣Γ∣ ∣A∣) × (t~ : Γ~ ρ₀ ρ₁ → A~ (∣t∣[ρ₀]) (∣t∣[ρ₁]))



----------------------------------------


(Γ:Grp) :=
  ∣Γ∣ : Set
  Γ~  : ∣Γ∣ → ∣Γ∣ → Prop                            thin groupoid = setoid;  thin CwF = logika
  RΓ  :                           id
  TΓ  :                           _∘_
  ass :
  idl :
  idr :
  SΓ  :                           _⁻¹
  invl : TΓ (SΓ f) f = RΓ J       (f ∘ f⁻¹) = id_J
  invr : TΓ f (SΓ f) = RΓ I




setoid modell:
--------------
  Con = Setoid

  (A:Ty Γ) =
    ∣A∣ : (∣Γ∣ → S.Con)  ≅                     ∣Γ∣→ X×Y×... ≅ (∣Γ∣→X)×(∣Γ∣→Y)×...
                  ∣A∣ : ∣Γ∣ → Set
                  A~ :  (γ:∣Γ∣) →  ∣A∣γ → ∣A∣γ → Prop
                  R  :  (γ:∣Γ∣)(α:∣A∣γ) → A~ γ α α
                  S  :  (γ:∣Γ∣)(α₀ α₁:∣A∣γ) → A~ γ α₀ α₁ → A~ γ α₁ α₀
                  T
    coA : Γ~ γ₀ γ₁ → S.Sub ∣A∣γ₀ ∣A∣γ₁
    RA  : coA (RΓ γ) = id
    TA  : coA (TΓ γ₀₁ γ₁₂) = coA γ₁₂ ∘ coA γ₀₁

  SΓ γ₀₁ : Γ~ γ₁ γ₀


MLTTPRop
--------

TyP : Con → Set
↑   : TyP Γ → Ty Γ
irr : (u v : Tm Γ (↑ A)) → u = v
TyP[]
↑[]
(↓ : Ty Γ → TyP Γ   -- truncation (inductive type))
rövidítések:
  TmP Γ A := Tm Γ (↑ A)
  _▷P_ : (Γ : Con) → TyP Γ → Con
  Γ ▷P A := Γ ▷ ↑A
Tm : (Γ : Con) → Ty Γ → Set
<!-- Tm : ((Γ▹A) : Con) → Ty (Γ▹A) → Set -->


setoid(ification):
----------------
M MLTTProp modellbol indulunk, es kapunk egy S SeTT-modellt. Ha M=standard, akkor S=setoid modell.

(Γ:S.Con) =
  ∣Γ∣ : M.Con
  Γ~  : M.Sub Ω ∣Γ∣ → M.Sub Ω ∣Γ∣ → Prop
  RΓ  : (ρ : M.Sub Ω ∣Γ∣) → Γ~ ρ ρ
  SΓ  : Γ~ ρ₀ ρ₁ → Γ~ ρ₁ ρ₀
  TΓ  : Γ~ ρ₀ ρ₁ → Γ~ ρ₁ ρ₂ → Γ~ ρ₀ ρ₂
  <!-- Γ~[] : Γ~ ρ₀ ρ₁ → (σ : M.Sub Ω' Ω) → Γ~ (ρ₀∘σ) (ρ₁∘σ) -->
  _[_]Γ : Γ~ ρ₀ ρ₁ → (σ : M.Sub Ω' Ω) → Γ~ (ρ₀∘σ) (ρ₁∘σ)

(σ:S.Sub Γ Δ) :=
  ∣σ∣ : M.Sub ∣Γ∣ ∣Δ∣
  σ~  : Γ~ ρ₀ ρ₁ → Δ~ (∣σ∣∘ρ₀) (∣σ∣∘ρ₁)

(A:S.Ty Γ) =
  ∣A∣ : M.Ty ∣Γ∣
  <!-- A~' : M.Sub Ω |Γ| → M.Ty |Γ| → M.Ty |Γ| → M.TyP ∣Γ∣ -->
  A~  : M.TyP (∣Γ∣▷∣A∣▷∣A∣[p])

      A~ : M.Tm Ω (∣A∣[ρ]) → M.Tm Ω (∣A∣[ρ]) → M.TyP Ω
      A~[] : (A~ {ρ} u₀ u₁)[σ] = A~ {ρ∘σ} (u₀[σ]) (u₁[σ])
                u₀,u₁ : M.Tm Ω (∣A∣[ρ])
                σ : Sub Ω' Ω
  
  RA  : M.TmP (∣Γ∣▷∣A∣) (A~[id,q])

       RA : (u:M.Tm Ω (∣A∣[ρ])) → M.TmP Ω (A~[ρ,u,u])

  SA  : M.TmP (∣Γ∣▷∣A∣▷∣A∣[p]▷A~) (A~[p∘p∘p,q[p],q[p][p]])

       SA : M.TmP Ω (A~[ρ,u₀,u₁]) → M.TmP Ω (A~[ρ,u₁,u₀])

  TA  : M.TmP
          (∣Γ∣ ▷ ∣A∣ ▷ ∣A∣[p] ▷ ∣A∣[p ∘ p] ▷ A~[p] ▷ A~[p⁴, q[p²], q[p]])
          (A~[p⁵ , q[p⁴] , q[p²]])

       TA : M.TmP Ω (A~[ρ,u₀,u₁]) → M.TmP Ω (A~[ρ,u₁,u₂]) → M.TmP Ω (A~[ρ,u₀,u₂])
       TA u₀₁ u₁₂ := TA[ρ,u₀,u₁,u₂,u₀₁,u₁₂]

       T3A : M.TmP Ω (A~[ρ,u₀,u₁]) → M.TmP Ω (A~[ρ,u₁,u₂]) → M.TmP Ω (A~[ρ,u₂,u₃]) → M.TmP Ω (A~[ρ,u₀,u₃])
       T3A u₀₁ u₁₂ u₂₃ := TA u₀₁ (TA u₁₂ u₂₃)

  coeA : Γ~ ρ₀ ρ₁ → M.Tm (Ω▷∣A∣[ρ₀]) (∣A∣[ρ₁∘p])            M.Tm Ω (∣A∣[ρ₀] ⇒ ∣A∣[ρ₁])
                                                                  A ⇒ B := Π A (B[p])

       coeA ρ₀₁ u := coeA ρ₀₁[id,u]

  <!-- coeA' : Γ~ ρ₀ ρ₁ → M.Tm Ω ∣A∣[ρ₀] → M.Tm Ω (∣A∣[ρ₁∘p])  -->

          ρ₀, ρ₁ : M.Sub Ω ∣Γ∣
  coe~A : (ρ₀₁:Γ~ ρ₀ ρ₁) → M.TmP (Ω▷∣A∣[ρ₀]▷∣A∣[ρ₀∘p]▷A~[ρ₀∘p²,q[p],q])
                              (A~[ρ₁∘p³,coeA ρ₀₁[p²],coeA ρ₀₁[p³,q[p]]])

    A~  : M.TyP (γ:∣Γ∣, α₀:∣A∣, α₁:∣A∣[p])
    coe~A : (ρ₀₁:Γ~ ρ₀ ρ₁) → M.TmP (Ω, α₀:∣A∣[ρ₀], α₁: ∣A∣[ρ₀∘p], α₀₁ : A~[ρ₀∘p²,α₀,α₁])
                                (A~[ρ₁∘p³, coeA ρ₀₁ α₀, coeA ρ₀₁ α₁])
                                         A~ α₀ α₁ → A~ (coe α₀) (coe α₁)
     (x₀:Aρ₀) → A~ x₀ (coe ρ₀₁ x₀)

      σ^ := (σ∘p,q)
  coeRA : coeA (RΓ ρ) = q : M.Tm (Ω▷∣A∣[ρ]) (∣A∣[ρ∘p])
          coeA (RΓ ρ) = q         , ha ρ̂ : Γ~ ρ ρ → coeA ρ̂ = q       (ρ̂ = RΓ ρ)

  coeTA : coeA (TΓ ρ₀₁ ρ₁₂) = coeA ρ₁₂[p,coeA ρ₀₁]
                                :  M.Tm (Ω▷∣A∣[ρ₀]) (∣A∣[ρ₂∘p])
                              : M.Tm (Ω▷∣A∣[ρ₀]) (∣A∣[ρ₁∘p])
                              : M.Tm (Ω▷∣A∣[ρ₁]) (∣A∣[ρ₂∘p])

  coeA[] : coeA ρ₀₁ [σ ∘ p , q] ≡ coeA (ρ₀₁[σ])


  (A : Γ ⇒ U) groupoid-morfizmus (Γ-ból a setoidok groupoidjába megy)
  1. A : ∣Γ∣ → ∣U∣
         ∣Γ∣ → Setoid                            A → (B × C) ≅ (A → B) × (A → C)   (b*c)^a = b^a*c^a
    A(γ) : Setoid    A : ∣Γ∣ → Σ(...)
    ∣A(γ)∣ : Set                               ∣A∣ : ∣Γ∣ → Set
    A(γ)~  : ∣A(γ)∣ → ∣A(γ)∣ → Prop            A~  : (γ:∣Γ∣) → ∣A∣γ → ∣A∣γ → Prop
    RA(γ)  : (α:∣A(γ)∣) → A(γ)~ α α            RA  : (γ:∣Γ∣)(α:∣A∣γ) → A~ γ α α
    SA(γ)  : A(γ) α₀ α₁ → A(γ) α₁ α₀           SA
    TA(γ)  :
    A~ :(γ:∣Γ∣) → ∣A∣γ → ∣A∣γ
  2. A : homΓ(γ₀,γ₁) → homU(A(γ₀),A(γ₁))
         Γ~ γ₀ γ₁    → SetoidMor A(γ₀) A(γ₁)
         Γ~ γ₀ γ₁    → (f : ∣A(γ₀)∣ → ∣A(γ₁)∣) ×                  Γ~ γ₀ γ₁ → (f : ∣A∣γ₀ → ∣A∣γ₁) ×
                       (A(γ₀)~ α₀ α₁ → A(γ₁)~ (f(α₀)) (f(α₁)))           A~ γ₀ α₀ α₁ → A~ γ₁ fα₀ fα₁
  3. A id = id
  4. A (f∘g) = A(f)∘A(g)


(t:S.Tm Γ A) :=
  ∣t∣ : M.Tm ∣Γ∣ ∣A∣
  t~  : (ρ₀₁ : Γ~ ρ₀ ρ₁) → TmP Ω (A~[ρ₁,coeA ρ₀₁ (∣t∣[ρ₀]),∣t∣[ρ₁]])

                                        :∣A∣[ρ₁]

                         (∣σ∣∘ρ₀) (∣σ∣∘ρ₁)

 

(A:TyP Γ) :=
  ∣A∣ : M.TyP ∣Γ∣
  coeA : Γ~ ρ₀ ρ₁ → M.TmP (Ω▷∣A∣[ρ₀]) (∣A∣[ρ₁∘p])

setoidification specifikacioja (szortokkal mi lesz)
                implementacioja:

# id : S.Sub Γ Γ
  - ∣id∣    := M.id                  : M.Sub ∣Γ∣ ∣Γ∣
  - id~ ρ₀₁ := ρ₀₁                   : Γ~ ρ₀ ρ₁ → Γ~ (id∘ρ₀) (id∘ρ₁) := λ
  
# _∘_
  <!-- σ∘δ : S.Sub Γ Δ  (σ:S.Sub Γ Θ) (δ:S.Sub Θ Δ) -->
  - ∣σ∘δ∣ := ∣σ∣∘∣δ∣
  - (σ∘δ)~ ρ₀₁ := σ~ (δ~ ρ₀₁)       ρ₀₁ : Γ~ ρ₀ ρ₁, kell: Δ~ (∣σ∣∘(∣δ∣∘ρ₀)) (∣σ∣∘(∣δ∣∘ρ₁))

# ass : (σ ∘ δ) ∘ ν = σ ∘ (δ ∘ ν)
  - (|σ| ∘ |δ|) ∘ |ν| = |σ| ∘ (|δ| ∘ |ν|)
  
# idl : id ∘ σ = σ
  - ∣id ∘ σ∣ = ∣id∣ ∘ ∣σ∣ = id ∘ ∣σ∣ = ∣σ∣

# idr : σ ∘ id = σ
  - |idr| : ∣σ ∘ id∣ = ∣σ∣ ∘ ∣id∣ = ∣σ∣ ∘ id = ∣σ∣
                                             ^-M.idr

# A[σ]                                A : Ty Δ  σ : Sub Γ Δ
  - ∣A[σ]∣ := ∣A∣[∣σ∣]

  <!-- - (A[σ])~ := A~[|σ|^^] :  M.TyP (∣Γ∣▷∣A∣[∣σ∣]▷∣A∣[∣σ∣][p]) -->
  - (A[σ])~ := A~[∣σ∣∘p²,q[p],q] :  M.TyP (∣Γ∣▷∣A∣[∣σ∣]▷∣A∣[∣σ∣][p])
       A~ : M.TyP (∣Δ∣▷∣A∣▷∣A∣[p])
       ∣σ∣ : M.Sub ∣Γ∣ ∣Δ∣
       σ~  : Γ~ ρ₀ ρ₁ → Δ~ (∣σ∣∘ρ₀) (∣σ∣∘ρ₁)

  - R(A[σ]) := RA[∣σ∣∘p,q]     :  M.TmP (∣Γ∣▷∣A∣[∣σ∣]) (A~[∣σ∣∘p,q,q])
                kell : ∣A∣[∣σ∣∘p]         q : M.TmP (∣Γ∣▷∣A∣[∣σ∣]) (∣A∣[∣σ∣][p])
            RA : M.TmP (∣Δ∣▷∣A∣) (A~[id,q])                    A~[id,q][∣σ∣∘p,q]

  - S(A[σ]) := SA[|σ|∘p³,q[p²],q[p],q]
       : M.TmP (∣Γ∣ ▷ ∣A[σ]∣ ▷ ∣A[σ]∣[p] ▷ A[σ]~) (A[σ]~[p∘p∘p , q[p] , q[p][p]])

    <!-- SA : M.TmP (∣Δ∣ ▷ ∣A∣ ▷ ∣A∣[p] ▷ A~) (A~[p∘p∘p , q[p] , q[p][p]]) -->

  - T(A[σ]) := TA[|σ|^^^^^]
      : M.TmP
          (∣Γ∣ ▷ ∣A[σ]∣ ▷ ∣A[σ]∣[p] ▷ ∣A[σ]∣[p ∘ p] ▷ A[σ]~[p] ▷ A[σ]~[p])
          (A[σ]~[p⁵ , q[p⁴] , q[p²]])

    <!-- TA  : M.TmP -->
    <!--         (|Δ∣ ▷ ∣A∣ ▷ ∣A∣[p] ▷ ∣A∣[p ∘ p] ▷ A~[p] ▷ A~[p⁵, q[p²], q[p]]) -->
    <!--         (A~[p⁵ , q[p⁴] , q[p²]]) -->

  - coe(A[σ]) ρ₀₁ := coeA (σ~ ρ₀₁) : M.Tm (Ω▷∣A∣[∣σ∣∘ρ₀]) (∣A∣[∣σ∣∘ρ₁∘p])
                coeA ν₀₁: M.Tm (Ω▷∣A∣[ν₀]) (∣A∣[ν₁∘p])
                     ν₀₁ : Δ~ ν₀ ν₁
                     ρ₀₁ : Γ~ ρ₀ ρ₁
                     σ~ ρ₀₁ : Δ~ (∣σ∣∘ρ₀) (∣σ∣∘ρ₁)
  - coe~(A[σ]) ρ₀₁ := coe~A (σ~ ρ₀₁) :
    M.TmP
      (Ω▷∣A∣[∣σ∣∘ρ₀]▷∣A∣[∣σ∣∘ρ₀∘p]▷A~[∣σ∣∘ρ₀∘p²,q[p],q])
      (A~[∣σ∣∘ρ₁∘p³,coeA (σ~ ρ₀₁)[p²],coeA (σ~ ρ₀₁)[p³,q[p]]])
      
  - coeR(A[σ]) : coe(A[σ]) (RΓ ρ) =  coeA (σ~ (RΓ ρ))          = coeA (RΔ (∣σ∣∘ρ)) = q
                                        ^ : Δ~ (∣σ∣∘ρ) (∣σ∣∘ρ)      ^ : Δ~ (∣σ∣∘ρ) (∣σ∣∘ρ)

                                  ρ : Sub Ω ∣Γ∣
    coeRA : coeA (RΔ ν) = q       ν : Sub Ω ∣Δ∣

  - coeT(A[σ]) :
      coe(A[σ]) (TΓ ρ₀₁ ρ₁₂)          =
      coeA (σ~ (TΓ ρ₀₁ ρ₁₂))          = -- Prop
      coeA (TΔ (σ~ ρ₀₁) (σ~ ρ₁₂))     = -- coeTA
      coeA (σ~ ρ₁₂) [p,coeA (σ~ ρ₀₁)] = 
      coe(A[σ]) ρ₁₂ [p,coe(A[σ]) ρ₀₁]
      
  - coe(A[δ])[] :
      coe(A[δ]) ρ₀₁ [σ ∘ p , q] ≡ 
      coeA (δ~ ρ₀₁) [σ ∘ p , q] ≡⟨ coeA[] ⟩
      coeA ((δ~ ρ₀₁)[σ]) ≡
      coeA (δ~ (ρ₀₁[σ])) ≡
      coe(A[δ]) (ρ₀₁[σ])

# A[σ] : TyP Γ   σ:Sub Γ Δ, A:TyP Δ
  - ∣A[σ]∣ := ∣A∣[∣σ∣]
  - coe(A[σ]) ρ₀₁ := coeA (σ~ ρ₀₁)

# t[σ]
  - ∣t[σ]∣ := ∣t∣[∣σ∣]
  - (t[σ])~ ρ₀₁ := t~ (σ~ ρ₀₁) : TmP Ω (A~[∣σ∣∘ρ₁,coeA (σ~ ρ₀₁) (∣t∣[∣σ∣][ρ₀]),∣t∣[∣σ∣][ρ₁]])
                   t~ (σ~ ρ₀₁) : TmP Ω (A~[∣σ∣∘ρ₁,coeA (σ~ ρ₀₁) (∣t∣[∣σ∣∘ρ₀]),∣t∣[∣σ∣∘ρ₁]])

# [id] :
  - [id]Ty  : 
    + |[id]Ty| : A[id] = |A|[|id|] = |A|[id] = |A| = A
    + [id]Ty~ : A[id]~ = A~
    + coe([id]Ty) : coe(A[id]) ρ₀₁ = coeA (id~ ρ₀₁) = coeA ρ₀₁

      <!-- -- coeA [id]Ty) ρ₀₁ : -->
      <!-- -- Γ~ ρ₀ ρ₁ → M.Tm (Ω▷∣A∣[ρ₀]) (∣A∣[ρ₁∘p]) -->

  - [id]TyP :
      A[id] = |A|[|id|] = |A|[id] = |A| = A

  - [id]Tm  :
      t[id] = |t|[|id|] = |t|[id] = |t| = t

# [∘] :                                                                  |---∣σ∘δ∣
  - [∘]Ty                                                                v
    + |[∘]Ty|    : A [ σ ] [ δ ] = |A| [ |σ| ] [ |δ| ] = A [ |σ| ∘ |δ| ] = A [ |σ ∘ δ| ] = A [ σ ∘ δ ]
    + [∘]Ty~     :
        A[σ][δ]~                                                  =
        A[σ]~[∣δ∣∘p²,q[p],q]                                      =
        A~[∣σ∣∘p²,q[p],q][∣δ∣∘p²,q[p],q]                          =
        A~[(∣σ∣∘p²,q[p],q) ∘ (∣δ∣∘p²,q[p],q)]                     = 
        A~[(∣σ∣∘p²,q[p]) ∘ (∣δ∣∘p²,q[p],q) , q [ ∣δ∣∘p²,q[p],q ]] = 
        A~[(∣σ∣∘p² ∘ (∣δ∣∘p²,q[p],q) , q[p][∣δ∣∘p²,q[p],q] , q]   = 
        A~[(∣σ∣∘∣δ∣∘p², q[p∘(∣δ∣∘p²,q[p],q)] , q]                 = 
        A~[(∣σ∣∘∣δ∣∘p², q[∣δ∣∘p²,q[p]] , q]                       = 
        A~[(∣σ∣∘∣δ∣∘p², q[p] , q]                                 = 
        A~[(∣σ∘δ∣∘p², q[p] , q]                                   = 
        A[σ∘δ]~
    + coe([∘]Ty) : coe(A[σ ∘ δ]) ρ₀₁ = coeA ((σ ∘ δ)~ ρ₀₁) = coeA ρ₀₁
  
  - [∘]Typ :
    + |[∘]Typ| : A [ σ ] [ δ ] = |A| [ |σ| ] [ |δ| ] = A [ |σ| ∘ |δ| ] = A [ |σ ∘ δ| ] = A [ σ ∘ δ ]
  
  - [∘]Tm :
    + |[∘]Tm| : t [ σ ] [ δ ] = |t| [ |σ| ] [ |δ| ] = |t| [ |σ| ∘ |δ| ] = t [ σ ∘ δ ]

# ∙
  ∣∙∣ : M.Con := M.∙
  ∙~ σ₀ σ₁ := ⊤             : M.Sub Ω ∙ → M.Sub Ω ∙ → Prop
  R∙ ρ := tt
  S    := tt
  T    := tt
  ρ₀₁[σ]∙ := tt

# S.ε :
  |S.ε| = M.ε : M.Sub |Γ| M.∙
  (S.ε)~ = const tt : Γ~ ρ₀ ρ₁ → ∙~ (|ε∣∘ρ₀) (∣ε∣∘ρ₁)

# S.∙η : ∀{Γ} → σ : Sub Γ ∙ → σ ≡ ε
  |∙η| = M.∙η : |σ| = M.ε = |ε|
                    ^     ^ 
                  M.∙η   refl
             
  ∙η~ : σ~ ≡ ε~
        σ~ : Γ~ ρ₀ ρ₁ → (∙~ (∣σ∣∘ρ₀) (∣σ∣∘ρ₁) : Prop) 

# _▷_
  - ∣Γ▷A∣ : M.Con := ∣Γ∣▷∣A∣
  
  - (Γ▷A)~ (ρ₀,t₀) (ρ₁,t₁) := (ρ₀₁ : Γ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,(coeA ρ₀₁)[id,t₀],t₁])
             ρ₀, ρ₁ : Sub Ω Γ
             t₀ : Tm Ω |A|[ρ₀]
             t₁ : Tm Ω |A|[ρ₁]
             coeA ρ₀₁ : M.Tm (Ω▷∣A∣[ρ₀]) (∣A∣[ρ₁∘p])
             [id, t₀] : Sub Ω (Ω▷∣A∣[ρ₀])
             coeA ρ₀₁ [id , t₀] : M.Tm Ω (∣A∣[ρ₁])
             _,_ : (σ : Sub Γ Δ) → Tm Γ A[σ] → Sub Γ Δ▹A
             ρ₁,coeA ρ₀₁ [id , t₀] : Sub Ω (Γ▹∣A∣)
  
  - R(Γ▷A) : ((ρ,t): M.Sub Ω ∣Γ▹A∣) → (Γ▹A)~ (ρ,t) (ρ,t) 
    R(Γ▷A) (ρ,t) := (RΓ ρ, RA[ρ,t]) 
                         ^ : M.TmP Ω (A~[ρ,t,t])
                         
  - S(Γ▷A) : (Γ▹A)~ (ρ₀, t₀) (ρ₁, t₁) → (Γ▹A)~ (ρ₁, t₁) (ρ₀, t₀)
             (ρ₀₁ : Γ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,(coeA ρ₀₁)[id,t₀],t₁]) →
             (ρ₁₀ : Γ~ ρ₁ ρ₀) × M.TmP Ω (A~[ρ₀,(coeA ρ₁₀)[id,t₁],t₀])
    S(Γ▷A) (ρ₀₁, t₀₁) = (SΓ ρ₀₁ , SA[ρ₀,t₀,(coeA ρ₁₀)[id,t₁],l2r t₀₁])
   
    σ : Sub Ω (∣Γ∣▷∣A∣▷∣A∣[p]▷A~)
    (A~[p∘p∘p,q[p],q[p][p]])[σ] = (A~[ρ₀,(coeA ρ₁₀)[id,t₁],t₀]) 
    (p∘p∘p,q[p],q[p][p])∘σ = ρ₀,(coeA (SΓ ρ₀₁))[id,t₁],t₀
    σ = (ρ₀,t₀,(coeA ρ₁₀)[id,t₁],l2r ρ₀₁ t₀₁)
    
    t? : TmP Ω A~[ρ₀,t₀,(coeA (SΓ ρ₀₁))[id,t₁]] 
    t? = l2r ρ₀₁ t₀₁ 
    
      <!--
        (coeA ρ₀₁) : Tm (Ω▹|A|[ρ₀]) (|A|[ρ₁∘p])
        (coeA ρ₀₁)[id, t₀] :
        σ : Sub ? (Ω▹|A|[ρ₀])
      -->
    + l2r : (ρ₀₁ : Γ~ ρ₀ ρ₁) → TmP Ω (A~[ρ₁,(coeA ρ₀₁)[id, t₀],t₁]) → TmP Ω (A~[ρ₀,t₀,coeA (SΓ ρ₀₁) [id, t₁]])
    + l2r ρ₀₁ t₀₁ = coe~A (SΓ ρ₀₁) [id,(coeA ρ₀₁)[id, t₀],t₁,t₀₁] :
        : ( A~[ρ₀∘p³,coeA ρ₁₀[p²],coeA ρ₁₀[p³,q[p]]]  [id,(coeA ρ₀₁)[id, t₀],t₁,t₀₁])
        : (A~[(ρ₀∘p³,coeA ρ₁₀[p²],coeA ρ₁₀[p³,q[p]) ∘ (id,(coeA ρ₀₁)[id, t₀],t₁,t₀₁)])
        : (A~[ρ₀,coeA ρ₁₀[id,(coeA ρ₀₁)[id, t₀]],coeA ρ₁₀[id,t₁]]
        : (A~[ρ₀,coeA ρ₁₀[p,coeA ρ₀₁][id, t₀],coeA ρ₁₀[id,t₁]]
        <!-- : (A~[ρ₀,coeA ρ₁₀[id,(coeA ρ₀₁)[(id ∘ p , q) ∘ (id , t₀)]],coeA ρ₁₀[id,t₁]]  <\!-- ASK: id vs p -\-> -->
        : (A~[ρ₀,coeA ρ₁₀[id,(coeA ρ₀₁)[p, t₀]],coeA ρ₁₀[id,t₁]]
          (A~[ρ₀,coeA (TΓ ρ₀₁ ρ₁₀) [id, t₀]],coeA ρ₁₀[id,t₁]]
          <!--
            coeA ρ₀₁ : Tm (Ω▹|A|[ρ₀]) (|A|[ρ₁∘p])
            coeA ρ₁₂ : Tm (Ω▹|A|[ρ₁]) (|A|[ρ₂∘p])
            ?        : Tm (Ω▹|A|[ρ₀]) (|A|[ρ₂∘p])

            ? = coeA ρ₁₂[σ]
                        σ : Sub (Ω▹|A|[ρ₀]) (Ω▹|A|[ρ₁])
                        σ = (p, ?)
                                ? : Tm (Ω▹|A|[ρ₀]) |A|[ρ₁∘p]
                                ? = 
                                    coeA ρ₀₁ : (|A|[ρ₁∘p])

            coeA ρ₁₂[?] : Tm (Ω▹|A|[ρ₀]) (|A|[ρ₂∘p])
            (coeA ρ₁₂)[σ] => σ : Sub (Ω▹|A|[ρ₀]) (Ω▹|A|[ρ₁])
                            σ = 
  
  
  coeTA : coeA (TΓ ρ₀₁ ρ₁₂) = coeA ρ₁₂[p,coeA ρ₀₁]
                                :  M.Tm (Ω▷∣A∣[ρ₀]) (∣A∣[ρ₂∘p])
                                
                                
          -->



      * (id,(coeA ρ₀₁)[id, t₀],t₁,t₀₁) : Sub Ω (Ω▹|A|[ρ₁]▹|A|[ρ₁∘p]▹(A~[ρ₁,(coeA ρ₀₁)[id, t₀],t₁][p^2])) <!-- ?? -->
    
      * (coe~A (SΓ ρ₀₁)) : M.TmP (Ω▷∣A∣[ρ₁]▷∣A∣[ρ₁∘p]▷A~[ρ₁∘p²,q[p],q]) (A~[ρ₀∘p³,coeA ρ₁₀[p²],coeA ρ₁₀[p³,q[p]]])
    <!-- r2l : Tm Ω (A~[ρ₀,t₀,coeA ρ₁₀ [id, t₁]]) → (A~[ρ₁,coeA ρ₀₁[id, t₀],t₁]) -->
    
    
    
    M.TmP (∣Γ∣▷∣A∣▷∣A∣[p]▷A~) (A~[p∘p∘p,q[p],q[p][p]])
    M.TmP Ω                   (A~[ρ₀,(coeA (SΓ ρ₀₁))[id,t₁],t₀]) 
   
   
               coeTA
  - T(Γ▷A) : (Γ▹A)~ (ρ₀, t₀) (ρ₁, t₁) → (Γ▹A)~ (ρ₁, t₁) (ρ₂, t₂) → (Γ▹A)~ (ρ₀, t₀) (ρ₂, t₂)
             (ρ₀₁ : Γ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,(coeA ρ₀₁)[id,t₀],t₁]) →
             (ρ₁₂ : Γ~ ρ₁ ρ₂) × M.TmP Ω (A~[ρ₂,(coeA ρ₁₂)[id,t₁],t₂]) →
             <!-- (ρ₀₂ : Γ~ ρ₀ ρ₂) × M.TmP Ω (A~[ρ₂,(coeA ρ₀₂)[id,t₀],t₂]) -->
             (ρ₀₂ : Γ~ ρ₀ ρ₂) × M.TmP Ω (A~[ρ₂,(coeA (TΓ ρ₀₁ ρ₁₂))[id,t₀],t₂])
             (ρ₀₂ : Γ~ ρ₀ ρ₂) × M.TmP Ω (A~[ρ₂,(coeA ρ₁₂[p,coeA ρ₀₁])[id,t₀],t₂])
             (ρ₀₂ : Γ~ ρ₀ ρ₂) × M.TmP Ω (A~[ρ₂,(coeA ρ₁₂[id,coeA ρ₀₁[id,t₀]),t₂])
    T(Γ▷A) (ρ₀₁, t₀₁) (ρ₁₂, t₁₂) = (TΓ ρ₀₁ ρ₁₂ , TA[?, t₀, (coeA ρ₁₂)[id,t₁], t₂, (l2r t₀₁)]
    T(Γ▷A) (ρ₀₁, t₀₁) (ρ₁₂, t₁₂) = (TΓ ρ₀₁ ρ₁₂ , t₁₂[?? SA[?, t₀₁] ??])
    
  <!-- - (Γ▷A)~[] : (Γ▹A)~ (ρ₀, t₀) (ρ₁, t₁) → (σ : M.Sub Ω' Ω) → (Γ▹A)~ ((ρ₀, t₀)∘σ) ((ρ₁, t₁)∘σ) -->
  <!--              (Γ▹A)~ (ρ₀, t₀) (ρ₁, t₁) → (σ : M.Sub Ω' Ω) → (Γ▹A)~ (ρ₀∘σ, t₀[σ]) (ρ₁∘σ, t₁[σ]) -->
  <!--              (ρ₀₁ : Γ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,(coeA ρ₀₁)[id,t₀],t₁]) → -->
  <!--              (σ : M.Sub Ω' Ω) → -->
  <!--              (ρ₀₁ : Γ~ (ρ₀∘σ) (ρ₁∘σ) × M.TmP Ω (A~[(ρ₁∘σ),(coeA ρ₀₁)[id,t₀[σ]],t₁[σ]]) → -->
               
  <!--              <\!-- M.TmP Ω (A~[ ρ₁,   (coeA ρ₀₁)[id,t₀],   t₁]) → -\-> -->
  <!--              <\!-- M.TmP Ω (A~[(ρ₁∘σ),(coeA ρ₀₁)[id,t₀[σ]],t₁[σ]]) → -\-> -->
  <!--   (Γ▷A)~[] (ρ₀₁, t₀₁) σ = (ρ₀₁ [ σ ], (coeA ρ₀₁)[ σ ]) -->
    
  - _[_](Γ▹A) : (Γ▹A)~ (ρ₀, t₀) (ρ₁, t₁) → (σ : M.Sub Ω' Ω) → (Γ▹A)~ ((ρ₀, t₀)∘σ) ((ρ₁, t₁)∘σ)
                (Γ▹A)~ (ρ₀, t₀) (ρ₁, t₁) → (σ : M.Sub Ω' Ω) → (Γ▹A)~ (ρ₀∘σ, t₀[σ]) (ρ₁∘σ, t₁[σ])
                (ρ₀₁ : Γ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,(coeA ρ₀₁)[id,t₀],t₁]) →
                (σ : M.Sub Ω' Ω) →
                (ρ₀₁ : Γ~ (ρ₀∘σ) (ρ₁∘σ) × M.TmP Ω (A~[(ρ₁∘σ),(coeA ρ₀₁)[id,t₀[σ]],t₁[σ]]) →
               
               <!-- M.TmP Ω (A~[ ρ₁,   (coeA ρ₀₁)[id,t₀],   t₁]) → -->
               <!-- M.TmP Ω (A~[(ρ₁∘σ),(coeA ρ₀₁)[id,t₀[σ]],t₁[σ]]) → -->
    (ρ₀₁, t₀₁)[σ](Γ▹A) = (ρ₀₁ [ σ ], (coeA ρ₀₁)[ σ ])

# _S.,_ :
  - |σ S., t| = |σ| M., |t|
  - (σ,t)~ : Γ~ ρ₀ ρ₁ → (Δ ▹ A)~ (∣σ,t∣∘ρ₀) (∣σ,t∣∘ρ₁)
             Γ~ ρ₀ ρ₁ → (Δ ▹ A)~ ((∣σ| M., |t∣)∘ρ₀) ((∣σ| M., |t∣)∘ρ₁)
             Γ~ ρ₀ ρ₁ → (Δ ▹ A)~ ((∣σ|∘ρ₀, |t∣[ρ₀]) ((∣σ|∘ρ₁, |t∣[ρ₁])
             Γ~ ρ₀ ρ₁ → (ρ₀₁ : Δ~ ∣σ|∘ρ₀ ∣σ|∘ρ₁) × M.TmP Ω (A~[∣σ|∘ρ₁,coeA ρ₀₁[id,|t∣[ρ₀]],|t∣[ρ₁]]) 
    (σ,t)~ ρ₀₁ = ( σ~ ρ₀₁ , t~ ρ₀₁ )
  
# p :
  - |p| : M.p
  - p~  : (Δ ▹ A)~ (ρ₀, t₀) (ρ₁, t₁) → Δ~ (∣p∣∘(ρ₀, t₀)) (∣p∣∘(ρ₁, t₁)) 
          (ρ₀₁ : Δ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,coeA ρ₀₁[id,t₀],t₁]) → Δ~ (M.p∘(ρ₀, t₀)) (M.p∘(ρ₁, t₁)) 
          (ρ₀₁ : Δ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,coeA ρ₀₁[id,t₀],t₁]) → Δ~ ρ₀ ρ₁ 
    <!-- p~ = π₁ -- proj₁ -->
    p~ (ρ₀₁ , t₀₁) = ρ₀₁
  
# q : Tm (Δ ▹ A) A[p] :
  - |q| : M.q
  - q~  : (ρ₀₁ : (Δ ▹ A)~ (ρ₀, t₀) (ρ₁, t₁) → TmP Ω (A[p]~[(ρ₁, t₁),coeA ρ₀₁ (∣q∣[(ρ₀, t₀)]),∣q∣[(ρ₁, t₁)]])
          (ρ₀₁ : Δ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,coeA ρ₀₁[id,t₀],t₁]) → TmP Ω (A[p]~[(ρ₁, t₁),coeA ρ₀₁ (M.q[(ρ₀, t₀)]),M.q[(ρ₁, t₁)]])
                                                                 → TmP Ω (A[p]~[(ρ₁, t₁),coeA ρ₀₁ t₀,t₁])
                                                                 → TmP Ω (A~[∣p∣∘p²,q[p],q][(ρ₁, t₁),coeA ρ₀₁ t₀,t₁])
                                                                 → TmP Ω (A~[M.p∘p²,q[p],q][(ρ₁, t₁),coeA ρ₀₁ t₀,t₁])
                                                                 → TmP Ω (A~[p³,q[p],q][(ρ₁, t₁),coeA ρ₀₁ t₀,t₁])
                                                                 → TmP Ω (A~[(p³,q[p],q) ∘ ((ρ₁, t₁),coeA ρ₀₁ t₀,t₁)])
          (ρ₀₁ : Δ~ ρ₀ ρ₁) × M.TmP Ω (A~[ρ₁,coeA ρ₀₁[id,t₀],t₁]) → TmP Ω (A~[(ρ₁,coeA ρ₀₁ t₀,t₁)])
    q~ (ρ₀₁ , t₀₁) = t₀₁


# ▷β₁ : p ∘ (σ, t) = σ
  - |p ∘ (σ, t)| = |p| ∘ |(σ, t)| = M.p ∘ (|σ| , |t|) = |σ|
# ▷β₂ : q[σ, t] = t
  - ∣q[σ, t]∣ = ∣q|[|σ, t∣] = M.q[|σ|, |t∣] = ∣t∣
# ▷η : (p, q) = id 
  - |(p, q)| = |p|, |q| = (p, q) = id = |id| 

# eddig a CwF, jön: injektivitás
--- 







