{-# OPTIONS --prop --type-in-type #-}
-- {-# OPTIONS --prop #-}

open import Agda.Primitive
open import Relation.Binary.PropositionalEquality renaming (subst to Lsubst ; refl to Lrefl)
open import Data.Unit renaming (⊤ to L⊤ ; tt to Ltt)
open import Data.Bool renaming (Bool to LBool ; true to Ltrue ; false to Lfalse)
open import Data.Product renaming (Σ to LΣ ; _,_ to _L,_ ; proj₁ to Lproj₁ ; proj₂ to Lproj₂ )

module SetoidHomInlined where

module Lib where

  -- variable
  --   i j k l : Level

  record ↑_ (P : Prop) : Set where
    constructor ↑[_]↑
    field
      ↓[_]↓ : P
  open ↑_ public

  infix 4 _≡ₚ_
  data _≡ₚ_ {A : Set} (x : A) : A → Prop where
    instance Lreflₚ : x ≡ₚ x

  -- record ⊤ : Set where
  --   constructor tt

  record ⊤ₚ : Prop where
    constructor ttₚ

  -- const : {A B : Set i} → A → B → A
  -- const a b = a

  -- infixr 4 _,ₚ_

  record Σₛₚ (A : Set) (B : A → Prop) : Set where
    constructor _,ₛₚ_
    field
      proj₁ₛₚ : A
      proj₂ₛₚ : B proj₁ₛₚ
  open Σₛₚ public

  record Σₚ (A : Prop) (B : A → Prop) : Prop where
    constructor _,ₚ_
    field
      proj₁ₚ : A
      proj₂ₚ : B proj₁ₚ
  open Σₚ public

open Lib renaming
  ( Σₚ to LΣₚ
  ; _,ₚ_ to _L,ₚ_

  ; Σₛₚ to LΣₛₚ
  ; _,ₛₚ_ to _L,ₛₚ_

  ; ⊤ₚ to L⊤ₚ
  ; ttₚ to Lttₚ
  )

{- Category -}

record Con : Set where
  field
    carrier : Set
    equality : carrier → carrier → Prop
    reflexivity : {c : carrier} → equality c c
    symmetry : {c₀ c₁ : carrier} → equality c₀ c₁ → equality c₁ c₀
    transitivity : {c₀ c₁ c₂ : carrier} → equality c₀ c₁ → equality c₁ c₂ → equality c₀ c₂
  -- syntax equality Γ γ₀ γ₁ = γ₀ ~[ Γ ]~  γ₁
open Con renaming
  ( carrier to ¦_¦
  ; equality to infix 5 [_]_~_
  ; reflexivity to [_]⟳
  ; symmetry to [_]↔_
  ; transitivity to [_]_»_
  )

variable
  Γ Δ Θ Ω : Con

record Sub (Γ : Con) (Δ : Con) : Set where
  field
    carrier : ¦ Γ ¦ → ¦ Δ ¦
    preservation : {γ₀ γ₁ : ¦ Γ ¦} → [ Γ ] γ₀ ~ γ₁ → [ Δ ] (carrier γ₀) ~ (carrier γ₁)
open Sub renaming
  ( carrier to ¦_¦
  ; preservation to infix 5 [_]≈_
  )

variable
  σ δ ν : Sub Γ Δ

_∘_ : Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
¦ σ ∘ δ ¦ γ      = ¦ σ ¦ (¦ δ ¦ γ)
[ σ ∘ δ ]≈ γ₀~γ₁ = [ σ ]≈ ([ δ ]≈ γ₀~γ₁)

id : Sub Γ Γ
¦ id ¦ γ      = γ
[ id ]≈ γ₀~γ₁ = γ₀~γ₁

ass : (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = Lrefl

{- Terminal Object -}

∙ : Con
¦ ∙ ¦       = L⊤
[ ∙ ] _ ~ _ = L⊤ₚ
[ ∙ ]⟳      = Lttₚ
[ ∙ ]↔ _    = Lttₚ
[ ∙ ] _ » _ = Lttₚ

ε : Sub Γ ∙
¦ ε ¦ _  = Ltt
[ ε ]≈ _ = Lttₚ

∙η : {σ : Sub Γ ∙} → σ ≡ ε
∙η = Lrefl

{- Families -}

-- Ty variations:
-- + homogeneous / heterogeneous
-- + coe0, coe1 / symmetry, transport
-- + coeRefl?, coeTrans? (strict _fibrancy_) (= / ~)
-- + TyP + TmP / U(P) + El

record Ty (Γ : Con) : Set where
  field
    carrier : ¦ Γ ¦ → Set
    equality : {γ : ¦ Γ ¦} → carrier γ → carrier γ → Prop
    reflexivity : {γ : ¦ Γ ¦} → {c : carrier γ} → equality c c
    symmetry : {γ : ¦ Γ ¦} → {c₀ c₁ : carrier γ} → equality c₀ c₁ → equality c₁ c₀
    transitivity : {γ : ¦ Γ ¦} → {c₀ c₁ c₂ : carrier γ} → equality c₀ c₁ → equality c₁ c₂ → equality c₀ c₂
    coercion : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (carrier γ₀) → (carrier γ₁)
    -- coherence : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) →
    --             {c₀ : carrier γ₀} {c₁ : carrier γ₁} → equality (coercion γ₀~γ₁ c₀) c₁
    preservation : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → {c₀ c₁ : carrier γ₀} →
                   equality c₀ c₁ → equality (coercion γ₀~γ₁ c₀) (coercion γ₀~γ₁ c₁)
    coercionReflexivity : {γ : ¦ Γ ¦} → {c : carrier γ} → equality (coercion ([ Γ ]⟳) c) c
    -- coercionSymmetry : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → {c₀ c₁ : carrier γ₀} →
    --                    equality (coercion () c) c
    coercionTransitivity : {γ₀ γ₁ γ₂ : ¦ Γ ¦} → {γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁} {γ₁~γ₂ : [ Γ ] γ₁ ~ γ₂} {c₀ : carrier γ₀} →
                           equality (coercion ([ Γ ] γ₀~γ₁ » γ₁~γ₂) c₀) (coercion γ₁~γ₂ (coercion γ₀~γ₁ c₀))
    -- coeTrans~
    -- reflexivity : (γ : ¦ Γ ¦) → coercion {γ₀ = γ} ([ Γ ]⟳) ≡ id
    -- transitivity : {γ₀ γ₁ γ₂ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (γ₁~γ₂ : [ Γ ] γ₁ ~ γ₂) →
    --                coercion ([ Γ ] γ₀~γ₁ » γ₁~γ₂) ≡ coercion γ₁~γ₂ ∘ coercion γ₀~γ₁
    -- coherence : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (c₀ : carrier γ₀) → equality c₀ (coercion γ₀~γ₁ c₀)
open Ty renaming
  ( carrier to ¦_¦
  ; equality to infix 5 [_]_~_
  ; reflexivity to [_]⟳
  ; symmetry to [_]↔_
  ; transitivity to [_]_»_
  ; coercion to [_]↷_⇒_
  ; preservation to [_]≈_⇒_
  ; coercionReflexivity to [_]↷⟳
  ; coercionTransitivity to [_]↷»
  )

variable
  A : Ty Γ


-- TODO: Derivable
CoercionSymmetry : Prop
CoercionSymmetry = {Γ : Con} → (A : Ty Γ) →
                   {γ₀ γ₁ : ¦ Γ ¦} → {γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁} → {a₀ : ¦ A ¦ γ₀} →
                   [ A ] ([ A ]↷ ([ Γ ]↔ γ₀~γ₁) ⇒ ([ A ]↷ γ₀~γ₁ ⇒ a₀)) ~ a₀

coercionSymmetry : CoercionSymmetry
coercionSymmetry {Γ = Γ} A {γ₀~γ₁} {a₀ = a₀} =
  [ A ] ([ A ]↔ [ A ]↷»)   »   ([ A ] ([ A ]≈ [ Γ ]⟳ ⇒ [ A ]⟳)   »   [ A ]↷⟳)

[_]↷↔ : CoercionSymmetry
[_]↷↔ = coercionSymmetry

-- [_]↷↔» : [ A ] ([ A ]↷ a~₀ ([ A ]↷ a~₁ a)) ~ ([ A ]↷ a~₁ ([ A ]↷ a~₀ a))
-- [_]↷↔» = ([ A ] ([ A ]↔ [ A ]↷») » [ A ]↷»)

record Tm (Γ : Con) (A : Ty Γ) : Set where
  field
    carrier : (γ : ¦ Γ ¦) → ¦ A ¦ γ
    -- preservation : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → [ ¦ A ¦ γ₁ ] (¦ [ A ]↷ γ₀~γ₁ (carrier γ₀)) ~ (carrier γ₁)
    preservation : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → [ A ] ([ A ]↷ γ₀~γ₁ ⇒ carrier γ₀) ~ (carrier γ₁)
open Tm renaming
  ( carrier to ¦_¦
  ; preservation to infix 5 [_]≈_
  )

variable
  a : Tm Γ A

_[_]T : {Γ Δ : Con} → Ty Δ → Sub Γ Δ → Ty Γ
¦ A [ σ ]T ¦ γ = ¦ A ¦ (¦ σ ¦ γ)
[ A [ σ ]T ] a₀ ~ a₁ = [ A ] a₀ ~ a₁
[ A [ σ ]T ]⟳ = [ A ]⟳
[ A [ σ ]T ]↔ a₀~a₁ = [ A ]↔ a₀~a₁
[ A [ σ ]T ] a₀~a₁ » a₁~a₂ = [ A ] a₀~a₁ » a₁~a₂
[ A [ σ ]T ]↷ γ₀~γ₁ ⇒ a₀ = [ A ]↷ ([ σ ]≈ γ₀~γ₁) ⇒ a₀
[ A [ σ ]T ]≈ γ₀~γ₁ ⇒ a₀~a₁ = [ A ]≈ ([ σ ]≈ γ₀~γ₁) ⇒ a₀~a₁
[ A [ σ ]T ]↷⟳ = [ A ]↷⟳
[ A [ σ ]T ]↷» = [ A ]↷»

_[_]t : Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
¦ t [ σ ]t ¦ γ      = ¦ t ¦ ((¦ σ ¦ γ))
[ t [ σ ]t ]≈ γ₀~γ₁ = [ t ]≈ ([ σ ]≈ γ₀~γ₁)


[∘]T : A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]T = Lrefl

idT : A [ id ]T ≡ A
idT = Lrefl

[∘]t : a [ σ ∘ δ ]t ≡ a [ σ ]t [ δ ]t
[∘]t = Lrefl

idt : {A : Ty Γ}{a : Tm Γ A} → a [ id ]t ≡ a -- TODO why not remove {} here?
idt = Lrefl

-- {- FamiliesP -}

record TyP (Γ : Con) : Set where
  field
    carrier : ¦ Γ ¦ → Set
    coercion : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → carrier γ₀ → carrier γ₁
open TyP renaming
  ( carrier to ¦_¦
  ; coercion to [_]↷_⇒_
  )

variable
  P : TyP Γ

record TmP (Γ : Con) (A : TyP Γ) : Set where
  field
    carrier : (γ : ¦ Γ ¦) → ¦ A ¦ γ
open TmP renaming
  ( carrier to ¦_¦
  )


_[_]TP : {Γ Δ : Con} → TyP Δ → Sub Γ Δ → TyP Γ
¦ P [ σ ]TP ¦ γ           = ¦ P ¦ (¦ σ ¦ γ)
[ P [ σ ]TP ]↷ γ₀~γ₁ ⇒ a₀ = [ P ]↷ ([ σ ]≈ γ₀~γ₁) ⇒ a₀

_[_]tP : TmP Δ P → (σ : Sub Γ Δ) → TmP Γ (P [ σ ]TP)
¦ t [ σ ]tP ¦ γ = ¦ t ¦ ((¦ σ ¦ γ))


[∘]TP : P [ σ ∘ δ ]TP ≡ P [ σ ]TP [ δ ]TP
[∘]TP = Lrefl

idTP : P [ id ]TP ≡ P
idTP = Lrefl


-- -- {- Comprehension -}

_▷_ : (Γ : Con) → (A : Ty Γ) → Con
¦ Γ ▷ A ¦ = LΣ (¦ Γ ¦) ¦ A ¦
[ Γ ▷ A ] (γ₀ L, a₀) ~ (γ₁ L, a₁) = LΣₚ ([ Γ ] γ₀ ~ γ₁) (λ γ₀~γ₁ → [ A ] ([ A ]↷ γ₀~γ₁ ⇒ a₀) ~ a₁)
[ Γ ▷ A ]⟳ = [ Γ ]⟳ L,ₚ [ A ]↷⟳
[ Γ ▷ A ]↔ (γ₀~γ₁ L,ₚ ↷a₀~a₁) = ([ Γ ]↔ γ₀~γ₁) L,ₚ
  ([ A ] [ A ]↔ ↷↷a₀~↷a₁ » [ A ]↷↔)
  where
    ↷↷a₀~↷a₁ = [ A ]≈ [ Γ ]↔ γ₀~γ₁ ⇒ ↷a₀~a₁
[ Γ ▷ A ] (γ₀~γ₁ L,ₚ ↷a₀~a₁) » (γ₁~γ₂ L,ₚ ↷a₁~a₂) = ([ Γ ] γ₀~γ₁ » γ₁~γ₂) L,ₚ
  ([ A ] [ A ]↷» » ↷↷a₀~a₂)
  where
    ↷↷a₀~↷a₁ = [ A ]≈ γ₁~γ₂ ⇒ ↷a₀~a₁
    ↷↷a₀~a₂ = [ A ] ↷↷a₀~↷a₁ » ↷a₁~a₂

-- ¦ Γ ▷ A ¦                                        = LΣ (¦ Γ ¦) ¦ A ¦
-- [ Γ ▷ A ]  (γ₀   L,  a₀  ) ~ (γ₁   L,  a₁  )     = LΣₚ ([ Γ ] γ₀ ~ γ₁) (λ γ₀~γ₁ → {! [ A ] ? ~ a₁ !})
-- [ Γ ▷ A ]⟳                                       = [ Γ ]⟳ L,ₚ {!   !}
-- [ Γ ▷ A ]↔ (γ₀~γ₁ L,ₚ a₀~a₁)                     = ([ Γ ]↔ γ₀~γ₁) L,ₚ {!   !}
-- [ Γ ▷ A ]  (γ₀~γ₁ L,ₚ a₀~a₁) » (γ₁~γ₂ L,ₚ a₁~a₂) = ([ Γ ] γ₀~γ₁ » γ₁~γ₂) L,ₚ {!   !}

-- ¦ Γ ▷ A ¦                                        = LΣ (¦ Γ ¦) ¦ A ¦
-- [ Γ ▷ A ]  (γ₀   L,  a₀  ) ~ (γ₁   L,  a₁  )     = LΣₚ ([ Γ ] γ₀ ~ γ₁) (λ γ₀~γ₁ → [ A ] γ₀~γ₁ ⇒ a₀ ~ a₁)
-- [ Γ ▷ A ]⟳                                       = [ Γ ]⟳ L,ₚ [ A ]⟳
-- [ Γ ▷ A ]↔ (γ₀~γ₁ L,ₚ a₀~a₁)                     = ([ Γ ]↔ γ₀~γ₁) L,ₚ ([ A ]↔ γ₀~γ₁ ⇒ a₀~a₁)
-- [ Γ ▷ A ]  (γ₀~γ₁ L,ₚ a₀~a₁) » (γ₁~γ₂ L,ₚ a₁~a₂) = ([ Γ ] γ₀~γ₁ » γ₁~γ₂) L,ₚ ([ A ] γ₀~γ₁ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂)

_,_ : (σ : Sub Γ Δ)(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
¦ σ , t ¦ γ      = (¦ σ ¦ γ)      L,  (¦ t ¦ γ)
[ σ , t ]≈ γ₀~γ₁ = ([ σ ]≈ γ₀~γ₁) L,ₚ ([ t ]≈ γ₀~γ₁)

p : Sub (Γ ▷ A) Γ
¦ p ¦ (γ L, a)           = γ
[ p ]≈ (γ₀~γ₁ L,ₚ a₀~a₁) = γ₀~γ₁

q : {Γ : Con}{A : Ty Γ} → Tm (Γ ▷ A) (A [ p {A = A} ]T)
¦ q ¦ (γ L, a)           = a
[ q ]≈ (γ₀~γ₁ L,ₚ a₀~a₁) = a₀~a₁

▷β₁ : {t : Tm Γ (A [ σ ]T)} → (p {A = A}) ∘ (_,_ {A = A} σ t) ≡ σ
▷β₁ = Lrefl

▷β₂ : {t : Tm Γ (A [ σ ]T)} → (q {A = A}) [ (_,_ {A = A} σ t) ]t ≡ t
▷β₂ = Lrefl

-- ▹η : {Γ : Con i}{Δ : Con j}{A : Ty Δ k}{σ : Sub (Γ ▷ (A [ σ ]T)) (Δ ▷ A)} →
--      ((p ∘ σ) , (q [ σ ]t)) ≡ σ
-- ▹η = ?

▹η : (_,_ {A = A} (p {A = A}) (q {A = A})) ≡ id
▹η = Lrefl


{- Unit -}

⊤ : TyP Γ
¦ ⊤ ¦ _      = L⊤
[ ⊤ ]↷ _ ⇒ _ = Ltt

tt : TmP Γ ⊤
¦ tt ¦ _ = Ltt

{- Id -}

-- Spoilers:
-- + Ty problémák?
-- + ID

Id : {A : Ty Γ} → (a₀ a₁ : Tm Γ A) → Ty Γ
¦ Id {A = A} a₀ a₁ ¦ γ = ↑ ([ A ] (¦ a₀ ¦ γ) ~ (¦ a₁ ¦ γ))
[ Id a₀ a₁ ] _ ~ _  = L⊤ₚ
[ Id a₀ a₁ ]⟳       = Lttₚ
[ Id a₀ a₁ ]↔ _     = Lttₚ
[ Id a₀ a₁ ] _ » _  = Lttₚ
-- [ Id {Γ = Γ} {A = A} a₀ a₁ ]↷ γ₀~γ₁ ⇒ ↑[ a₀~₀a₁ ]↑ =
--   ↑[
--     [ A ]
--           ([ a₀ ]≈ ([ Γ ]↔ γ₀~γ₁)) » [ A ]
--                                            a₀~₀a₁ » ([ a₁ ]≈ (γ₀~γ₁))
--   ]↑
[ Id {A = A} a₀ a₁ ]↷ γ₀~γ₁ ⇒ ↑[ a₀~₀a₁ ]↑ =
  ↑[
    [ A ] ([ A ]↔ ([ a₀ ]≈ γ₀~γ₁)) » ([ A ] ([ A ]≈ γ₀~γ₁ ⇒ a₀~₀a₁) » ([ a₁ ]≈ γ₀~γ₁))
  ]↑
[ Id a₀ a₁ ]≈ _ ⇒ _ = Lttₚ
[ Id a₀ a₁ ]↷⟳      = Lttₚ
[ Id a₀ a₁ ]↷»      = Lttₚ

refl : {A : Ty Γ} → {a : Tm Γ A} → Tm Γ (Id a a)
¦ refl {A = A} ¦ _ = ↑[ [ A ]⟳ ]↑
[ refl ]≈ _ = Lttₚ

-- subst : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j} →
--         {γ : ¦ Γ ¦} → {a₀ a₁ : Tm Γ A} → ([ A ] [ Γ ]⟳ ⇒ ¦ a₀ ¦ γ ~ ¦ a₁ ¦ γ) →
--         (Tm Γ (B [ _,_ {A = A} id a₀ ]T)) → (Tm Γ (B [ _,_ {A = A} id a₁ ]T))
subst : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} →
        {a₀ a₁ : Tm Γ A} → (Tm Γ (Id a₀ a₁)) →
        (Tm Γ (B [ _,_ {A = A} id a₀ ]T)) → (Tm Γ (B [ _,_ {A = A} id a₁ ]T))
¦ subst {Γ = Γ} {A = A} {B = B} id b₀ ¦ γ = [ B ]↷ [ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » ↓[ ¦ id ¦ γ ]↓) ⇒ ¦ b₀ ¦ γ
[_]≈_ (subst {Γ = Γ} {A = A} {B = B} id b₀) {γ₁ = γ₁} γ₀~γ₁ =
  [ B ] [ B ]   ([ B ]↔ [ B ]↷»)   »   [ B ]↷»   »   ↷b₀γ₁~b₁γ₁
  where
    a₀γ₁~a₁γ₁ = ↓[ ¦ id ¦ γ₁ ]↓
    ↷a₀γ₁~a₁γ₁ = [ A ] [ A ]↷⟳ » a₀γ₁~a₁γ₁
    ↷b₀γ₁~b₁γ₁ = [ B ]≈ [ Γ ]⟳ L,ₚ ↷a₀γ₁~a₁γ₁ ⇒ ([ b₀ ]≈ γ₀~γ₁)

-- Goal: (g01, ?) ((g00, a01) b0g0) ~ (g11, a01) b0g1

--       (g01) ((g00, a01) b0g0) ~
--       ((g01) >> (g00, a01)) b0g0 ~
--       ((g11, a01) >> (g01)) b0g0 ~
--       (g11, a01) ((g01) b0g0) ~
--       (g11, a01) b0g1
-- Have:
-- + b0≈ g01 : (g01) b0g0 ~ b0g1
-- +
--
-- transC3 (EL Id' _)
--    (symC (EL Id' _) (subst-trans Id' _ _ _))
--    (subst-trans Id' _ _ _)
--    (~s (subst Id' _) (~t idp' γ~))

{- Inductive Types -}

-- Sigma

Σ : (A : Ty Γ) → (B : Ty (Γ ▷ A)) → Ty Γ
¦ Σ A B ¦ γ = LΣ (¦ A ¦ γ) (λ a → ¦ B ¦ (γ L, a))
[ Σ {Γ = Γ} A B ] (a₀ L, b₀) ~ (a₁ L, b₁) = LΣₚ
  ([ A ] a₀ ~ a₁)
  (λ a₀~a₁ → ([ B ] [ B ]↷ [ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » a₀~a₁) ⇒ b₀ ~ b₁))
[ Σ A B ]⟳ = [ A ]⟳ L,ₚ [ B ]↷⟳
[ Σ {Γ = Γ} A B ]↔ (a₀~a₁ L,ₚ b₀~b₁) =
  ([ A ]↔ a₀~a₁)
    L,ₚ
  ([ B ] ([ B ]≈ [ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » ([ A ]↔ a₀~a₁)) ⇒ [ B ]↔ b₀~b₁) » [ B ]↷↔)
[ Σ {Γ = Γ} A B ] (a₀~a₁ L,ₚ b₀~b₁) » (a₁~a₂ L,ₚ b₁~b₂) =
  ([ A ] a₀~a₁ » a₁~a₂)
    L,ₚ
  ([ B ] [ B ] [ B ]↷» » ([ B ]≈ [ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » a₁~a₂) ⇒ b₀~b₁) » b₁~b₂)
[ Σ A B ]↷ γ₀~γ₁ ⇒ (a₀ L, b₀) = ([ A ]↷ γ₀~γ₁ ⇒ a₀) L, (([ B ]↷ (γ₀~γ₁ L,ₚ [ A ]⟳) ⇒ b₀))
[ Σ A B ]≈ γ₀~γ₁ ⇒ (a₀~a₁ L,ₚ b₀~b₁) =
  ([ A ]≈ γ₀~γ₁ ⇒ a₀~a₁)
    L,ₚ
  ([ B ] ([ B ] ([ B ]↔ [ B ]↷») » [ B ]↷») » ([ B ]≈ γ₀~γ₁ L,ₚ [ A ]⟳ ⇒ b₀~b₁))
[ Σ A B ]↷⟳ = [ A ]↷⟳ L,ₚ [ B ]↷↔
[ Σ A B ]↷» = [ A ]↷» L,ₚ ([ B ] ([ B ]↔ [ B ]↷») » [ B ]↷»)

_,Σ_ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} →
       (a : Tm Γ A)(b : Tm Γ (B [ _,_ {A = A} id a ]T)) → Tm Γ (Σ A B)
¦ a ,Σ b ¦ γ = ¦ a ¦ γ L, ¦ b ¦ γ
[ _,Σ_ {B = B} a b ]≈ γ₀~γ₁ = ([ a ]≈ γ₀~γ₁) L,ₚ ([ B ] [ B ]↔ [ B ]↷» » ([ b ]≈ γ₀~γ₁))

proj₁ : {A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm Γ (Σ A B) → Tm Γ A
¦ proj₁ t ¦ γ = Lproj₁ (¦ t ¦ γ)
[ proj₁ t ]≈ γ₀~γ₁ = proj₁ₚ ([ t ]≈ γ₀~γ₁)

proj₂ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} →
        (t : Tm Γ (Σ A B)) →
        Tm Γ (B [ _,_ {A = A} id (proj₁ {B = B} t) ]T)
¦ proj₂ t ¦ γ = Lproj₂ (¦ t ¦ γ)
[ proj₂ {B = B} t ]≈ γ₀~γ₁ = [ B ] [ B ]↷» » proj₂ₚ ([ t ]≈ γ₀~γ₁)

Σβ₁ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}{a : Tm Γ A}{b : Tm Γ (B [ _,_ {A = A} id a ]T)} →
      proj₁ {B = B} (_,Σ_ {B = B} a b) ≡ a
Σβ₁ = Lrefl

Σβ₂ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}{a : Tm Γ A}{b : Tm Γ (B [ _,_ {A = A} id a ]T)} →
      proj₂ {A = A} {B = B} (_,Σ_ {B = B} a b) ≡ b
Σβ₂ = Lrefl

Ση : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)}{t : Tm Γ (Σ A B)} →
     _,Σ_ {A = A} {B = B} (proj₁ {B = B} t) (proj₂ {A = A} {B = B} t) ≡ t
Ση = Lrefl

-- -- Bool

Bool : Ty Γ
¦ Bool ¦ γ = LBool
[ Bool ] b₀ ~ b₁ = b₀ ≡ₚ b₁
[ Bool ]⟳ = Lreflₚ
[ Bool ]↔ Lreflₚ = Lreflₚ
[ Bool ] Lreflₚ » Lreflₚ = Lreflₚ
[ Bool ]↷ γ₀~γ₁ ⇒ b₀ = b₀
[ Bool ]≈ γ₀~γ₁ ⇒ Lreflₚ = Lreflₚ
[ Bool ]↷⟳ = Lreflₚ
[ Bool ]↷» = Lreflₚ

true : Tm Γ Bool
¦ true ¦ γ = Ltrue
[ true ]≈ _ = Lreflₚ

false : Tm Γ Bool
¦ false ¦ γ = Lfalse
[ false ]≈ _ = Lreflₚ

if : {Γ : Con}{A : Ty Γ} → (b : Tm Γ Bool) → (Tm Γ A) → (Tm Γ A) → (Tm Γ A)
¦_¦ (if b t f) γ with ¦ b ¦ γ
... | Lfalse = ¦ f ¦ γ
... | Ltrue = ¦ t ¦ γ
[_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with ¦ b ¦ γ₀ | ¦ b ¦ γ₁ | [ b ]≈ γ₀~γ₁
... | Lfalse | .Lfalse | Lreflₚ = [ f ]≈ γ₀~γ₁
... | Ltrue | .Ltrue | Lreflₚ = [ t ]≈ γ₀~γ₁
-- ... | Lfalse | Lfalse | Lreflₚ = [ f ]≈ γ₀~γ₁
-- ... | Lfalse | Ltrue | ()
-- ... | Ltrue | Lfalse | ()
-- ... | Ltrue | Ltrue | Lreflₚ = [ t ]≈ γ₀~γ₁

-- -- TODO Dependent eliminator
-- -- if : {Γ : Con i}{C : Ty (Γ ▷ Bool) i j} →
-- --   (b : Tm Γ Bool) →
-- --   (Tm Γ (C [ _,_ {A = Bool} id true ]T)) →
-- --   (Tm Γ (C [ _,_ {A = Bool} id false ]T)) →
-- --   (Tm Γ (C [ _,_ {A = Bool} id b ]T))
-- -- ¦_¦ (if b t f) γ with ¦ b ¦ γ
-- -- ... | Lfalse = ¦ f ¦ γ
-- -- ... | Ltrue = ¦ t ¦ γ
-- -- [_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with [ b ]≈ γ₀~γ₁
-- -- ... | a rewrite a = {! a !}
-- -- [_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with ¦ b ¦ γ₁ | ¦ b ¦ γ₀
-- -- ... | a | c = {! a b !}
-- -- [_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with ¦ b ¦ γ₀
-- -- ... | a = {!  !}

Boolβ₁ : {Γ : Con}{A : Ty Γ} → {t : Tm Γ A} → {f : Tm Γ A} → if true t f ≡ t
Boolβ₁ = Lrefl

Boolβ₂ : {Γ : Con}{A : Ty Γ} → {t : Tm Γ A} → {f : Tm Γ A} → if false t f ≡ f
Boolβ₂ = Lrefl

{- Quotient Types -}

-- Interval

I : {Γ : Con} → Ty Γ
¦ I ¦ γ      = LBool
[ I ] _ ~ _  = L⊤ₚ
[ I ]⟳       = Lttₚ
[ I ]↔ _     = Lttₚ
[ I ] _ » _  = Lttₚ
[ I ]↷ _ ⇒ i = i
[ I ]≈ _ ⇒ _ = Lttₚ
[ I ]↷⟳      = Lttₚ
[ I ]↷»      = Lttₚ

left : {Γ : Con} → Tm Γ I
¦ left ¦  _ = Lfalse
[ left ]≈ _ = Lttₚ

right : {Γ : Con} → Tm Γ I
¦ right ¦  _ = Ltrue
[ right ]≈ _ = Lttₚ

Ieq : {Γ : Con} → Tm Γ (Id left right)
-- Ieq = _
¦ Ieq ¦  _ = ↑[ Lttₚ ]↑
[ Ieq ]≈ _ = Lttₚ

Iif : {Γ : Con} {A : Ty Γ} → Tm Γ I → (l : Tm Γ A) → (r : Tm Γ A) → Tm Γ (Id l r) → Tm Γ A
¦ Iif i l r e ¦ γ with ¦ i ¦ γ
... | Lfalse = ¦ l ¦ γ
... | Ltrue = ¦ r ¦ γ
[_]≈_ (Iif {Γ = Γ} {A = A} i l r e) {γ₀} {γ₁} γ₀~γ₁ with ¦ i ¦ γ₀ | ¦ i ¦ γ₁
... | Lfalse | Lfalse = [ l ]≈ γ₀~γ₁
... | Lfalse | Ltrue = [ A ] [ l ]≈ γ₀~γ₁ » ↓[ ¦ e ¦ γ₁ ]↓
... | Ltrue | Lfalse = [ A ] [ r ]≈ γ₀~γ₁ » ([ A ]↔ ↓[ ¦ e ¦ γ₁ ]↓)
... | Ltrue | Ltrue = [ r ]≈ γ₀~γ₁

{- Coinductive Types -}

-- Pi

Π : {Γ : Con} → (A : Ty Γ) → (B : Ty (Γ ▷ A)) → Ty Γ
¦ Π {Γ = Γ} A B ¦ γ = LΣₛₚ
  ((a : ¦ A ¦ γ) → ¦ B ¦ (γ L, a))
  (λ f → {a₀ a₁ : ¦ A ¦ γ}(a₀~a₁ : [ A ] a₀ ~ a₁) →
    [ B ] [ B ]↷ [ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » a₀~a₁) ⇒ f a₀ ~ f a₁)
[_]_~_ (Π {Γ = Γ} A B) {γ = γ} (f₀ L,ₛₚ f₀~) (f₁ L,ₛₚ f₁~) =
  {a₀ : ¦ A ¦ γ}{a₁ : ¦ A ¦ γ}(a₀~a₁ : [ A ] a₀ ~ a₁) →
  [ B ] [ B ]↷ [ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » a₀~a₁) ⇒ f₀ a₀ ~ f₁ a₁
[ Π A B ]⟳ {c = f L,ₛₚ f~} a₀~a₁ = f~ a₀~a₁
([ Π {Γ = Γ} A B ]↔ f₀~f₁) a₀~a₁ =
  [ B ] [ B ]≈ [ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » a₀~a₁) ⇒ f₁a₀~↷f₀a₁ » [ B ]↷↔
  where
    f₁a₀~↷f₀a₁ = [ B ]↔ (f₀~f₁ ([ A ]↔ a₀~a₁))
([ Π A B ] f₀~f₁ » f₁~f₂) a₀~a₁ = [ B ] f₀~f₁ a₀~a₁ » ([ B ] [ B ]↔ [ B ]↷⟳ » f₁~f₂ [ A ]⟳)
[ Π {Γ = Γ} A B ]↷ γ₀~γ₁ ⇒ (f L,ₛₚ f~) =
  (λ a → [ B ]↷ γ₀~γ₁ L,ₚ [ A ]↷↔ ⇒ f ([ A ]↷ γ₁~γ₀ ⇒ a))
    L,ₛₚ
  (λ a₀~a₁ → [ B ] ([ B ] ([ B ]↔ [ B ]↷») » [ B ]↷») » ([ B ]≈ γ₀~γ₁ L,ₚ [ A ]↷↔ ⇒ f~ ([ A ]≈ γ₁~γ₀ ⇒ a₀~a₁)))
  where
    γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁
([ Π {Γ = Γ} A B ]≈ γ₀~γ₁ ⇒ f₀~f₁) a₀~a₁ =
  [ B ] ([ B ] ([ B ]↔ [ B ]↷») » [ B ]↷») » ([ B ]≈ γ₀~γ₁ L,ₚ [ A ]↷↔ ⇒ f₀~f₁ ([ A ]≈ γ₁~γ₀ ⇒ a₀~a₁))
  where
    γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁
[ Π A B ]↷⟳ {c = (f L,ₛₚ f~)} a₀~a₁ = [ B ] [ B ]↔ [ B ]↷» » f~ ([ A ] [ A ]↷⟳ » a₀~a₁)
[ Π {Γ = Γ} A B ]↷» {γ₀~γ₁ = γ₀~γ₁} {γ₁~γ₂ = γ₁~γ₂} {c₀ = (f₀ L,ₛₚ f₀~)} a₀~a₁ =
  [ B ] [ B ] ([ B ] ([ B ]↔ [ B ]↷») » [ B ]↷»)
            » ([ B ]≈ γ₀~γ₂ L,ₚ ([ A ] ([ A ]↔ [ A ]↷») » [ A ]↷↔)
                    ⇒ f₀~ ([ A ] [ A ]≈ γ₂~γ₀ ⇒ a₀~a₁ » [ A ]↷»))
            » [ B ]↷»
  where
    γ₀~γ₂ = [ Γ ] γ₀~γ₁ » γ₁~γ₂
    γ₂~γ₀ = [ Γ ]↔ γ₀~γ₂


lam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
¦ lam {Γ = Γ} {A = A} t ¦ γ =
  (λ a → ¦ t ¦ (γ L, a))
    L,ₛₚ
  (λ a₀~a₁ → [ t ]≈ ([ Γ ]⟳ L,ₚ ([ A ] [ A ]↷⟳ » a₀~a₁)))
([ lam {Γ = Γ} {A = A} {B = B} t ]≈ γ₀~γ₁) a₀~a₁ =
  [ B ] [ B ]↔ [ B ]↷» » ([ t ]≈ (γ₀~γ₁ L,ₚ ([ A ] [ A ]↷↔ » a₀~a₁)))

app : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
¦ app t ¦ (γ L, a) = proj₁ₛₚ (¦ t ¦ γ) a
-- [_]≈_ (app {A = A} {B = B} t) {γ₀ L, a₀} {γ₁ L, a₁} (γ₀~γ₁ L,ₚ ↷a₀~a₁) with ¦ t ¦ γ₀ | ¦ t ¦ γ₁
-- ... | f₀ L,ₛₚ f₀~ | f₁ L,ₛₚ f₁~ = {! [ t ]≈ γ₀~γ₁  !}
[_]≈_ (app {Γ = Γ} {A = A} {B = B} t) {γ₀ L, a₀} {γ₁ L, a₁} (γ₀~γ₁ L,ₚ ↷a₀~a₁) =
  [ B ] [ B ] ([ B ] [ B ]↷» » [ B ]↷»)
            » ([ B ]≈ [ Γ ]⟳ L,ₚ ([ A ] ([ A ]↔ [ A ]↷») » ↷a₀~a₁)
                    ⇒ ([ B ]≈ γ₀~γ₁ L,ₚ ([ A ]↔ ([ A ] [ A ]↷» » [ A ]↷»))
                            ⇒ proj₂ₛₚ (¦ t ¦ γ₀) ([ A ]↔ [ A ]↷↔)))
            » ([ t ]≈ γ₀~γ₁) ↷a₀~a₁

Πβ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} → {t : Tm (Γ ▷ A) B} → app {A = A} (lam {A = A} t) ≡ t
Πβ = Lrefl

Πη : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▷ A)} → {t : Tm Γ (Π A B)} → lam {A = A} {B = B} (app {A = A} t) ≡ t
Πη = Lrefl

-- {- Universe -}

-- -- Not in scope of the project
