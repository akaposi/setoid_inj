{-# OPTIONS --prop --rewriting #-}

module MLTTP where

open import Agda.Primitive
open import Relation.Binary.PropositionalEquality
open import Agda.Builtin.Equality.Rewrite

variable
  i j k l m n : Level

record MLTTP : Setω where
  infixl 10 _∘_
  infixl 10 _▹_
  infixl 9 _,_

  field
    -- Category

    Con : Set
    Sub : Con → Con → Set

    id  : {Γ : Con} →
          Sub Γ Γ
    _∘_ : {Γ Δ Θ : Con} →
          Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
    ass : {Γ Δ Θ Ω : Con} → {σ : Sub Θ Ω} → {δ : Sub Δ Θ} → {γ : Sub Γ Δ} →
          (σ ∘ δ) ∘ γ ≡ σ ∘ (δ ∘ γ)
    idl : {Γ Δ : Con} → {σ : Sub Γ Δ} →
          id ∘ σ ≡ σ
    idr : {Γ Δ : Con} → {σ : Sub Γ Δ} →
          σ ∘ id ≡ σ


    -- Terminal object

    ∙  : Con
    ε  : {Γ : Con} →
         Sub Γ ∙
    ∙η : {Γ : Con} →
         {σ : Sub Γ ∙} → σ ≡ ε


    -- Families

    Ty  : Con → (i : Level) → Set
    _[_]T : {Γ Δ : Con} →
            Ty Δ i → Sub Γ Δ → Ty Γ i
    [∘]T  : {Γ Δ Θ : Con} → {A : Ty Θ i} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} →
            A [ σ ]T [ δ ]T ≡ A [ (σ ∘ δ) ]T
    -- {-# REWRITE [∘]T #-}
    [id]T : {Γ : Con} → {A : Ty Γ i} →
            A [ id ]T ≡ A

    Tm  : (Γ : Con) → Ty Γ i → Set
    _[_]t : {Γ Δ : Con} → {A : Ty Δ i} →
            Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
    [∘]t : {Γ Δ Θ : Con} → {A : Ty Θ i} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {t : Tm Θ A} →
           t [ σ ]t [ δ ]t ≡ subst (Tm Γ) (sym [∘]T) (t [ (σ ∘ δ) ]t)
    -- {-# REWRITE [∘]t #-}
    [id]t : {Γ : Con} → {A : Ty Γ i} → {t : Tm Γ A} →
            t [ id ]t ≡ subst (Tm Γ) (sym [id]T) t


    -- Comprehension

    _▹_ : (Γ : Con) → Ty Γ i → Con
    _,_ : {Γ Δ : Con} → {A : Ty Δ i} →
          (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
    p   : {Γ : Con} → {A : Ty Γ i} →
          Sub (Γ ▹ A) Γ
    q   : {Γ : Con} → {A : Ty Γ i} →
          Tm (Γ ▹ A) (A [ p ]T)

    ▹β₁ : {Γ Δ : Con} → {σ : Sub Γ Δ} → {A : Ty Δ i} → {t : Tm Γ (A [ σ ]T)} →
          p ∘ (σ , t) ≡ σ
    -- {-# REWRITE ▹β₁ #-}
    ▹β₂ : {Γ Δ : Con} → {σ : Sub Γ Δ} → {A : Ty Δ i} → {t : Tm Γ (A [ σ ]T)} →
          q [ σ , t ]t ≡ subst (Tm Γ) (trans (cong (A [_]T) (sym ▹β₁)) (sym [∘]T)) t

    ▹η  : {Γ : Con} → {A : Ty Γ i} →
          (p {A = A} , q) ≡ id
    ,∘  : {Γ Δ Θ : Con} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} → {A : Ty Θ i} → {t : Tm Δ (A [ σ ]T)} →
          (σ , t) ∘ δ ≡ (σ ∘ δ , subst (Tm Γ) [∘]T (t [ δ ]t))


    -- Unit

    ⊤    : {Γ : Con} → Ty Γ lzero
    ⊤[]  : {Γ δ : Con} → {σ : Sub Γ δ} → ⊤ [ σ ]T ≡ ⊤
    tt   : {Γ : Con} → Tm Γ ⊤
    -- tt[] : {Γ δ : Con} → {σ : sub Γ δ} → tt [ σ ]t ≡ subst (Tm Γ) (sym ⊤[]) tt
    ttη  : {Γ : Con} → {t : Tm Γ ⊤} → t ≡ tt

    -- Pi
