{-# OPTIONS --prop --rewriting #-}

module MLTTPPostulate where

open import Agda.Primitive
open import Relation.Binary.PropositionalEquality
open import Agda.Builtin.Equality.Rewrite

variable
  i j k l m n : Level

postulate
  Con : Set
  Sub : Con → Con → Set
  Ty : Con → (i : Level) → Set
  Tm : (Γ : Con) → Ty Γ i → Set

variable
  Γ Δ Θ : Con
  σ δ γ : Sub Γ Δ
  A B C : Ty Γ i
  t u v : Tm Γ A

postulate
  id : Sub Γ Γ
  _∘_ : Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
  ass : (σ ∘ δ) ∘ γ ≡ σ ∘ (δ ∘ γ)
  idl : id ∘ σ ≡ σ
  idr : σ ∘ id ≡ σ

  ∙ : Con
  ε : Sub Γ ∙
  ∙η : {σ : Sub Γ ∙} → σ ≡ ε

  _[_]T : Ty Δ i → Sub Γ Δ → Ty Γ i
  _[_]t : Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  [∘]T : A [ σ ]T [ δ ]T ≡ A [ (σ ∘ δ) ]T
  {-# REWRITE [∘]T #-}
  [∘]t : t [ σ ]t [ δ ]t ≡ t [ (σ ∘ δ) ]t
  {-# REWRITE [∘]t #-}

  _▹_ : (Γ : Con) → Ty Γ i → Con
  _,_ : (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T) → Sub Γ (Δ ▹ A)
  p : Sub (Γ ▹ A) Γ
  q : Tm (Γ ▹ A) (A [ p ]T)

  ▹β₁ : p ∘ (σ , t) ≡ σ
  {-# REWRITE ▹β₁ #-}
  -- A : Ty Δ
  -- q : Tm (Δ ▹ A) (A [ p ]T)
  -- σ : Sub Γ Δ
  -- t : Tm Γ A[σ]
  -- σ , t : Sub Γ (Δ ▹ A)
  -- _[_]t : Tm Δ A → (σ : Sub ?1 Δ) → Tm ?1 (A [ σ ]T)
  ▹β₂ : q [ σ , t ]t ≡ t