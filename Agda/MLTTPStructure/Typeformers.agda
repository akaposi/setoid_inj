{-# OPTIONS --prop #-}

module Typeformers where

open import Agda.Primitive
open import Relation.Binary.PropositionalEquality
open ≡-Reasoning

open import CWF

-- cong-dep : {A : Set i} → {B : A → Set j} → (f : (a : A) → B a) → {x y : A} → (eq : x ≡ y) → f x ≡ subst B (sym eq) (f y)
-- cong-dep f refl = refl

-- subst-sym :
-- cong-sym :

record Unit (CAT : Category) (FAM : Families CAT) : Setω where
  open Category CAT
  open Families FAM
  field
    ⊤    : {Γ : Con} → Ty Γ lzero
    ⊤[]  : {Γ Δ : Con} → {σ : Sub Γ Δ} → ⊤ [ σ ]T ≡ ⊤
    tt   : {Γ : Con} → Tm Γ ⊤
    -- tt[] : {Γ δ : Con} → {σ : sub Γ δ} → tt [ σ ]t ≡ subst (Tm Γ) (sym ⊤[]) tt
    ttη  : {Γ : Con} → {t : Tm Γ ⊤} → t ≡ tt

record Pi (CAT : Category) (FAM : Families CAT) (EXT : ContextExtension CAT FAM) : Setω where
  open Category CAT
  open Families FAM
  open ContextExtension EXT
  field
    Π     : {Γ : Con} → (A : Ty Γ i) → Ty (Γ ▹ A) j →
            Ty Γ (i ⊔ j)
    Π[]   : {Γ Δ : Con} → {A : Ty Δ i} → {B : Ty (Δ ▹ A) j} → {σ : Sub Γ Δ} →
            ((Π A B)[ σ ]T) ≡ Π (A [ σ ]T) (B [ (σ ∘ p) , subst (Tm (Γ ▹ (A [ σ ]T))) (sym [∘]T) q ]T)
    lam   : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} →
            Tm (Γ ▹ A) B → Tm Γ (Π A B)
    lam[] : {Γ Δ : Con} → {A : Ty Δ i} → {B : Ty (Δ ▹ A) j} → {t : Tm (Δ ▹ A) B} → {σ : Sub Γ Δ} →
            (lam t) [ σ ]t ≡ subst (Tm Γ) (sym Π[]) (lam (t [ (σ ∘ p) , subst (Tm (Γ ▹ (A [ σ ]T))) (sym [∘]T) q ]t))
    app   : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} →
            Tm Γ (Π A B) → Tm (Γ ▹ A) B
    Πβ    : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} → {t : Tm (Γ ▹ A) B} →
            app (lam t) ≡ t
    Πη    : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} → {t : Tm Γ (Π A B)} →
            lam (app t) ≡ t

record Sigma (CAT : Category) (FAM : Families CAT) (EXT : ContextExtension CAT FAM) : Setω where
  open Category CAT
  open Families FAM
  open ContextExtension EXT
  field
    Σ      : {Γ : Con} → (A : Ty Γ i) → Ty (Γ ▹ A) j →
             Ty Γ (i ⊔ j)
    Σ[]    : {Γ Δ : Con} → {A : Ty Δ i} → {B : Ty (Δ ▹ A) j} → {σ : Sub Γ Δ} →
             ((Σ A B) [ σ ]T) ≡ Σ (A [ σ ]T) (B [ (σ ∘ p) , subst (Tm (Γ ▹ (A [ σ ]T))) (sym [∘]T) q ]T)
    pair   : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} →
             (t : Tm Γ A) → Tm Γ (B [ id , subst (Tm Γ) (sym [id]T) t ]T) → Tm Γ (Σ A B)
    -- pair[] : {Γ Δ : Con} → {A : Ty Δ i} → {B : Ty (Δ ▹ A) j} →
    --          {t : Tm Δ A} → {u : Tm Δ (B [ id , subst (Tm Δ) (sym [id]T) t ]T)} →
    --          {σ : Sub Γ Δ} → (pair t u) [ σ ]t ≡ subst (Tm Γ) (sym Σ[]) (pair (t [ σ ]t) (subst (Tm Γ)
    --         --    (trans (sym [∘]T) (trans (cong (B [_]T) (trans ,∘ (trans {!   !} (sym ,∘)))) [∘]T))
    --         (begin
    --           (B [ id , subst (Tm Δ) (sym [id]T) t ]T) [ σ ]T
    --             ≡⟨ sym [∘]T ⟩
    --           B [ (id , subst (Tm Δ) (sym [id]T) t) ∘ σ ]T
    --             ≡⟨ cong (B [_]T) (
    --               begin
    --                 (id , subst (Tm Δ) (sym [id]T) t) ∘ σ
    --                   ≡⟨ ,∘ ⟩
    --                 (id ∘ σ) , (subst (Tm Γ) (sym [∘]T) (subst (Tm Δ) (sym [id]T) t [ σ ]t))
    --                   -- ≡⟨ ,-cong idl (sym (subst-sym-subst (cong (A [_]T) idl)))⟩
    --                   ≡⟨ ,-cong idl (sym {! subst (λ t → subst (Tm Γ) t (subst (Tm Γ) (cong (A [_]T) idl) (subst (Tm Γ) (sym [∘]T) (subst (Tm Δ) (sym [id]T) t [ σ ]t))) ≡ subst (Tm Γ) (sym [∘]T) (subst (Tm Δ) (sym [id]T) t [ σ ]t)) ? ?  !})⟩


    --                 (σ , subst (Tm Γ) (cong (A [_]T) idl) (subst (Tm Γ) (sym [∘]T) (subst (Tm Δ) (sym [id]T) t [ σ ]t)))
    --                   ≡⟨ {! cong ()   !} ⟩
    --                 {!   !}
    --                   ≡⟨ {!   !} ⟩
    --                 σ ∘ p ∘ (id , subst (Tm Γ) (sym [id]T) (t [ σ ]t)) , subst (Tm Γ) (sym [∘]T) (subst (Tm (Γ ▹ (A [ σ ]T))) (sym [∘]T) q [ id , subst (Tm Γ) (sym [id]T) (t [ σ ]t) ]t)
    --                   ≡⟨ sym ,∘ ⟩
    --                 (σ ∘ p , subst (Tm (Γ ▹ (A [ σ ]T))) (sym [∘]T) q) ∘ (id , subst (Tm Γ) (sym [id]T) (t [ σ ]t))
    --               ∎) ⟩
    --           B [ (σ ∘ p , subst (Tm (Γ ▹ (A [ σ ]T))) (sym [∘]T) q) ∘ (id , subst (Tm Γ) (sym [id]T) (t [ σ ]t)) ]T
    --             ≡⟨ [∘]T ⟩
    --           (B [ σ ∘ p , subst (Tm (Γ ▹ (A [ σ ]T))) (sym [∘]T) q ]T) [ id , subst (Tm Γ) (sym [id]T) (t [ σ ]t) ]T
    --         ∎)
    --          (u [ σ ]t)))

             -- B [ id , t ] [ ? ] = B [ σ ∘ p , q ] [ id , t [ σ ] ]
             --                      B [ (σ ∘ p , q) ∘ (id , t [ σ ]) ]
             --                      B [ σ ∘ p ∘ (id , t [ σ ]) , t [ σ ]]
             --                      B [ σ ∘ id , t [ σ ]]
             --                      B [ σ , t [ σ ]]
    fst    : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} →
             Tm Γ (Σ A B) → Tm Γ A
    snd    : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} →
             (t : Tm Γ (Σ A B)) → Tm Γ (B [ id , subst (Tm Γ) (sym [id]T) (fst t) ]T)
    Σβ₁    : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} →
             {a : Tm Γ A} → {b : Tm Γ (B [ id , subst (Tm Γ) (sym [id]T) a ]T)} →
             fst (pair a b) ≡ a
    Σβ₂    : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} →
             {a : Tm Γ A} → {b : Tm Γ (B [ id , subst (Tm Γ) (sym [id]T) a ]T)} →
             snd (pair a b) ≡ subst (Tm Γ) (cong (λ T → B [ id , subst (Tm Γ) (sym [id]T) T ]T) (sym Σβ₁)) b
    Ση     : {Γ : Con} → {A : Ty Γ i} → {B : Ty (Γ ▹ A) j} → {t : Tm Γ (Σ A B)} →
             pair (fst t) (snd t) ≡ t

record Boolean (CAT : Category) (FAM : Families CAT) (EXT : ContextExtension CAT FAM) : Setω where
  open Category CAT
  open Families FAM
  open ContextExtension EXT

  -- subst-[] : {Γ Δ : Con} → {A : Ty Δ i} → {t u : Tm Δ A} → (P : Tm Δ A → Set) → {eq : t ≡ u} → (pt : P t) → {σ : Sub Γ Δ} →
  --            (subst (Tm Δ) eq pt) [ σ ]t ≡ subst P eq (t [ σ ]t)
  -- subst-[] = {!   !}

  field
    Bool    : {Γ : Con} → Ty Γ i
    Bool[]  : {Γ Δ : Con} → {σ : Sub Γ Δ} → (Bool {i}) [ σ ]T ≡ Bool
    true    : {Γ : Con} → Tm Γ (Bool {i})
    true[]  : {Γ Δ : Con} → {σ : Sub Γ Δ} → (true {i}) [ σ ]t ≡ subst (Tm Γ) (sym Bool[]) true
    false   : {Γ : Con} → Tm Γ (Bool {i})
    false[] : {Γ Δ : Con} → {σ : Sub Γ Δ} → (false {i}) [ σ ]t ≡ subst (Tm Γ) (sym Bool[]) false
    ite     : {Γ : Con} → {A : Ty (Γ ▹ Bool {i}) i} →
              (t : Tm Γ (Bool {i})) →
              (u : Tm Γ (A [ id , subst (Tm Γ) (sym [id]T) true ]T)) →
              (v : Tm Γ (A [ id , subst (Tm Γ) (sym [id]T) false ]T)) →
              Tm Γ (A [ id , subst (Tm Γ) (sym [id]T) t ]T)
    -- ite[]   : {Γ Δ : Con} → {A : Ty (Δ ▹ Bool {i}) i} →
    --           {t : Tm Δ (Bool {i})} →
    --           {u : Tm Δ (A [ id , subst (Tm Δ) (sym [id]T) true ]T)} →
    --           {v : Tm Δ (A [ id , subst (Tm Δ) (sym [id]T) false ]T)} →
    --           {σ : Sub Γ Δ} →
    --           -- (ite t u v) [ σ ]t ≡ subst (Tm Γ) {!   !} (ite {A = A [ (σ ∘ p) , subst (Tm (Γ ▹ Bool)) (trans Bool[] (sym Bool[])) q ]T}
    --           (ite t u v) [ σ ]t ≡ subst (Tm Γ) {!   !} (ite
    --             (subst (Tm Γ) Bool[] (t [ σ ]t))
    --             (subst (Tm Γ) {!   !} (u [ σ ]t))
    --             (subst (Tm Γ) {!   !} (v [ σ ]t))
    --           )
    iteβ₁   : {Γ : Con} → {A : Ty (Γ ▹ Bool {i}) i} →
              (u : Tm Γ (A [ id , subst (Tm Γ) (sym [id]T) true ]T)) →
              (v : Tm Γ (A [ id , subst (Tm Γ) (sym [id]T) false ]T)) →
              ite true u v ≡ u
    iteβ₂   : {Γ : Con} → {A : Ty (Γ ▹ Bool {i}) i} →
              (u : Tm Γ (A [ id , subst (Tm Γ) (sym [id]T) true ]T)) →
              (v : Tm Γ (A [ id , subst (Tm Γ) (sym [id]T) false ]T)) →
              ite false u v ≡ v
    Ite     : {Γ : Con} → (t : Tm Γ (Bool {i})) → (A : Ty Γ i) → (B : Ty Γ i) → Ty Γ i
    Ite[]   : {Γ Δ : Con} → {t : Tm Δ (Bool {i})} → {A : Ty Δ i} → {B : Ty Δ i} → {σ : Sub Γ Δ} →
              (Ite t A B) [ σ ]T ≡ Ite (subst (Tm Γ) Bool[] (t [ σ ]t)) (A [ σ ]T) (B [ σ ]T)
    Iteβ₁   : {Γ : Con} → (A : Ty Γ i) → (B : Ty Γ i) → Ite true A B ≡ A
    Iteβ₂   : {Γ : Con} → (A : Ty Γ i) → (B : Ty Γ i) → Ite false A B ≡ B