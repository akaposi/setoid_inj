{-# OPTIONS --prop #-}

module MLTTPAlgebra where

open import Agda.Primitive
open import Relation.Binary.PropositionalEquality
open ≡-Reasoning

open import CWF
open import Typeformers
open import Extra

record MLTTP : Setω where
  field
    CAT : Category
    TOB : TerminalObject    CAT
    FAM : Families          CAT
    EXT : ContextExtension  CAT FAM
    UNT : Unit              CAT FAM
    PI  : Pi                CAT FAM EXT
    SIG : Sigma             CAT FAM EXT
    BLN : Boolean           CAT FAM EXT
    RAI : Raising           CAT FAM
    PRO : Propositions      CAT FAM

