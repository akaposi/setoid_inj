{-# OPTIONS --prop #-}

module Extra where

open import Agda.Primitive
open import Relation.Binary.PropositionalEquality
open ≡-Reasoning

open import CWF

record Raising (CAT : Category) (FAM : Families CAT) : Setω where
  open Category CAT
  open Families FAM
  field
    Raise : {Γ : Con} → {i j : Level} → Ty Γ i → Ty Γ (i ⊔ j)
    Raise[] : {Γ Δ : Con} → {i j : Level} → {A : Ty Δ i} → {σ : Sub Γ Δ} →
              (Raise {j = j} A) [ σ ]T ≡ Raise {j = j} (A [ σ ]T)
    mk : {Γ : Con} → {A : Ty Γ i} → Tm Γ A → Tm Γ (Raise {j = j} A)
    un : {Γ : Con} → {A : Ty Γ i} → Tm Γ (Raise {j = j} A) → Tm Γ A
    Raiseβ : {Γ : Con} → {A : Ty Γ i} → {t : Tm Γ A} → un {j = j} (mk t) ≡ t
    Raiseη : {Γ : Con} → {A : Ty Γ i} → {t : Tm Γ (Raise {j = j} A)} → mk (un t) ≡ t

record Propositions (CAT : Category) (FAM : Families CAT) : Setω where
  open Category CAT
  open Families FAM
  field
    TyP : Con → Level → Set
    _[_]TP : {Γ Δ : Con} →
             TyP Δ i → Sub Γ Δ → TyP Γ i
    [∘]TP  : {Γ Δ Θ : Con} → {A : TyP Θ i} → {σ : Sub Δ Θ} → {δ : Sub Γ Δ} →
             A [ (σ ∘ δ) ]TP ≡ A [ σ ]TP [ δ ]TP
    [id]TP : {Γ : Con} → {A : TyP Γ i} →
             A [ id ]TP ≡ A

    Lift   : {Γ : Con} → TyP Γ i → Ty Γ i
    Lift[] : {Γ Δ : Con} → {P : TyP Δ i} → {σ : Sub Γ Δ} → (Lift P) [ σ ]T ≡ Lift (P [ σ ]TP)
    irr    : {Γ : Con} → {A : TyP Γ i} → {u v : Tm Γ (Lift A)} → u ≡ v

    Tr     : {Γ : Con} → Ty Γ i → TyP Γ i
    Tr[]   : {Γ Δ : Con} → {A : Ty Δ i} → {σ : Sub Γ Δ} → (Tr A) [ σ ]TP ≡ Tr (A [ σ ]T)
    mkTr   : {Γ : Con} → {A : Ty Γ i} → Tm Γ A → Tm Γ (Lift (Tr A))
    unTr   : {Γ : Con} → {A : Ty Γ i} → Tm Γ (Lift (Tr A)) → Tm Γ A
    Tyβ    : {Γ : Con} → {A : Ty Γ i} → {t : Tm Γ A} → unTr (mkTr t) ≡ t
