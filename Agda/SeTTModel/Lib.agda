{-# OPTIONS --prop #-}
-- {-# OPTIONS --cumulativity #-}
{-# OPTIONS --no-pattern-match #-}
module Lib where

open import Agda.Primitive

private
  variable
    i j : Level
    A B : Set i

infix 4 _≡_
data _≡_ {ℓ} {A : Set ℓ} (x : A) : A → Set ℓ where
  instance refl : x ≡ x
{-# BUILTIN EQUALITY _≡_ #-}

-- cong : (f : A → B) {x y : A} → x ≡ y → f x ≡ f y
-- cong f refl = refl

-- subst : {A : Set} {x y : A} (P : A → Prop) → x ≡ y → P x → P y
-- subst P refl px = px

-- subst : {ℓ₀ ℓ₁ : Level} {A : Set ℓ₀} {x y : A} (P : A → Set ℓ₁) → x ≡ y → P x → P y
-- subst P refl px = px

infix 4 _≡ω_
data _≡ω_ {A : Setω} (x : A) : A → Prop where
  instance reflω : x ≡ω x

-- infix 4 _≡ω₁_
-- data _≡ω₁_ {A : Setω₁} (x : A) : A → Prop where
--   instance reflω₁ : x ≡ω₁ x
