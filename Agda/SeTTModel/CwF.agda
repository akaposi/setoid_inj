{-# OPTIONS --prop #-}
-- {-# OPTIONS --cumulativity #-}
-- {-# OPTIONS --no-pattern-match #-}
module CwF where

open import Agda.Primitive

open import Lib as L using (_≡_ ; _≡ω_)
open import MLTTP

variable
  i j k l m n : Level

module Category where

  -- carrier cannot be Setω, because equality would need to be in Propω
  -- if we had Propω, we wouldn't need the `level` field
  -- in case we would omit levels, we couldn't add a universe to the model
  record Con : Setω where
    field
      level        : Level
      carrier      : Set level
      equality     : carrier → carrier → Prop level
      reflexivity  : {c        : carrier} → equality c c
      symmetry     : {c₀ c₁    : carrier} → equality c₀ c₁ → equality c₁ c₀
      transitivity : {c₀ c₁ c₂ : carrier} → equality c₀ c₁ → equality c₁ c₂ → equality c₀ c₂
    -- syntax equality Γ γ₀ γ₁ = γ₀ ~[ Γ ]~ γ₁
  open Con public renaming
    ( level        to #_#
    ; carrier      to ∣_∣
    ; equality     to infix 5 [_]_~_
    ; reflexivity  to [_]⟳
    ; symmetry     to [_]↔_
    ; transitivity to [_]_»_
    )

  variable
    Γ Δ Θ Ω : Con

  record Sub (Γ : Con) (Δ : Con) : Setω where
    field
      carrier      : ∣ Γ ∣ → ∣ Δ ∣
      preservation : {γ₀ γ₁ : ∣ Γ ∣} → [ Γ ] γ₀ ~ γ₁ → [ Δ ] (carrier γ₀) ~ (carrier γ₁)
  open Sub public renaming
    ( carrier      to ∣_∣
    ; preservation to infix 5 [_]≈_
    )

  variable
    σ δ γ ν : Sub Γ Δ

  infixl 6 _∘_
  _∘_ : (σ : Sub Δ Θ) → (δ : Sub Γ Δ) → Sub Γ Θ
  ∣ σ ∘ δ ∣ γ      = ∣ σ ∣ (∣ δ ∣ γ)
  [ σ ∘ δ ]≈ γ₀~γ₁ = [ σ ]≈ ([ δ ]≈ γ₀~γ₁)

  id : Sub Γ Γ
  ∣ id ∣ γ      = γ
  [ id ]≈ γ₀~γ₁ = γ₀~γ₁

  ass : (σ ∘ δ) ∘ γ ≡ω σ ∘ (δ ∘ γ)
  ass = L.reflω

  idl : id ∘ σ ≡ω σ
  idl = L.reflω

  idr : σ ∘ id ≡ω σ
  idr = L.reflω


module TerminalObject where
  open Typeformersₘ.Unitₘ
  open Typeformersₚₘ.Unitₚₘ

  open Category

  ◆ : Con
  # ◆ #       = lzero
  ∣ ◆ ∣       = ⊤ₘ
  [ ◆ ] _ ~ _ = ⊤ₚₘ
  [ ◆ ]⟳      = ttₚₘ
  [ ◆ ]↔ _    = ttₚₘ
  [ ◆ ] _ » _ = ttₚₘ

  ε : Sub Γ ◆
  ∣ ε ∣ _  = ttₘ
  [ ε ]≈ _ = ttₚₘ

  ◆η : {σ : Sub Γ ◆} → σ ≡ω ε
  ◆η = L.reflω

module Families where
  open Category

  record Ty (Γ : Con) (j : Level) : Setω where
    field
      carrier      : ∣ Γ ∣ → Set j
      equality     : {γ₀ γ₁ : ∣ Γ ∣} → [ Γ ] γ₀ ~ γ₁ → carrier γ₀ → carrier γ₁ → Prop j
      reflexivity  : {γ     : ∣ Γ ∣}{c : carrier γ} → equality ([ Γ ]⟳) c c
      symmetry     : {γ₀ γ₁ : ∣ Γ ∣}{c₀ : carrier γ₀}{c₁ : carrier γ₁} →
                     (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → equality γ₀~γ₁ c₀ c₁ → equality ([ Γ ]↔ γ₀~γ₁) c₁ c₀
      transitivity : {γ₀ γ₁ γ₂ : ∣ Γ ∣}{c₀ : carrier γ₀}{c₁ : carrier γ₁}{c₂ : carrier γ₂} →
                     (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (γ₁~γ₂ : [ Γ ] γ₁ ~ γ₂) →
                     equality γ₀~γ₁ c₀ c₁ → equality γ₁~γ₂ c₁ c₂ → equality ([ Γ ] γ₀~γ₁ » γ₁~γ₂) c₀ c₂
      coercion     : {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → carrier γ₀ → carrier γ₁
      coherence    : {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (c₀ : carrier γ₀) →
                     equality γ₀~γ₁ c₀ (coercion γ₀~γ₁ c₀)
  open Ty public renaming
    ( carrier      to ∣_∣
    ; equality     to infix 5 [_]_⇒_~_
    ; reflexivity  to [_]⟳
    ; symmetry     to [_]↔_⇒_
    ; transitivity to [_]_»_⇒_»_
    ; coercion     to [_]↷_⇒_
    ; coherence    to [_]≈_⇒_
    )

  -- module mkTy≡ where
  --   carrier : (Γ : Con) → (j : Level) → Set (# Γ # ⊔ lsuc j)
  --   carrier Γ j = ∣ Γ ∣ → Set j

  --   equality : (∣A∣ : carrier Γ j) → Set (# Γ # ⊔ lsuc j)
  --   equality {Γ} {j = j} ∣A∣ = {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → ∣A∣ γ₀ → ∣A∣ γ₁ → Prop j

  --   reflexivity : (∣A∣ : carrier Γ j) → (A~ : equality {Γ} ∣A∣) → Prop (# Γ # ⊔ j)
  --   reflexivity {Γ = Γ} ∣A∣ A~ = {γ : ∣ Γ ∣}{c : ∣A∣ γ} → A~ ([ Γ ]⟳) c c

  --   symmetry : (∣A∣ : carrier Γ j) → (A~ : equality {Γ} ∣A∣) → Prop (# Γ # ⊔ j)
  --   symmetry {Γ = Γ} ∣A∣ A~ =
  --     {γ₀ γ₁ : ∣ Γ ∣}{c₀ : ∣A∣ γ₀}{c₁ : ∣A∣ γ₁} →
  --     (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → A~ γ₀~γ₁ c₀ c₁ → A~ ([ Γ ]↔ γ₀~γ₁) c₁ c₀

  --   transitivity : (∣A∣ : carrier Γ j) → (A~ : equality {Γ} ∣A∣) → Prop (# Γ # ⊔ j)
  --   transitivity {Γ = Γ} ∣A∣ A~ =
  --     {γ₀ γ₁ γ₂ : ∣ Γ ∣}{c₀ : ∣A∣ γ₀}{c₁ : ∣A∣ γ₁}{c₂ : ∣A∣ γ₂} →
  --     (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (γ₁~γ₂ : [ Γ ] γ₁ ~ γ₂) →
  --     A~ γ₀~γ₁ c₀ c₁ → A~ γ₁~γ₂ c₁ c₂ → A~ ([ Γ ] γ₀~γ₁ » γ₁~γ₂) c₀ c₂

  --   coercion : (∣A∣ : carrier Γ j) → Set (# Γ # ⊔ j)
  --   coercion {Γ} ∣A∣ = {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → ∣A∣ γ₀ → ∣A∣ γ₁

  --   coherence : (∣A∣ : carrier Γ j) → (A~ : equality {Γ} ∣A∣) → (A↷ : coercion {Γ} ∣A∣) → Prop (# Γ # ⊔ j)
  --   coherence {Γ = Γ} ∣A∣ A~ A↷ = {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (c₀ : ∣A∣ γ₀) →
  --                                 A~ γ₀~γ₁ c₀ (A↷ γ₀~γ₁ c₀)

  --   mkTy≡ :
  --     (∣A∣ ∣B∣ : ∣ Γ ∣ → Set i) → (∣=∣ : ∣A∣ ≡ ∣B∣) →

  --     (A~ : equality {Γ} ∣A∣) →
  --     (B~ : equality {Γ} ∣B∣) →
  --     (_≡_ {A = equality {Γ} ∣B∣} (L.subst {A = carrier Γ _} (equality {Γ}) ∣=∣ A~) B~) →

  --     (A⟳ : reflexivity {Γ} ∣A∣ A~) →
  --     (B⟳ : reflexivity {Γ} ∣B∣ B~) →

  --     (A↔ : symmetry {Γ} ∣A∣ A~) →
  --     (B↔ : symmetry {Γ} ∣B∣ B~) →

  --     (A» : transitivity {Γ} ∣A∣ A~) →
  --     (B» : transitivity {Γ} ∣B∣ B~) →

  --     (A↷ : coercion {Γ} ∣A∣) →
  --     (B↷ : coercion {Γ} ∣B∣) →
  --     (_≡_ {A = coercion {Γ} ∣B∣} (L.subst {A = carrier Γ _} (coercion {Γ}) ∣=∣ A↷) B↷) →

  --     (A≈ : coherence {Γ} ∣A∣ A~ A↷) →
  --     (B≈ : coherence {Γ} ∣B∣ B~ B↷) →

  --     _≡ω_ {A = Ty Γ i}
  --       (record
  --         { carrier      = ∣A∣
  --         ; equality     = A~
  --         ; reflexivity  = A⟳
  --         ; symmetry     = A↔
  --         ; transitivity = A»
  --         ; coercion     = A↷
  --         ; coherence    = A≈
  --       })
  --       (record
  --         { carrier      = ∣B∣
  --         ; equality     = B~
  --         ; reflexivity  = B⟳
  --         ; symmetry     = B↔
  --         ; transitivity = B»
  --         ; coercion     = B↷
  --         ; coherence    = B≈
  --       })

  --   mkTy≡ {Γ} {i} ∣A∣ _ L.refl A~ _ L.refl A⟳ B⟳ A↔ B↔ A» B» A↷ _ L.refl A≈ B≈ = L.reflω

  -- open mkTy≡ using (mkTy≡) public

  variable
    A B C : Ty Γ i

  _[_]T : (A : Ty Δ i) → (σ : Sub Γ Δ) → Ty Γ i
  ∣ A [ σ ]T ∣ γ                             = ∣ A ∣ (∣ σ ∣ γ)
  [ A [ σ ]T ] γ₀~γ₁ ⇒ a₀ ~ a₁               = [ A ] ([ σ ]≈ γ₀~γ₁) ⇒ a₀ ~ a₁
  [ A [ σ ]T ]⟳                              = [ A ]⟳
  [ A [ σ ]T ]↔ γ₀~γ₁ ⇒ a₀~a₁                = [ A ]↔ ([ σ ]≈ γ₀~γ₁) ⇒ a₀~a₁
  [ A [ σ ]T ] γ₀~γ₁ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂ = [ A ] ([ σ ]≈ γ₀~γ₁) » ([ σ ]≈ γ₁~γ₂) ⇒ a₀~a₁ » a₁~a₂
  [ A [ σ ]T ]↷ γ₀~γ₁ ⇒ a₀                   = [ A ]↷ ([ σ ]≈ γ₀~γ₁) ⇒ a₀
  [ A [ σ ]T ]≈ γ₀~γ₁ ⇒ a₀~a₁                = [ A ]≈ ([ σ ]≈ γ₀~γ₁) ⇒ a₀~a₁

  [∘]T : A [ σ ∘ δ ]T ≡ω A [ σ ]T [ δ ]T
  [∘]T = L.reflω

  [id]T : A [ id ]T ≡ω A
  [id]T = L.reflω


  record Tm (Γ : Con) (A : Ty Γ j) : Setω where
    field
      carrier      : (γ     : ∣ Γ ∣) → ∣ A ∣ γ
      preservation : {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → [ A ] γ₀~γ₁ ⇒ (carrier γ₀) ~ (carrier γ₁)
  open Tm public renaming
    ( carrier      to ∣_∣
    ; preservation to infix 5 [_]≈_
    )

  variable
    t u v : Tm Γ A

  _[_]t : (t : Tm Δ A) → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  ∣ t [ σ ]t ∣ γ      = ∣ t ∣ ((∣ σ ∣ γ))
  [ t [ σ ]t ]≈ γ₀~γ₁ = [ t ]≈ ([ σ ]≈ γ₀~γ₁)

  tm[] : (a : Tm Δ A) → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
  tm[] {A = A} = _[_]t {A = A}

  syntax tm[] {A = A} a σ = a ⟨ A ⟩[ σ ]t

  [∘]t : t [ σ ∘ δ ]t ≡ω t [ σ ]t [ δ ]t
  [∘]t = L.reflω

  [id]t : {t : Tm {j} Γ A} → t [ id ]t ≡ω t
  [id]t = L.reflω


module ContextExtension where
  open Typeformersₘ.Sigmaₘ
  open Typeformersₚₘ.Sigmaₚₘ

  open Category
  open Families

  infixl 5 _▹_
  _▹_ : (Γ : Con) → (A : Ty Γ j) → Con
  # _▹_ {j = j} Γ A #                 = # Γ # ⊔ j
  ∣ Γ ▹ A ∣                           = Σₘ ∣ Γ ∣ ∣ A ∣
  [ Γ ▹ A ] (γ₀ ,Σₘ a₀) ~ (γ₁ ,Σₘ a₁) = Σₚₘ ([ Γ ] γ₀ ~ γ₁) (λ γ₀~γ₁ → [ A ] γ₀~γ₁ ⇒ a₀ ~ a₁)
  [ Γ ▹ A ]⟳                          = [ Γ ]⟳ ,Σₚₘ [ A ]⟳
  [ Γ ▹ A ]↔ γa₀~γa₁                  = ([ Γ ]↔ π₁ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₀~γa₁) ,Σₚₘ
                                        ([ A ]↔ π₁ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₀~γa₁ ⇒ π₂ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₀~γa₁)
  [ Γ ▹ A ] γa₀~γa₁ » γa₁~γa₂         = ([ Γ ] π₁ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₀~γa₁ » π₁ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₁~γa₂) ,Σₚₘ
                                        ([ A ] π₁ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₀~γa₁ » π₁ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₁~γa₂ ⇒
                                               π₂ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₀~γa₁ » π₂ₚₘ {Bₚ = ([ A ]_⇒ _ ~ _)} γa₁~γa₂)
  -- [ Γ ▹ A ]↔ (γ₀~γ₁ ,Σₘₚ a₀~a₁)                     =      ([ Γ ]↔ γ₀~γ₁)        ,Σₚₘ ([ A ]↔ γ₀~γ₁ ⇒ a₀~a₁)
  -- [ Γ ▹ A ] (γ₀~γ₁ ,Σₘₚ a₀~a₁) » (γ₁~γ₂ ,Σₘₚ a₁~a₂) =      ([ Γ ] γ₀~γ₁ » γ₁~γ₂) ,Σₚₘ ([ A ] γ₀~γ₁ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂)

  infixl 5 _,_
  _,_ : (σ : Sub Γ Δ) → (t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▹ A)
  ∣ σ , t ∣ γ      = (∣ σ ∣ γ)      ,Σₘ  (∣ t ∣ γ)
  [ σ , t ]≈ γ₀~γ₁ = ([ σ ]≈ γ₀~γ₁) ,Σₚₘ ([ t ]≈ γ₀~γ₁)

  -- _,⟨_⟩_ : (σ : Sub Γ Δ) → (A : Ty Δ i) → (t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▹ A)
  -- σ ,⟨ A ⟩ t = _,_ {A = A} σ t

  con, : (σ : Sub Γ Δ) → (t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▹ A)
  con, {A = A} = _,_ {A = A}

  infixl 5 con,
  syntax con, {A = A} σ t = σ ,⟨ A ⟩ t

  p : Sub (Γ ▹ A) Γ
  ∣ p ∣ γa       = π₁ₘ γa
  [ p {A = A} ]≈ γa₀~γa₁ = π₁ₚₘ {Bₚ = [ A ]_⇒ _ ~ _} γa₀~γa₁

  syntax p {A = A} = p⟨ A ⟩

  -- p² : Sub (Γ ▹ A ▹ B) Γ
  -- p² {A = A} {B = B} = p⟨ A ⟩ ∘ p⟨ B ⟩

  q : Tm (Γ ▹ A) (A [ p⟨ A ⟩ ]T)
  ∣ q ∣ (γa)           = π₂ₘ γa
  [ q {A = A} ]≈ γa₀~γa₁ = π₂ₚₘ {Bₚ = [ A ]_⇒ _ ~ _} γa₀~γa₁

  syntax q {A = A} = q⟨ A ⟩

  ▷β₁ : p⟨ A ⟩ ∘ (σ ,⟨ A ⟩ t) ≡ω σ
  ▷β₁ = L.reflω

  ▷β₂ : q⟨ A ⟩ [ σ ,⟨ A ⟩ t ]t ≡ω t
  ▷β₂ = L.reflω

  ▹η : p⟨ A ⟩ ,⟨ A ⟩ q⟨ A ⟩ ≡ω id
  ▹η = L.reflω

  ▹η' : (p⟨ A ⟩ ∘ (σ ,⟨ A ⟩ t)) ,⟨ A ⟩ (q⟨ A ⟩ [ σ ,⟨ A ⟩ t ]t) ≡ω σ ,⟨ A ⟩ t
  ▹η' = L.reflω

  ,∘ : (σ ,⟨ A ⟩ t) ∘ δ ≡ω (σ ∘ δ ,⟨ A ⟩ (t [ δ ]t))
  ,∘ = L.reflω

  _^ : (σ : Sub Γ Δ) → Sub (Γ ▹ A [ σ ]T) (Δ ▹ A)
  _^ {A = A} σ = (σ ∘ p⟨ A [ σ ]T ⟩) ,⟨ A ⟩ q⟨ A [ σ ]T ⟩

  sub^ : (σ : Sub Γ Δ) → Sub (Γ ▹ A [ σ ]T) (Δ ▹ A)
  sub^ {A = A} = _^ {A = A}

  syntax sub^ {A = A} σ = σ ^⟨ A ⟩

module Lifting where
  open CwFₘ.SizeLevelLiftingₘ
  open Propositionsₘ.SizeLevelLiftingₚₘ

  open Category
  open Families

  Lift : Ty Γ i → Ty Γ (i ⊔ j)
  ∣ Lift {j = j} A ∣ γ₀                    = Liftₘ  {j = j} (∣ A ∣ γ₀)
  [ Lift {j = j} A ] γ₀~γ₁ ⇒ a₀ ~ a₁       = Liftₚₘ {j = j} ([ A ] γ₀~γ₁ ⇒ lowerₘ a₀ ~ lowerₘ a₁)
  [ Lift A ]⟳                              = liftₚₘ [ A ]⟳
  [ Lift A ]↔ γ₀~γ₁ ⇒ a₀~a₁                = liftₚₘ ([ A ]↔ γ₀~γ₁ ⇒ lowerₚₘ a₀~a₁)
  [ Lift A ] γ₀~γ₁ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂ = liftₚₘ ([ A ] γ₀~γ₁ » γ₁~γ₂ ⇒ lowerₚₘ a₀~a₁ » lowerₚₘ a₁~a₂)
  [ Lift A ]↷ γ₀~γ₁ ⇒ a                    = liftₘ ([ A ]↷ γ₀~γ₁ ⇒ lowerₘ a)
  [ Lift A ]≈ γ₀~γ₁ ⇒ a                    = liftₚₘ ([ A ]≈ γ₀~γ₁ ⇒ lowerₘ a)

  Lift[] : Lift {j = j} A [ σ ]T ≡ω Lift {j = j} (A [ σ ]T)
  Lift[] = L.reflω

  lift : Tm Γ A → Tm Γ (Lift {j = j} A)
  ∣ lift t ∣  γ     = liftₘ  (∣ t ∣ γ)
  [ lift t ]≈ γ₀~γ₁ = liftₚₘ ([ t ]≈ γ₀~γ₁)

  lift[] : lift {j = j} t [ σ ]t ≡ω lift {j = j} (t [ σ ]t)
  lift[] = L.reflω

  lower : Tm Γ (Lift {j = j} A) → Tm Γ A
  ∣ lower t ∣  γ     = lowerₘ  (∣ t ∣ γ)
  [ lower t ]≈ γ₀~γ₁ = lowerₚₘ ([ t ]≈ γ₀~γ₁)

  Liftβ : lower (lift {A = A} {j = j} t) ≡ω t
  Liftβ = L.reflω

  Liftη : lift (lower {j = j} {A = A} t) ≡ω t
  Liftη = L.reflω
