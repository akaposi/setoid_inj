{-# OPTIONS --prop #-}

module Typeformersₚ where

open import Agda.Primitive

open import Lib as L using (_≡_ ; _≡ω_)

open import CWF
open import CWFₚ
open import Typeformers


module Emptyₚ where
  open Category
  open Families
  open Familiesₚ

  ⊥ₚ : Tyₚ Γ lzero
  ∣ ⊥ₚ ∣ γ = L.Liftₚ _ L.⊥ₚ
  [ ⊥ₚ ]↷ γ₀~γ₁ ⇒ e = e

  exfalso : Tm Γ (Expand ⊥ₚ) → Tm Γ A
  ∣ exfalso b ∣ γ = L.exfalso (L.lowerₚ (L.shrink (∣ b ∣ γ)))
  [_]≈_ (exfalso b) {γ₀} γ₀~γ₁ = L.exfalsoₚ (L.lowerₚ (L.shrink (∣ b ∣ γ₀)))

  ⊥ₚ[] : ⊥ₚ [ σ ]Tₚ ≡ω ⊥ₚ
  ⊥ₚ[] = L.reflω

  exfalso[] : {A : Ty Γ i} → exfalso {A = A} t [ σ ]t ≡ω exfalso t
  exfalso[] = L.reflω


module Unitₚ where
  open Category
  open Familiesₚ
  open Unit
  open Extraₚ

  ⊤ₚ : Tyₚ Γ lzero
  ⊤ₚ = Truncate ⊤

  ttₚ : Tmₚ Γ ⊤ₚ
  ttₚ = truncate tt

  ⊤[]ₚ : ⊤ₚ [ σ ]Tₚ ≡ω ⊤ₚ
  ⊤[]ₚ = L.reflω

  ttηₚ : {t : Tmₚ Γ ⊤ₚ} → t ≡ω ttₚ
  ttηₚ = L.reflω


module Sigmaₚ where
  open Category
  open Families
  open Familiesₚ
  open ContextExtension
  open ContextExtensionₚ
  open Extraₚ
  open Sigma

  Σₚ : (P : Tyₚ Γ i) → (Q : Tyₚ (Γ ▹ₚ P) j) → Tyₚ Γ (i ⊔ j)
  Σₚ P Q = Truncate (Σ (Expand P) (Expand Q))

  infix 5 _,Σₚ_
  _,Σₚ_ : (p : Tmₚ Γ P) → (q : Tmₚ Γ (Q [ id ,⟨ Expand P ⟩ p ]Tₚ)) → Tmₚ Γ (Σₚ P Q)
  _,Σₚ_ {P = P} {Q = Q} p q = truncate (p ,Σ⟨ Expand P ⟩⟨ Expand Q ⟩ q)

  sig,ₚ : (p : Tmₚ Γ P) → (q : Tmₚ Γ (Q [ id ,⟨ Expand P ⟩ p ]Tₚ)) → Tmₚ Γ (Σₚ P Q)
  sig,ₚ {P = P} {Q = Q} = _,Σₚ_ {P = P} {Q = Q}

  infix 5 sig,ₚ
  syntax sig,ₚ {P = P} {Q = Q} p q = p ,Σₚ⟨ P ⟩⟨ Q ⟩ q

  π₁ₚ : (t : Tmₚ Γ (Σₚ P Q)) → Tmₚ Γ P
  π₁ₚ {P = P} {Q = Q} t =
    let ΣPQ = Σ (Expand P) (Expand Q) in
    truncElim {A = ΣPQ} {P = P [ pₚ⟨ Σₚ P Q ⟩ ]Tₚ} (π₁ q⟨ ΣPQ ⟩) t

  π₂ₚ : (t : Tmₚ Γ (Σₚ P Q)) → Tmₚ Γ (Q [ id ,ₚ⟨ P ⟩ π₁ₚ {P = P} {Q = Q} t ]Tₚ)
  π₂ₚ {P = P} {Q = Q} t =
    let ΣPQ = Σ (Expand P) (Expand Q); EP = Expand P [ pₚ⟨ Truncate ΣPQ ⟩ ]T in
    truncElim
      {A = ΣPQ}
      {P =
        Q [ pₚ⟨ Σₚ P Q ⟩
          ,ₚ⟨ P ⟩
        π₁ₚ
          {P = P [ pₚ⟨ Truncate ΣPQ ⟩ ]Tₚ}
          {Q = Q [ pₚ⟨ Truncate ΣPQ ⟩ ∘ p⟨ EP ⟩ ,ₚ⟨ P ⟩ q⟨ EP ⟩ ]Tₚ}
          qₚ⟨ Truncate ΣPQ ⟩ ]Tₚ}
      (π₂ q⟨ ΣPQ ⟩)
      t

  Σₚ[] : (Σₚ P Q) [ σ ]Tₚ ≡ω Σₚ
           (P [ σ ]Tₚ)
           (Q [ σ ^ₚ⟨ P ⟩ ]Tₚ)
  Σₚ[] = L.reflω

  ,Σₚ[] : {σ : Sub Γ Δ}{x : Tmₚ {i} Δ P} → {y : Tmₚ Δ (Q [ id ,ₚ⟨ P ⟩ x ]Tₚ)} →
          (x ,Σₚ⟨ P ⟩⟨ Q ⟩ y) ⟨ Σₚ P Q ⟩[ σ ]tₚ ≡ω
          ((x ⟨ P ⟩[ σ ]tₚ) ,Σₚ⟨ P [ σ ]Tₚ ⟩⟨ Q [ σ ^ₚ⟨ P ⟩ ]Tₚ ⟩ (y [ σ ]t))
  ,Σₚ[] = L.reflω

  Σₚβ₁ : {x : Tmₚ Γ P}{y : Tmₚ Γ (Q [ id ,ₚ⟨ P ⟩ x ]Tₚ)} →
        --  π₁ₚ {P = P} {Q = Q} (_,Σₚ_ {P = P} {Q = Q} x y) ≡ω x
         π₁ₚ {P = P} {Q = Q} (x ,Σₚ⟨ P ⟩⟨ Q ⟩ y) ≡ω x
  Σₚβ₁ = L.reflω

  Σₚβ₂ : {x : Tmₚ Γ P}{y : Tmₚ Γ (Q [ id ,ₚ⟨ P ⟩ x ]Tₚ)} →
         π₂ₚ {P = P} {Q = Q} (x ,Σₚ⟨ P ⟩⟨ Q ⟩ y) ≡ω y
  Σₚβ₂ = L.reflω

  Σₚη : {x : Tmₚ Γ (Σₚ {i = i} {j = j} P Q)} →
        (π₁ₚ {P = P} {Q = Q} x) ,Σₚ⟨ P ⟩⟨ Q ⟩ (π₂ₚ {P = P} {Q = Q} x) ≡ω x
  Σₚη = L.reflω

  -- test : Tm Γ (Expand (Truncate))

  -- π₂ₚ : (t : Tmₚ Γ (Σₚ P Q)) → Tmₚ Γ (Q [ id ,ₚ⟨ P ⟩ (π₁ₚ {Q = Q} t) ]Tₚ)

  -- Sub (Γ ▹ Expand (Truncate (Σ (Expand P) (Expand Q)))) (Γ ▹ Expand P)
  -- Tm (Γ ▹ Expand (Truncate (Σ (Expand P) (Expand Q)))) (Expand P)

  -- let
  --   ΣPQ = Σ (Expand P) (Expand Q)
  --   EP = Expand P [ p⟨ ΣPQ ⟩ ]T
  -- in
  -- truncElim {A = ΣPQ} {P = P [ pₚ⟨ Σₚ P Q ⟩ ]Tₚ}
  -- (π₁
  --   {Γ = Γ ▹ ΣPQ}
  --   {A = EP}
  --   {B = (Expand Q) [ p⟨ ΣPQ ⟩ ∘ p⟨ EP ⟩ ,ₚ⟨ P ⟩ q⟨ EP ⟩ ]T}
  --   q⟨ ΣPQ ⟩)
  -- t

  -- {B = (Expand Q) [ p⟨ Σ (Expand P) (Expand Q) ⟩ ∘ p⟨ (Expand P [ p ]T) ⟩ ,⟨ ? ⟩ {!   !} ]T}
  -- π₁ₚ t = truncElim {! qₚ ?  !} tk

  -- Tmₚ (Γ ▹ Σ (Expand P) (Expand Q)) ((P [ pₚ ]Tₚ) [ con,ₚ p (expandTruncate q) ]Tₚ)
  -- Tm  (Γ ▹ Σ (Expand P) (Expand Q)) (Expand ((P [ pₚ ]Tₚ) [ con,ₚ p (expandTruncate q) ]Tₚ))
  -- Tm  (Γ ▹ Σ (Expand P) (Expand Q)) (Expand ((P [ pₚ ]Tₚ) [ con,ₚ p (expand (truncate q)) ]Tₚ))
  -- Tm  (Γ ▹ Σ (Expand P) (Expand Q)) (Expand ((P [ pₚ ]Tₚ) [ p ,ₚ (expand (truncate q)) ]Tₚ))
  -- Tm  (Γ ▹ Σ (Expand P) (Expand Q)) (Expand ((P [ p  ]Tₚ) [ p , truncate q ]Tₚ))

module Piₚ where
  open Category
  open Families
  open Familiesₚ
  open ContextExtension
  open Pi
  open Extraₚ

  Πₚ : (A : Ty Γ i) → (P : Tyₚ (Γ ▹ A) j) → Tyₚ Γ (i ⊔ j)
  Πₚ A P = Truncate (Π A (Expand P))

  lamₚ : Tmₚ (Γ ▹ A) P → Tmₚ Γ (Πₚ A P)
  lamₚ {A = A} x = expandTruncate (lam {A = A} x)

  -- test : Tm Γ A → Tm (Γ ▹ B ▹ C) (A [ p⟨ B ⟩ ∘ p⟨ C ⟩ ]T)
  -- test t = t [ p ∘ p ]t

  appₚ : Tmₚ Γ (Πₚ A P) → Tmₚ (Γ ▹ A) P
  appₚ {Γ = Γ} {A = A} {P = P} x =
    let
      funtype = (Π A (Expand P) [ p⟨ A ⟩ ]T)
      A' = A [ p⟨ A ⟩ ∘ p⟨ funtype ⟩ ]T
    in
    truncElim
      {A = funtype}
      {P = P [ p⟨ Expand (Truncate funtype) ⟩ ]Tₚ}
      {!   !}
      -- (q⟨ funtype ⟩
      --   $⟨ _ ⟩⟨
      --     ?
      --     -- Expand (P [
      --     --   -- {! p ∘ p  !}
      --     --   p⟨ funtype ⟩ ∘ ((p⟨ A ⟩ ∘ (p⟨ funtype ⟩ ∘ p⟨ A' ⟩))
      --     --     ,⟨ A ⟩
      --     --   (q⟨ A ⟩ ⟨ A [ p⟨ A ⟩ ]T ⟩[ p⟨ funtype ⟩ ∘ {! p⟨ ? ⟩  !} ]t)
      --     --   -- (q⟨ _ ⟩ ⟨ _ ⟩[ p⟨ funtype ⟩ ∘ p⟨ A' ⟩ ]t)
      --     --     ,⟨ funtype ⟩
      --     --   (q⟨ funtype ⟩ [ p⟨ A' ⟩ ]t))
      --     -- ]Tₚ)
      --   ⟩
      --  (q⟨ A ⟩ [ p⟨ funtype ⟩ ]t))
      -- (app {A = A [ p⟨ A ⟩ ∘ p⟨ funtype ⟩ ]T} (q {Γ = Γ ▹ A} {A = funtype}) ⟨ {!   !} ⟩[ id ,⟨ (A [ p⟨ A ⟩ ∘ p⟨ funtype ⟩ ]T) ⟩ (q {Γ = Γ} {A = A}) ⟨ (A [ p⟨ A ⟩ ]T) ⟩[ p⟨ funtype ⟩ ]t ]t)
      -- (q $ (q [ p ]t))
      -- app {A = A} f ⟨ B ⟩[ id ,⟨ A ⟩ a ]t
      -- (q⟨ funtype ⟩ $⟨ _ ⟩⟨ Expand (P [ p⟨ A ⟩ ∘ p⟨ funtype ⟩ ∘ p , q⟨ A ⟩ [ p⟨ funtype ⟩ ∘ p⟨ _ ⟩ ]t ]Tₚ) ⟩ (q⟨ A ⟩ [ p⟨ funtype ⟩ ]t))
      (x [ p⟨ A ⟩ ]t)
        -- test : Sub
        --   (Γ ▹ A ▹ (Π A (Expand P) [ p ]T) ▹
        --   record
        --   { carrier = λ z → ∣ A ∣ (L.π₁ (L.π₁ z))
        --   ; equality = λ {γ₀} {γ₁} z z₁ z₂ → [ A ] _ ⇒ z₁ ~ z₂
        --   ; reflexivity = _
        --   ; symmetry = λ γ₀~γ₁ a₀~a₁ → _
        --   ; transitivity = λ γ₀~γ₁ γ₁~γ₂ a₀~a₁ a₁~a₂ → _
        --   ; coercion = λ γ₀~γ₁ a₀ → [ A ]↷ _ ⇒ a₀
        --   ; coherence = λ γ₀~γ₁ a₀~a₁ → _
        --   })
        --   (Γ ▹ A ▹ (Π A (Expand P) [ p ]T))
        -- test = {!   !}
  -- appₚ {Γ = Γ} {A = A} {P = P} x = truncElim (q $ (q [ p ]t)) (x [ p ]t)

  -- appₚ : Tmₚ Γ (Πₚ A P) → Tmₚ (Γ ▹ A) P
  -- ∣ appₚ {Γ = Γ} {A = A} {P = P} x ∣ (γ L., a) =
  --   L.expand (L.truncElim (λ { (bar L.,ₛₚ tilde) → L.shrink (bar a) }) (L.shrink (∣ x ∣ γ)))
  -- [ appₚ x ]≈ γ₀~γ₁ = L.liftₚ L.ttₚ

  -- appₚ : Tmₚ Γ (Πₚ A P) → Tmₚ (Γ ▹ A) P
  -- ∣ appₚ {Γ = Γ} {A = A} {P = P} x ∣ γa =
  --    L.expand (L.truncElim (λ t → L.shrink ((L.π₁ₛₚ t) (L.π₂ γa))) (L.shrink (∣ x   ∣ (L.π₁ γa))))
  -- -- L.expand (L.truncElim (λ a → L.shrink (∣ a→p ∣ (γ L., a)))    (L.shrink (∣ tra ∣ γ        )))
  -- [ appₚ x ]≈ γ₀~γ₁ = L.liftₚ L.ttₚ

  -- appₚ : Tmₚ Γ (Πₚ A P) → Tmₚ (Γ ▹ A) P
  -- ∣ appₚ {Γ = Γ} {A = A} {P = P} x ∣ γa =
  -- -- L.expand (L.truncElim (λ t → L.shrink ((λ γat → (L.π₁ₛₚ (L.π₂ γat)) (L.π₂ (L.π₁ γat))) (γa L., t))) (L.shrink (∣ x   ∣ (L.π₁ γa))))
  --    L.expand (L.truncElim (λ t → L.shrink (∣ test ∣ (γa L., t))) (L.shrink (∣ x [ p⟨ A ⟩ ]t  ∣  γa))) where
  --      test : Tmₚ (Γ ▹ A ▹ (Π A (Expand P) [ p⟨ A ⟩ ]T)) (P [ p ]Tₚ)
  --      test = q $ (q [ p ]t)
  -- -- L.expand (L.truncElim (λ a → L.shrink (∣ a→p ∣                                         (γ  L., a))) (L.shrink (∣ tra ∣ γ        )))
  -- [ appₚ x ]≈ γ₀~γ₁ = L.liftₚ L.ttₚ

  -- L.expand (L.truncElim (λ   a →                 L.shrink (∣ a→p ∣ (γ L., a))) (L.shrink (∣ tra ∣ γ)))
  -- L.expand (L.truncElim (λ { (bar L.,ₛₚ tilde) → L.shrink (bar a) })           (L.shrink (∣ x ∣ γ)))

  -- appₚ : Tm Γ (Expand (Truncate (Π A (Expand P)))) → Tm Γ (Π A (Expand P))
  -- appₚ : Tmₚ Γ (Πₚ A P) → Tm Γ (Π A (Expand P))
  -- appₚ : Tmₚ Γ (Truncate (Π A (Expand P))) → Tmₚ (Γ ▹ A) P
  -- appₚ : Tmₚ Γ (Πₚ A P) → Tmₚ (Γ ▹ A) P
  -- appₚ x = truncElim {A = {!   !}} {P = {! P  !}} {!   !} {!   !}
  -- appₚ {A = A} {P = P} x = app {A = A} {!  !} where
  --   test = truncElim {A = (Π A (Expand P))} {P = {!   !}} {!   !} x
  -- appₚ {A = A} {P = P} x = (truncElim {A = (Π A (Expand P))} {P = P [ {!   !} ]Tₚ} {!   !} x) [ {! p  !} ]tₚ

  -- truncElim : (Tmₚ (Γ ▹ (Π A (Expand P))) (Q [ p⟨ (Π A (Expand P)) ⟩ ,ₚ⟨ Truncate (Π A (Expand P)) ⟩ expandTruncate q⟨ (Π A (Expand P)) ⟩ ]Tₚ)) →
  --             ((tra : Tmₚ Γ (Truncate (Π A (Expand P)))) → Tmₚ Γ (Q [ id ,ₚ⟨ Truncate (Π A (Expand P)) ⟩ tra ]Tₚ))

  -- Q [...] = P

  -- truncElim : (Tmₚ (Γ ▹ A) (P [ p⟨ A ⟩ ,ₚ⟨ Truncate A ⟩ expandTruncate q⟨ A ⟩ ]Tₚ)) →
  --             ((tra : Tmₚ Γ (Truncate A)) → Tmₚ Γ (P [ id ,ₚ⟨ Truncate A ⟩ tra ]Tₚ))

  -- appₚ' : Tm Γ (Π A (Expand P)) → Tmₚ (Γ ▹ A) P
  -- appₚ : Tmₚ Γ (Πₚ A P) → Tmₚ (Γ ▹ A) P
  -- appₚ x = truncElim {A = {!   !}} {P = {! P  !}} {!   !} {!   !}
  -- appₚ' {A = A} x = app {A = A} x

  -- | Id (Σ A B) | = (Σ A B)~
  --  (Σ A B)~ = ?
  -- | Id (Σ A B) | = pointwise


  -- [_]_⇒_~_ (Π A B) {γ₀} {γ₁} γ₀~γ₁ (f₀ L.,ₛₚ f₀~) (f₁ L.,ₛₚ f₁~) =
  --   {a₀ : ∣ A ∣ γ₀}{a₁ : ∣ A ∣ γ₁}(a₀~a₁ : [ A ] γ₀~γ₁ ⇒ a₀ ~ a₁) →
  --   [ B ] γ₀~γ₁ L.,ₚ a₀~a₁ ⇒ f₀ a₀ ~ f₁ a₁
