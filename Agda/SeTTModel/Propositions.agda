{-# OPTIONS --prop #-}

module CWFₚ where

open import Agda.Primitive

open import Lib as L using (_≡_ ; _≡ω_)

open import CWF

module Familiesₚ where
  open Category
  open Families
  open ContextExtension

  record Tyₚ (Γ : Con) (j : Level) : Setω where
    field
      carrier : ∣ Γ ∣ → Prop j
      coercion : {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → carrier γ₀ → carrier γ₁
  open Tyₚ public renaming
    ( carrier to ∣_∣
    ; coercion to [_]↷_⇒_
    )

  -- TODO: Rename to X Y Z?
  variable
    P Q R : Tyₚ Γ i

  _[_]Tₚ : Tyₚ Δ i → Sub Γ Δ → Tyₚ Γ i
  ∣ P [ σ ]Tₚ ∣ γ           = ∣ P ∣ (∣ σ ∣ γ)
  [ P [ σ ]Tₚ ]↷ γ₀~γ₁ ⇒ a₀ = [ P ]↷ ([ σ ]≈ γ₀~γ₁) ⇒ a₀

  [∘]Tₚ : P [ σ ∘ δ ]Tₚ ≡ω P [ σ ]Tₚ [ δ ]Tₚ
  [∘]Tₚ = L.reflω

  [id]Tₚ : P [ id ]Tₚ ≡ω P
  [id]Tₚ = L.reflω

  -- TODO: Rename to Embed?
  Expand : (P : Tyₚ Γ i) → Ty Γ i
  ∣ Expand P ∣ γ             = L.Expand (∣ P ∣ γ)
  [ Expand P ] _ ⇒ _ ~ _     = L.Liftₚ _ L.⊤ₚ
  [ Expand P ]⟳              = L.liftₚ L.ttₚ
  [ Expand P ]↔ _ ⇒ _        = L.liftₚ L.ttₚ
  [ Expand P ] _ » _ ⇒ _ » _ = L.liftₚ L.ttₚ
  [ Expand P ]↷ γ₀~γ₁ ⇒ p₀   = L.expand ([ P ]↷ γ₀~γ₁ ⇒ (L.shrink p₀))
  [ Expand P ]≈ _ ⇒ _        = L.liftₚ L.ttₚ

  Tmₚ : (Γ : Con) → (P : Tyₚ Γ j) → Setω
  Tmₚ Γ P = Tm Γ (Expand P)

  variable
    x y z : Tmₚ Γ P

  _[_]tₚ : (x : Tmₚ Δ P) → (σ : Sub Γ Δ) → Tmₚ Γ (P [ σ ]Tₚ)
  _[_]tₚ = _[_]t

  tmₚ[] : (x : Tmₚ Δ P) → (σ : Sub Γ Δ) → Tmₚ Γ (P [ σ ]Tₚ)
  tmₚ[] {P = P} = _[_]tₚ {P = P}

  syntax tmₚ[] {P = P} x σ = x ⟨ P ⟩[ σ ]tₚ

  [∘]tₚ : x ⟨ P ⟩[ σ ∘ δ ]tₚ ≡ω x ⟨ P ⟩[ σ ]tₚ ⟨ P [ σ ]Tₚ ⟩[ δ ]tₚ
  [∘]tₚ = L.reflω

  [id]tₚ : {x : Tmₚ {i} Γ P} → x ⟨ P ⟩[ id ]tₚ ≡ω x
  [id]tₚ = L.reflω


module ContextExtensionₚ where
  open Category
  open Familiesₚ
  open ContextExtension

  infixl 5 _▹ₚ_
  _▹ₚ_ : (Γ : Con) → (P : Tyₚ Γ j) → Con
  Γ ▹ₚ P = Γ ▹ (Expand P)

  -- TODO: Is this unnecessary?
  infix 5 _,ₚ_
  _,ₚ_ : (σ : Sub Γ Δ) → (t : Tmₚ Γ (P [ σ ]Tₚ)) → Sub Γ (Δ ▹ₚ P)
  _,ₚ_ {P = P} σ t = σ ,⟨ Expand P ⟩ t

  con,ₚ : (σ : Sub Γ Δ) → (t : Tmₚ Γ (P [ σ ]Tₚ)) → Sub Γ (Δ ▹ₚ P)
  con,ₚ {P = P} = _,ₚ_ {P = P}

  infix 5 con,ₚ
  syntax con,ₚ {P = P} σ t = σ ,ₚ⟨ P ⟩ t

  pₚ : Sub (Γ ▹ₚ P) Γ
  pₚ {P = P} = p⟨ Expand P ⟩

  syntax pₚ {P = P} = pₚ⟨ P ⟩

  qₚ : Tmₚ (Γ ▹ₚ P) (P [ pₚ⟨ P ⟩ ]Tₚ)
  qₚ {P = P} = q⟨ Expand P ⟩

  syntax qₚ {P = P} = qₚ⟨ P ⟩

  ▷ₚβ₁ : pₚ⟨ P ⟩ ∘ (σ ,ₚ⟨ P ⟩ x) ≡ω σ
  ▷ₚβ₁ = L.reflω

  ▷ₚβ₂ : qₚ⟨ P ⟩ ⟨ P [ pₚ⟨ P ⟩ ]Tₚ ⟩[ σ ,ₚ⟨ P ⟩ x ]tₚ ≡ω x
  ▷ₚβ₂ = L.reflω

  ▹ₚη : pₚ⟨ P ⟩ ,ₚ⟨ P ⟩ qₚ⟨ P ⟩ ≡ω id
  ▹ₚη = L.reflω

  ▹ₚη' : (pₚ⟨ P ⟩ ∘ (σ ,ₚ⟨ P ⟩ x)) ,ₚ⟨ P ⟩ (qₚ⟨ P ⟩ ⟨ P [ pₚ⟨ P ⟩ ]Tₚ ⟩[ σ ,ₚ⟨ P ⟩ x ]tₚ) ≡ω σ ,ₚ⟨ P ⟩ x
  ▹ₚη' = L.reflω

  _^ₚ : (σ : Sub Γ Δ) → Sub (Γ ▹ₚ P [ σ ]Tₚ) (Δ ▹ₚ P)
  _^ₚ {P = P} σ = (σ ∘ pₚ⟨ P [ σ ]Tₚ ⟩) ,ₚ⟨ P ⟩ qₚ⟨ P [ σ ]Tₚ ⟩

  sub^ₚ : (σ : Sub Γ Δ) → Sub (Γ ▹ₚ P [ σ ]Tₚ) (Δ ▹ₚ P)
  sub^ₚ {P = P} = _^ₚ {P = P}

  syntax sub^ₚ {P = P} σ = σ ^ₚ⟨ P ⟩


module Extraₚ where
  open Category
  open Families
  open Familiesₚ
  open ContextExtension
  open ContextExtensionₚ

  -- See Families for definition
  -- Expand : (P : Tyₚ Γ i) → Ty Γ i

  Expand[] : Expand P [ σ ]T ≡ω Expand (P [ σ ]Tₚ)
  Expand[] = L.reflω

  expand : Tmₚ Γ P → Tm Γ (Expand P)
  expand t = t

  irrelevance : {t u : Tm Γ (Expand P)} → u ≡ω v
  irrelevance = L.reflω


  Truncate : (A : Ty Γ i) → Tyₚ Γ i
  ∣ Truncate A ∣ γ                      = L.Truncate (∣ A ∣ γ)
  [ Truncate A ]↷ γ₀~γ₁ ⇒ L.truncate p₀ = L.truncate ([ A ]↷ γ₀~γ₁ ⇒ p₀)

  Truncate[] : Truncate A [ σ ]Tₚ ≡ω Truncate (A [ σ ]T)
  Truncate[] = L.reflω

  truncate : Tm Γ A → Tmₚ Γ (Truncate A)
  ∣ truncate t ∣ γ  = L.expand (L.truncate (∣ t ∣ γ))
  [ truncate t ]≈ _ = L.liftₚ L.ttₚ


  expandTruncate : Tm Γ A → Tm Γ (Expand (Truncate A))
  expandTruncate {A = A} t = expand {P = Truncate A} (truncate t)

  truncElim : {A : Ty Γ i} → {P : Tyₚ (Γ ▹ₚ Truncate A) j} →
              (Tmₚ (Γ ▹ A) (P [ p⟨ A ⟩ ,ₚ⟨ Truncate A ⟩ expandTruncate q⟨ A ⟩ ]Tₚ)) →
              ((tra : Tmₚ Γ (Truncate A)) → Tmₚ Γ (P [ id ,ₚ⟨ Truncate A ⟩ tra ]Tₚ))
  ∣ truncElim a→p tra ∣ γ  = L.expand (L.truncElim (λ a → L.shrink (∣ a→p ∣ (γ L., a))) (L.shrink (∣ tra ∣ γ)))
  [ truncElim a→p tra ]≈ _ = L.liftₚ L.ttₚ

  truncElim[] : {A : Ty Γ j}{P : Tyₚ (Γ ▹ₚ Truncate A) k} →
                {a→p : Tmₚ (Γ ▹ A) (P [ p⟨ A ⟩ ,ₚ⟨ Truncate A ⟩ expandTruncate q⟨ A ⟩ ]Tₚ)} →
                {tra : Tmₚ Γ (Truncate A)} →
                truncElim
                  {A = A}
                  {P = P}
                  a→p
                  tra
                  ⟨ (P [ id ,ₚ⟨ Truncate A ⟩ tra ]Tₚ) ⟩[ σ ]tₚ
                  ≡ω
                truncElim
                  {A = A [ σ ]T}
                  {P = P [ σ ^ₚ⟨ Truncate A ⟩ ]Tₚ}
                  (a→p ⟨ ((P [ p⟨ A ⟩ ,ₚ⟨ Truncate A ⟩ expandTruncate q⟨ A ⟩ ]Tₚ)) ⟩[ σ ^⟨ A ⟩ ]tₚ)
                  (tra ⟨ Truncate A ⟩[ σ ]tₚ)
  truncElim[] = L.reflω

  truncRec : Tmₚ (Γ ▹ A) (P [ p⟨ A ⟩ ]Tₚ) → Tmₚ Γ (Truncate A) → Tmₚ Γ P
  truncRec {A = A} {P = P} = truncElim {A = A} {P = P [ pₚ⟨ Truncate A ⟩ ]Tₚ}
