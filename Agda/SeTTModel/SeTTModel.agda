{-# OPTIONS --prop --rewriting #-}

module SeTTModel where

open import Agda.Primitive
open import CWF
open import Typeformers
open import Propositions
