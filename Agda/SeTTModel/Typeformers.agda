{-# OPTIONS --prop #-}

module Typeformers where

open import Agda.Primitive

open import Lib as L using (_≡_ ; _≡ω_ ; if_then_else_)

open import CWF
open import CWFₚ

module Empty where
  open Category
  open Families

  ⊥ : Ty Γ lzero
  ∣ ⊥ ∣ γ = {!   !}
  [_]_⇒_~_ ⊥ = {!   !}
  [_]⟳ ⊥ = {!   !}
  [_]↔_⇒_ ⊥ = {!   !}
  [_]_»_⇒_»_ ⊥ = {!   !}
  [_]↷_⇒_ ⊥ = {!   !}
  [_]≈_⇒_ ⊥ = {!   !}

module Unit where
  open Category
  open Families

  ⊤ : Ty Γ lzero
  ∣ ⊤ ∣ _             = L.⊤
  [ ⊤ ] _ ⇒ _ ~ _     = L.⊤ₚ
  [ ⊤ ]⟳              = L.ttₚ
  [ ⊤ ]↔ _ ⇒ _        = L.ttₚ
  [ ⊤ ] _ » _ ⇒ _ » _ = L.ttₚ
  [ ⊤ ]↷ _ ⇒ _        = L.tt
  [ ⊤ ]≈ _ ⇒ _        = L.ttₚ

  tt : Tm Γ ⊤
  ∣ tt ∣ _  = L.tt
  [ tt ]≈ _ = L.ttₚ

  ⊤[] : ⊤ [ σ ]T ≡ω ⊤
  ⊤[] = L.reflω

  ttη : {t : Tm Γ ⊤} → t ≡ω tt
  ttη = L.reflω


module Sigma where
  open Category
  open Families
  open ContextExtension

  Σ : (A : Ty Γ i) → (B : Ty (Γ ▹ A) j) → Ty Γ (i ⊔ j)

  ∣ Σ A B ∣ γ = L.Σ
          (∣ A ∣  γ)
    (λ a → ∣ B ∣ (γ L., a))

  [ Σ A B ] γ₀~γ₁ ⇒ (a₀ L., b₀) ~ (a₁ L., b₁) = L.Σₚ
               ([ A ]  γ₀~γ₁             ⇒ a₀ ~ a₁)
    (λ a₀~a₁ → ([ B ] (γ₀~γ₁ L.,ₚ a₀~a₁) ⇒ b₀ ~ b₁))

  [ Σ A B ]⟳ = L._,ₚ_
    [ A ]⟳
    [ B ]⟳

  [ Σ A B ]↔ γ₀~γ₁ ⇒ (a₀~a₁ L.,ₚ b₀~b₁) = L._,ₚ_
    ([ A ]↔  γ₀~γ₁             ⇒ a₀~a₁)
    ([ B ]↔ (γ₀~γ₁ L.,ₚ a₀~a₁) ⇒ b₀~b₁)

  [ Σ A B ] γ₀~γ₁ » γ₁~γ₂ ⇒ (a₀~a₁ L.,ₚ b₀~b₁) » (a₁~a₂ L.,ₚ b₁~b₂) = L._,ₚ_
    ([ A ]  γ₀~γ₁             »  γ₁~γ₂             ⇒ a₀~a₁ » a₁~a₂)
    ([ B ] (γ₀~γ₁ L.,ₚ a₀~a₁) » (γ₁~γ₂ L.,ₚ a₁~a₂) ⇒ b₀~b₁ » b₁~b₂)

  [ Σ A B ]↷ γ₀~γ₁ ⇒ (a₀ L., b₀) = L._,_
    ([ A ]↷  γ₀~γ₁                           ⇒ a₀)
    ([ B ]↷ (γ₀~γ₁ L.,ₚ ([ A ]≈ γ₀~γ₁ ⇒ a₀)) ⇒ b₀)

  [ Σ A B ]≈ γ₀~γ₁ ⇒ (a₀ L., b₀) = L._,ₚ_
    ([ A ]≈  γ₀~γ₁                           ⇒ a₀)
    ([ B ]≈ (γ₀~γ₁ L.,ₚ ([ A ]≈ γ₀~γ₁ ⇒ a₀)) ⇒ b₀)

  infix 5 _,Σ_
  _,Σ_ : (a : Tm {j} Γ A) → (b : Tm Γ (B [ id ,⟨ A ⟩ a ]T)) → Tm Γ (Σ A B)
  ∣ a ,Σ b ∣ γ      =  ∣ a ∣ γ       L., ∣ b ∣ γ
  [ a ,Σ b ]≈ γ₀~γ₁ = ([ a ]≈ γ₀~γ₁) L.,ₚ ([ b ]≈ γ₀~γ₁)

  sig, : (a : Tm {j} Γ A) → (b : Tm Γ (B [ id ,⟨ A ⟩ a ]T)) → Tm Γ (Σ A B)
  sig, {A = A} {B = B} = _,Σ_ {A = A} {B = B}

  infix 5 sig,
  syntax sig, {A = A} {B = B} a b = a ,Σ⟨ A ⟩⟨ B ⟩ b

  π₁ : (t : Tm Γ (Σ A B)) → Tm Γ A
  ∣ π₁ t ∣ γ      = L.π₁  (∣ t ∣ γ)
  [ π₁ t ]≈ γ₀~γ₁ = L.π₁ₚ ([ t ]≈ γ₀~γ₁)

  π₂ : (t : Tm Γ (Σ A B)) → Tm Γ (B [ id ,⟨ A ⟩ (π₁ {B = B} t) ]T)
  ∣ π₂ t ∣ γ      = L.π₂  (∣ t ∣ γ)
  [ π₂ t ]≈ γ₀~γ₁ = L.π₂ₚ ([ t ]≈ γ₀~γ₁)


  Σ[] : (Σ A B) [ σ ]T ≡ω Σ
          (A [ σ ]T)
          (B [ σ ^⟨ A ⟩ ]T)
  Σ[] = L.reflω

  π₁[] : {σ : Sub Γ Δ}{B : Ty (Γ ▹ A) j}{ab : Tm Γ (Σ A B)} →
         π₁ {A = A} {B = B} ab [ δ ]t ≡ω π₁ {A = A [ δ ]T} {B = B [ δ ^⟨ A ⟩ ]T} (ab [ δ ]t)
  π₁[] = L.reflω

  π₂[] : {σ : Sub Γ Δ}{B : Ty (Γ ▹ A) j}{ab : Tm Γ (Σ A B)} →
         π₂ {A = A} {B = B} ab [ δ ]t ≡ω π₂ {A = A [ δ ]T} {B = B [ δ ^⟨ A ⟩ ]T} (ab [ δ ]t)
  π₂[] = L.reflω

  ,Σ[] : {σ : Sub Γ Δ}{a : Tm {j} Δ A} → {b : Tm Δ (B [ id ,⟨ A ⟩ a ]T)} →
         (a ,Σ⟨ A ⟩⟨ B ⟩ b) [ σ ]t ≡ω
         ((a [ σ ]t) ,Σ⟨ A [ σ ]T ⟩⟨ B [ σ ^⟨ A ⟩ ]T ⟩ (b [ σ ]t))
  ,Σ[] = L.reflω

  Σβ₁ : {a : Tm {i} Γ A}{b : Tm {j} Γ (B [ id ,⟨ A ⟩ a ]T)} →
        -- π₁ {B = B} (_,Σ_ {B = B} a b) ≡ω a
        π₁ {B = B} (a ,Σ⟨ A ⟩⟨ B ⟩ b) ≡ω a
  Σβ₁ = L.reflω

  Σβ₂ : {a : Tm {i} Γ A}{b : Tm {j} Γ (B [ id ,⟨ A ⟩ a ]T)} →
        -- π₂ {A = A} {B = B} (_,Σ_ {B = B} a b) ≡ω b
        π₂ {A = A} {B = B} (a ,Σ⟨ A ⟩⟨ B ⟩ b) ≡ω b
  Σβ₂ = L.reflω

  Ση : {t : Tm Γ (Σ {i = j} {j = k} A B)} →
       (π₁ {B = B} t) ,Σ⟨ A ⟩⟨ B ⟩ (π₂ {A = A} {B = B} t) ≡ω t
  Ση = L.reflω


module Boolean where
  open Category
  open Families
  open ContextExtension

  Bool : Ty Γ lzero
  ∣ Bool ∣ _                                                           = L.Bool
  [ Bool ] _ ⇒ b₀ ~ b₁                                                 = b₀ L.≡ᴮ b₁
  [ Bool ]⟳ {c = L.false}                                              = L.ttₚ
  [ Bool ]⟳ {c = L.true}                                               = L.ttₚ
  [_]↔_⇒_    Bool {c₀ = L.false} {c₁ = L.false} C _                    = L.ttₚ
  [_]↔_⇒_    Bool {c₀ = L.true } {c₁ = L.true } _ _                    = L.ttₚ
  [_]_»_⇒_»_ Bool {c₀ = L.false} {c₁ = L.false} {c₂ = L.false} _ _ _ _ = L.ttₚ
  [_]_»_⇒_»_ Bool {c₀ = L.true } {c₁ = L.true } {c₂ = L.true} _ _ _ _  = L.ttₚ
  [ Bool ]↷ _ ⇒ b₀                                                     = b₀
  [ Bool ]≈ _ ⇒ L.false                                                = L.ttₚ
  [ Bool ]≈ _ ⇒ L.true                                                 = L.ttₚ

  Bool[] : Bool [ σ ]T ≡ω Bool
  Bool[] = L.reflω

  true : Tm Γ Bool
  ∣ true ∣ _  = L.true
  [ true ]≈ _ = L.ttₚ

  true[] : true [ σ ]t ≡ω true
  true[] = L.reflω

  false : Tm Γ Bool
  ∣ false ∣ _  = L.false
  [ false ]≈ _ = L.ttₚ

  false[] : false [ σ ]t ≡ω false
  false[] = L.reflω

  -- Non dependent eliminator
  ite' : {Γ : Con}{A : Ty Γ i} → (c : Tm Γ Bool) → (Tm Γ A) → (Tm Γ A) → (Tm Γ A)
  ∣_∣ (ite' c t f) γ with ∣ c ∣ γ
  ... | L.false = ∣ f ∣ γ
  ... | L.true  = ∣ t ∣ γ
  [_]≈_ (ite' c t f) {γ₀} {γ₁} γ₀~γ₁ with ∣ c ∣ γ₀ | ∣ c ∣ γ₁ | [ c ]≈ γ₀~γ₁
  ... | L.false | L.false | _ = [ f ]≈ γ₀~γ₁
  ... | L.true  | L.true  | _ = [ t ]≈ γ₀~γ₁

  -- Dependent eliminator
  ite : {C : Ty (Γ ▹ Bool) i} →
    (b : Tm Γ Bool) →
    (Tm Γ (C [ id ,⟨ Bool ⟩ true ]T)) →
    (Tm Γ (C [ id ,⟨ Bool ⟩ false ]T)) →
    (Tm Γ (C [ id ,⟨ Bool ⟩ b ]T))
  ∣ ite {C = C} b t f ∣ γ = L.indBool {A = λ b → ∣ C ∣ (γ L., b)} (∣ b ∣ γ) (∣ t ∣ γ) (∣ f ∣ γ)
  [_]≈_ (ite {C = C} b t f) {γ₀} {γ₁} γ₀~γ₁ =
    L.indBoolₚ
      {
          P = λ b₀ → (b₀= : b₀ L.≡ᴮ ∣ b ∣ γ₁) → [ C ] γ₀~γ₁ L.,ₚ b₀=
        ⇒
          L.indBool {A = λ b₀' → ∣ C ∣ (γ₀ L., b₀')} b₀ (∣ t ∣ γ₀) (∣ f ∣ γ₀)
        ~
          L.indBool {A = λ b₀' → ∣ C ∣ (γ₁ L., b₀')} (∣ b ∣ γ₁) (∣ t ∣ γ₁) (∣ f ∣ γ₁)
      }
      (∣ b ∣ γ₀)
      (λ b₀= → L.indBoolₚ
        {
          P = λ b₁ → (b₁= : L.true L.≡ᴮ b₁) → [ C ] γ₀~γ₁ L.,ₚ b₁=
        ⇒
          ∣ t ∣ γ₀
        ~
          L.indBool {A = λ b₁' → ∣ C ∣ (γ₁ L., b₁')} b₁ (∣ t ∣ γ₁) (∣ f ∣ γ₁)
        }
        (∣ b ∣ γ₁) (λ _ → [ t ]≈ γ₀~γ₁) (λ ()) b₀=)
      (λ b₀= → L.indBoolₚ
        {
          P = λ b₁ → (b₁= : L.false L.≡ᴮ b₁) → [ C ] γ₀~γ₁ L.,ₚ b₁=
        ⇒
          ∣ f ∣ γ₀
        ~
          L.indBool {A = λ b₁' → ∣ C ∣ (γ₁ L., b₁')} b₁ (∣ t ∣ γ₁) (∣ f ∣ γ₁)
        }
        (∣ b ∣ γ₁) (λ ()) (λ _ → [ f ]≈ γ₀~γ₁) b₀=)
      ([ b ]≈ γ₀~γ₁)

  ite[] :
    {σ : Sub Γ Δ} →
    {C : Ty (Δ ▹ Bool) i} →
    {b : Tm Δ Bool} →
    {t : Tm Δ (C [ id ,⟨ Bool ⟩ true ]T)} →
    {f : Tm Δ (C [ id ,⟨ Bool ⟩ false ]T)} →
    ite {C = C} b t f [ σ ]t ≡ω ite {C = C [ σ ^⟨ Bool ⟩ ]T} (b [ σ ]t) (t [ σ ]t) (f [ σ ]t)
  ite[] = L.reflω

  iteβ₁ : ite true t u ≡ω t
  iteβ₁ = L.reflω

  iteβ₂ : ite false t u ≡ω u
  iteβ₂ = L.reflω

  Ite~ : {γ₀ γ₁ : ∣ Γ ∣} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) →
          (b₀ b₁ : L.Bool) → (eq : b₀ L.≡ᴮ b₁) →
          (T F : Ty Γ i) →
          (c₀ : if b₀ then ∣ T ∣ γ₀ else ∣ F ∣ γ₀) →
          (c₁ : if b₁ then ∣ T ∣ γ₁ else ∣ F ∣ γ₁) →
          Prop i
  Ite~ {i = i} {γ₀ = γ₀} {γ₁} γ₀~γ₁ b₀ b₁ b₀≡ᴮb₁ T F c₀ c₁ =
    L.indBool
      {A = λ b₀' → (b= : b₀' L.≡ᴮ b₁) → (c₀' : if b₀' then ∣ T ∣ γ₀ else ∣ F ∣ γ₀) → Prop i}
      b₀
      (λ b= c₀' → L.indBool
                    {A = λ b₁' → (b=' : L.true L.≡ᴮ b₁') → (c₁' : if b₁' then ∣ T ∣ γ₁ else ∣ F ∣ γ₁) → Prop i}
                    b₁
                    (λ b=' c₁' → [ T ] γ₀~γ₁ ⇒ c₀' ~ c₁')
                    (λ ())
                    b=
                    c₁
      )
      (λ b= c₀' → L.indBool
                    {A = λ b₁' → (b=' : L.false L.≡ᴮ b₁') → (c₁' : if b₁' then ∣ T ∣ γ₁ else ∣ F ∣ γ₁) → Prop i}
                    b₁
                    (λ ())
                    (λ b=' c₁' → [ F ] γ₀~γ₁ ⇒ c₀' ~ c₁')
                    b=
                    c₁
      )
      b₀≡ᴮb₁
      c₀
  -- Ite~ γ₀~γ₁ L.false L.false _ T F c₀ c₁ = [ F ] γ₀~γ₁ ⇒ c₀ ~ c₁
  -- Ite~ γ₀~γ₁ L.true  L.true  _ T F c₀ c₁ = [ T ] γ₀~γ₁ ⇒ c₀ ~ c₁

  Ite⟳ : {γ : ∣ Γ ∣} →
          (b : L.Bool) →
          (T F : Ty Γ i) →
          {c : if b then ∣ T ∣ γ else ∣ F ∣ γ} →
          Ite~ [ Γ ]⟳ b b (L.reflᴮ {b}) T F c c
  Ite⟳ L.false T F = [ F ]⟳
  Ite⟳ L.true  T F = [ T ]⟳

  Ite↔ : {γ₀ γ₁ : ∣ Γ ∣}  → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) →
          (b₀ b₁ : L.Bool) → (b₀≡ᴮb₁ : b₀ L.≡ᴮ b₁)   →
          (T F : Ty Γ i) →
          {c₀ : if b₀ then ∣ T ∣ γ₀ else ∣ F ∣ γ₀} →
          {c₁ : if b₁ then ∣ T ∣ γ₁ else ∣ F ∣ γ₁} →
          Ite~ γ₀~γ₁ b₀ b₁ b₀≡ᴮb₁ T F c₀ c₁ →
          Ite~ ([ Γ ]↔ γ₀~γ₁) b₁ b₀ (L.symᴮ {b₀} b₀≡ᴮb₁) T F c₁ c₀
  Ite↔ γ₀~γ₁ L.false L.false _ T F c₀~c₁ = [ F ]↔ γ₀~γ₁ ⇒ c₀~c₁
  Ite↔ γ₀~γ₁ L.true  L.true  _ T F c₀~c₁ = [ T ]↔ γ₀~γ₁ ⇒ c₀~c₁

  Ite» : {γ₀ γ₁ γ₂ : ∣ Γ ∣}  → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (γ₁~γ₂ : [ Γ ] γ₁ ~ γ₂) →
          (b₀ b₁ b₂ : L.Bool) → (b₀≡ᴮb₁ : b₀ L.≡ᴮ b₁)   → (b₁≡ᴮb₂ : b₁ L.≡ᴮ b₂)   →
          (T F : Ty Γ i) →
          {c₀ : if b₀ then ∣ T ∣ γ₀ else ∣ F ∣ γ₀} →
          {c₁ : if b₁ then ∣ T ∣ γ₁ else ∣ F ∣ γ₁} →
          {c₂ : if b₂ then ∣ T ∣ γ₂ else ∣ F ∣ γ₂} →
          Ite~ γ₀~γ₁ b₀ b₁ b₀≡ᴮb₁ T F c₀ c₁ →
          Ite~ γ₁~γ₂ b₁ b₂ b₁≡ᴮb₂ T F c₁ c₂ →
          Ite~ ([ Γ ] γ₀~γ₁ » γ₁~γ₂) b₀ b₂ (L.transᴮ {b₀} b₀≡ᴮb₁ b₁≡ᴮb₂) T F c₀ c₂
  Ite» γ₀~γ₁ γ₁~γ₂ L.false L.false L.false _ _ T F c₀~c₁ c₀~c₂ = [ F ] γ₀~γ₁ » γ₁~γ₂ ⇒ c₀~c₁ » c₀~c₂
  Ite» γ₀~γ₁ γ₁~γ₂ L.true  L.true  L.true  _ _ T F c₀~c₁ c₀~c₂ = [ T ] γ₀~γ₁ » γ₁~γ₂ ⇒ c₀~c₁ » c₀~c₂

  Ite↷ : {γ₀ γ₁ : ∣ Γ ∣}  → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) →
          (b₀ b₁ : L.Bool) → (b₀≡ᴮb₁ : b₀ L.≡ᴮ b₁)   →
          (T F : Ty Γ i) →
          if b₀ then ∣ T ∣ γ₀ else ∣ F ∣ γ₀ →
          if b₁ then ∣ T ∣ γ₁ else ∣ F ∣ γ₁
  Ite↷ {γ₀ = γ₀} {γ₁} γ₀~γ₁ b₀ b₁ b₀≡ᴮb₁ T F c₀ =
    L.indBool
      {A = λ b₀' → (b= : b₀' L.≡ᴮ b₁) → if b₀' then ∣ T ∣ γ₀ else ∣ F ∣ γ₀ → if b₁ then ∣ T ∣ γ₁ else ∣ F ∣ γ₁}
      b₀
      (λ b= t → L.indBool {A = λ b₁' → (b=' : L.true  L.≡ᴮ b₁') → if b₁' then ∣ T ∣ γ₁ else ∣ F ∣ γ₁} b₁ (λ b=' → [ T ]↷ γ₀~γ₁ ⇒ t) (λ ()) b=)
      (λ b= f → L.indBool {A = λ b₁' → (b=' : L.false L.≡ᴮ b₁') → if b₁' then ∣ T ∣ γ₁ else ∣ F ∣ γ₁} b₁ (λ ()) (λ b=' → [ F ]↷ γ₀~γ₁ ⇒ f) b=)
      b₀≡ᴮb₁
      c₀
  -- Ite↷ γ₀~γ₁ L.false L.false _ T F c₀ = [ F ]↷ γ₀~γ₁ ⇒ c₀
  -- Ite↷ γ₀~γ₁ L.true  L.true  _ T F c₀ = [ T ]↷ γ₀~γ₁ ⇒ c₀

  Ite≈ : {γ₀ γ₁ : ∣ Γ ∣}  → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) →
          (b₀ b₁ : L.Bool) → (b₀≡ᴮb₁ : b₀ L.≡ᴮ b₁)   →
          (T F : Ty Γ i) →
          (c₀ : if b₀ then ∣ T ∣ γ₀ else ∣ F ∣ γ₀) →
          Ite~   γ₀~γ₁ b₀ b₁ b₀≡ᴮb₁ T F c₀ (
            Ite↷ γ₀~γ₁ b₀ b₁ b₀≡ᴮb₁ T F c₀
          )
  Ite≈ γ₀~γ₁ L.false L.false b₀≡ᴮb₁ T F c₀ = [ F ]≈ γ₀~γ₁ ⇒ c₀
  Ite≈ γ₀~γ₁ L.true  L.true  b₀≡ᴮb₁ T F c₀ = [ T ]≈ γ₀~γ₁ ⇒ c₀

  Ite : (b : Tm Γ Bool) → Ty Γ i → Ty Γ i → Ty Γ i
  ∣ Ite b T F ∣ γ                                               = if ∣ b ∣ γ then ∣ T ∣ γ else ∣ F ∣ γ
  [_]_⇒_~_   (Ite b T F) {γ₀} {γ₁}      γ₀~γ₁ c₀    c₁          = Ite~ γ₀~γ₁       (∣ b ∣ γ₀) (∣ b ∣ γ₁)            ([ b ]≈ γ₀~γ₁) T F c₀ c₁
  [_]↷_⇒_    (Ite b T F) {γ₀} {γ₁}      γ₀~γ₁ c₀                = Ite↷ γ₀~γ₁       (∣ b ∣ γ₀) (∣ b ∣ γ₁)            ([ b ]≈ γ₀~γ₁) T F c₀
  [_]≈_⇒_    (Ite b T F) {γ₀} {γ₁}      γ₀~γ₁ c₀                = Ite≈ γ₀~γ₁       (∣ b ∣ γ₀) (∣ b ∣ γ₁)            ([ b ]≈ γ₀~γ₁) T F c₀

  [_]⟳       (Ite b T F) {γ}                                    = Ite⟳             (∣ b ∣ γ)                                                      T F
  [_]↔_⇒_    (Ite b T F) {γ₀} {γ₁}      γ₀~γ₁       c₀~c₁       = Ite↔ γ₀~γ₁       (∣ b ∣ γ₀) (∣ b ∣ γ₁)            ([ b ]≈ γ₀~γ₁)                T F c₀~c₁
  [_]_»_⇒_»_ (Ite b T F) {γ₀} {γ₁} {γ₂} γ₀~γ₁ γ₁~γ₂ c₀~c₁ c₁~c₂ = Ite» γ₀~γ₁ γ₁~γ₂ (∣ b ∣ γ₀) (∣ b ∣ γ₁) (∣ b ∣ γ₂) ([ b ]≈ γ₀~γ₁) ([ b ]≈ γ₁~γ₂) T F c₀~c₁ c₁~c₂

  Ite[] :
    {σ : Sub Γ Δ} →
    {b : Tm Δ Bool} → {T F : Ty Δ i} →
    Ite b T F [ σ ]T ≡ω Ite (b [ σ ]t) (T [ σ ]T) (F [ σ ]T)
  Ite[] {σ = σ} {b} {T} {F} = L.reflω
    -- let
    --   left = Ite b T F [ σ ]T
    --   right = Ite (b [ σ ]t) (T [ σ ]T) (F [ σ ]T)
    -- in
    --   mkTy≡
    --     ∣ left ∣        ∣ right ∣ L.refl
    --     [ left ]_⇒_~_   [ right ]_⇒_~_ L.refl
    --     [ left ]⟳       [ right ]⟳
    --     [ left ]↔_⇒_    [ right ]↔_⇒_
    --     [ left ]_»_⇒_»_ [ right ]_»_⇒_»_
    --     [ left ]↷_⇒_    [ right ]↷_⇒_ L.refl
    --     [ left ]≈_⇒_    [ right ]≈_⇒_

  Boolβ₁ : ite true t u ≡ω t
  Boolβ₁ = L.reflω

  Boolβ₂ : ite false t u ≡ω u
  Boolβ₂ = L.reflω

  Iteβ₁ : Ite true A B ≡ω A
  Iteβ₁ = L.reflω

  Iteβ₂ : Ite false A B ≡ω B
  Iteβ₂ = L.reflω


module Pi where
  open Category
  open Families
  open ContextExtension

  Π : (A : Ty Γ i) → (B : Ty (Γ ▹ A) j) → Ty Γ (i ⊔ j)

  ∣ Π {Γ = Γ} A B ∣ γ = L.Σₛₚ
    ((a : ∣ A ∣ γ) → ∣ B ∣ (γ L., a))
    (λ f → {a₀ a₁ : ∣ A ∣ γ}(a₀~a₁ : [ A ] [ Γ ]⟳ ⇒ a₀ ~ a₁) →
           [ B ] ([ Γ ]⟳ L.,ₚ a₀~a₁) ⇒ f a₀ ~ f a₁)

  -- TODO this needs to be truncated?
  [_]_⇒_~_ (Π A B) {γ₀} {γ₁} γ₀~γ₁ (f₀ L.,ₛₚ f₀~) (f₁ L.,ₛₚ f₁~) =
    {a₀ : ∣ A ∣ γ₀}{a₁ : ∣ A ∣ γ₁}(a₀~a₁ : [ A ] γ₀~γ₁ ⇒ a₀ ~ a₁) →
    [ B ] γ₀~γ₁ L.,ₚ a₀~a₁ ⇒ f₀ a₀ ~ f₁ a₁

  -- [ Π A B ]⟳ {c = c} a₀~a₁ = (π₂ₛₚ c) a₀~a₁
  [ Π A B ]⟳ {c = f L.,ₛₚ f~} = λ a₀~a₁ → f~ a₀~a₁

  ([ Π {Γ = Γ} A B ]↔ γ₀~γ₁ ⇒ f) a₀~a₁ = [ B ]↔ (γ₀~γ₁ L.,ₚ a₁~a₀) ⇒ f a₁~a₀
    where
      a₁~a₀ = [ A ]↔ ([ Γ ]↔ γ₀~γ₁) ⇒ a₀~a₁

  ([ Π {Γ = Γ} A B ] γ₀~γ₁ » γ₁~γ₂ ⇒ f₀~f₁ » f₁~f₂) {a₀ = a₀} a₀~a₂ =
    [ B ] (γ₀~γ₁ L.,ₚ a₀~a₁) » (γ₁~γ₂ L.,ₚ a₁~a₂) ⇒ f₀~f₁ a₀~a₁ » f₁~f₂ a₁~a₂
    where
      γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁
      γ₀~γ₂ = [ Γ ] γ₀~γ₁ » γ₁~γ₂
      a₁    = [ A ]↷ γ₀~γ₁ ⇒ a₀
      a₀~a₁ = [ A ]≈ γ₀~γ₁ ⇒ a₀
      a₁~a₂ = [ A ] γ₁~γ₀ » γ₀~γ₂ ⇒ [ A ]↔ γ₀~γ₁ ⇒ a₀~a₁ » a₀~a₂

  [ Π {Γ = Γ} A B ]↷ γ₀~γ₁ ⇒ (f L.,ₛₚ f~) =
    (λ a → [ B ]↷ γ₀~γ₁ L.,ₚ ([ A ]↔ γ₁~γ₀ ⇒ [ A ]≈ γ₁~γ₀ ⇒ a) ⇒ f ([ A ]↷ γ₁~γ₀ ⇒ a))
      L.,ₛₚ
    (λ {a₀} {a₁} a₀~a₁ → let
        ↷a₀ = [ A ]↷ γ₁~γ₀ ⇒ a₀
        ↷a₁ = [ A ]↷ γ₁~γ₀ ⇒ a₁

        a₀~↷a₀ = [ A ]≈ γ₁~γ₀ ⇒ a₀
        a₁~↷a₁ = [ A ]≈ γ₁~γ₀ ⇒ a₁

        ↷a₀~a₀ = [ A ]↔ γ₁~γ₀ ⇒ a₀~↷a₀
        ↷a₁~a₁ = [ A ]↔ γ₁~γ₀ ⇒ a₁~↷a₁

        ↷a₀~a₁ = [ A ] γ₀~γ₁ » [ Γ ]⟳ ⇒ ↷a₀~a₀ » a₀~a₁
        a₀~↷a₁ = [ A ] [ Γ ]⟳ » γ₁~γ₀ ⇒ a₀~a₁ » a₁~↷a₁
        ↷a₀~↷a₁ = [ A ] γ₀~γ₁ » γ₁~γ₀ ⇒ ↷a₀~a₀ » a₀~↷a₁

        ↷f↷a₀~f↷a₀ = [ B ]↔ γ₀~γ₁ L.,ₚ ↷a₀~a₀ ⇒ [ B ]≈ γ₀~γ₁ L.,ₚ ↷a₀~a₀ ⇒ f ↷a₀
        f↷a₀~f↷a₁ = f~ ↷a₀~↷a₁
        f↷a₁~↷f↷a₁ = [ B ]≈ γ₀~γ₁ L.,ₚ ↷a₁~a₁ ⇒ f ↷a₁
        ↷f↷a₀~↷f↷a₁ = [ B ] γ₁~γ₀ L.,ₚ a₀~↷a₀ » γ₀~γ₁ L.,ₚ ↷a₀~a₁ ⇒
                      ↷f↷a₀~f↷a₀             » [ B ] [ Γ ]⟳ L.,ₚ ↷a₀~↷a₁ » γ₀~γ₁ L.,ₚ ↷a₁~a₁ ⇒
                                               f↷a₀~f↷a₁                 » f↷a₁~↷f↷a₁
      in
        ↷f↷a₀~↷f↷a₁
    )
    where
      γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁

  ([ Π {Γ = Γ} A B ]≈ γ₀~γ₁ ⇒ (f L.,ₛₚ f~)) {a₀} {a₁} a₀~a₁ =
    [ B ] [ Γ ]⟳ L.,ₚ a₀~↷a₁ » γ₀~γ₁ L.,ₚ ↷a₁~a₁                                  ⇒
    f~ a₀~↷a₁                » [ B ]≈ (γ₀~γ₁ L.,ₚ ↷a₁~a₁) ⇒ f ([ A ]↷ γ₁~γ₀ ⇒ a₁)
    where
      γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁
      a₁~↷a₁ = [ A ]≈ γ₁~γ₀ ⇒ a₁
      ↷a₁~a₁ = [ A ]↔ γ₁~γ₀ ⇒ a₁~↷a₁
      a₀~↷a₁ = [ A ] γ₀~γ₁ » γ₁~γ₀ ⇒ a₀~a₁ » a₁~↷a₁

  -- Π[] : {Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ}{A : Ty Δ k}{B : Ty (Δ ▹ A) l} → (Π A B) [ σ ]T ≡ Π (A [ σ ]T) (B [ σ ^⟨ A ⟩ ]T)
  Π[] : (Π A B) [ σ ]T ≡ω Π (A [ σ ]T) (B [ σ ^⟨ A ⟩ ]T)
  Π[] = L.reflω

  lam : Tm (Γ ▹ A) B → Tm Γ (Π A B)
  ∣ lam {Γ = Γ} t ∣ γ = (λ a → ∣ t ∣ (γ L., a)) L.,ₛₚ (λ a₀~a₁ → [ t ]≈ ([ Γ ]⟳ L.,ₚ a₀~a₁))
  [ lam {Γ = Γ} t ]≈ γ₀~γ₁ = λ a₀~a₁ → [ t ]≈ (γ₀~γ₁ L.,ₚ a₀~a₁)

  -- lam[] : {Γ : Con i}{Δ : Con j}{σ : Sub Γ Δ}{A : Ty Δ k}{B : Ty (Δ ▹ A) l}{t : Tm (Δ ▹ A) B} → lam {A = A} t [ σ ]t ≡ lam {A = A [ σ ]T} {B = B [ σ ^⟨ A ⟩ ]T} (t [ σ ^⟨ A ⟩ ]t)
  lam[] : {t : Tm (Δ ▹ A) B} → lam {A = A} t [ σ ]t ≡ω lam {A = A [ σ ]T} {B = B [ σ ^⟨ A ⟩ ]T} (t [ σ ^⟨ A ⟩ ]t)
  lam[] = L.reflω

  app : Tm Γ (Π A B) → Tm (Γ ▹ A) B
  ∣ app t ∣ (γ L., a) = L.π₁ₛₚ (∣ t ∣ γ) a
  [_]≈_ (app t) {γ₀ L., a₀} {γ₁ L., a₁} (γ₀~γ₁ L.,ₚ a₀~a₁) = ([ t ]≈ γ₀~γ₁) a₀~a₁

  app[] : {t : Tm Δ (Π A B)} → app {A = A} {B = B} t [ σ ^⟨ A ⟩ ]t ≡ω app {A = A [ σ ]T} {B = B [ σ ^⟨ A ⟩ ]T} (t [ σ ]t)
  app[] = L.reflω

  Πβ : app {A = A} (lam {A = A} t) ≡ω t
  Πβ = L.reflω

  Πη : lam {A = A} {B = B} (app {A = A} t) ≡ω t
  Πη = L.reflω

  infix 5 _$_
  _$_ : Tm Γ (Π {i = i} A B) → (a : Tm Γ A) → Tm Γ (B [ id ,⟨ A ⟩ a ]T)
  _$_ {A = A} {B = B} f a = app {A = A} f ⟨ B ⟩[ id ,⟨ A ⟩ a ]t

  app$ : Tm Γ (Π {i = i} A B) → (a : Tm Γ A) → Tm Γ (B [ id ,⟨ A ⟩ a ]T)
  app$ {A = A} {B = B} f a = _$_ {A = A} {B = B} f a

  infix 5 app$
  syntax app$ {A = A} {B = B} f a = f $⟨ A ⟩⟨ B ⟩ a

  -- $[] : {A : Ty Γ i}{B : Ty (Γ ▹ A) j}{t : Tm Γ (Π A B)}{u : Tm Γ A}{σ : Sub Δ Γ} →
  --       (t $⟨ A ⟩⟨ B ⟩ u) [ σ ]t ≡ω (t ⟨ Π A B ⟩[ σ ]t $⟨ A [ σ ]T ⟩⟨ B [ σ ^⟨ A ⟩ ]T ⟩ (u [ σ ]t))
  $[] : (t $⟨ A ⟩⟨ B ⟩ u) [ σ ]t ≡ω t [ σ ]t $⟨ A [ σ ]T ⟩⟨ B [ σ ^⟨ A ⟩ ]T ⟩ (u [ σ ]t)
  $[] = L.reflω

  -- app' : Tm Γ (Π A B) → Tm (Γ ▹ A) B
  -- app' = {! ? $ ? !}
