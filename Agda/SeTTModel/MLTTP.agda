{-# OPTIONS --prop #-}
-- {-# OPTIONS --cumulativity #-}
-- {-# OPTIONS --no-pattern-match #-}
module MLTTP where

open import Agda.Primitive
open import Lib

private
  variable
    i j k : Level

module CwFₘ where
  -- Category, Terminal Object, Families and Context Extension are built in

  module SizeLevelLiftingₘ where
    record Liftₘ (A : Set i) : Set (i ⊔ j) where
      constructor liftₘ
      field lowerₘ : A
    open Liftₘ public

module Propositionsₘ where

  -- Familiesₚ and Context Extensionₚ are built in

  module Conversionsₘ where
    record Embedₘ (A : Prop i) : Set i where
      constructor embedₘ
      field extractₘ : A
    open Embedₘ public

    irr : {Aₚ : Prop i} → (u v : Embedₘ Aₚ) → u ≡ v
    irr u v = refl

    data Truncₘ (A : Set i) : Prop i where
      truncₘ : A → Truncₘ A

    truncElimₘ : {A : Set i}{B : Truncₘ A → Prop j} →
                (a→b : (a : A) → B (truncₘ a)) →
                ((tra : Truncₘ A) → B tra)
    truncElimₘ a→b (truncₘ a) = a→b a

    module _ where -- Abbreviations

      truncRecₘ : {A : Set i} → {B : Prop j} → (A → B) → (Truncₘ A → B)
      truncRecₘ f a = truncElimₘ f a

  module SizeLevelLiftingₚₘ where
    open Conversionsₘ
    open CwFₘ.SizeLevelLiftingₘ

    module _ where -- Abbreviations

      Liftₚₘ : Prop i → Prop (i ⊔ j)
      Liftₚₘ {j = j} Aₚ = Truncₘ (Liftₘ {j = j} (Embedₘ Aₚ))

      liftₚₘ : {Aₚ : Prop i} → Aₚ → Liftₚₘ {j = j} Aₚ
      liftₚₘ aₚ = truncₘ (liftₘ (embedₘ aₚ))

      lowerₚₘ : {Aₚ : Prop i} → Liftₚₘ {j = j} Aₚ → Aₚ
      lowerₚₘ aₚ = truncRecₘ (λ aₚ' → extractₘ (lowerₘ aₚ')) aₚ

module Typeformersₘ where

  module Unitₘ {- Topₘ -} where
    record ⊤ₘ : Set where
      constructor ttₘ

  module Emptyₘ {- Bottomₘ -} where
    data ⊥ₘ : Set where

    exfalsoₘ : {A : Set i} → ⊥ₘ → A
    exfalsoₘ ()

    exfalsoₚₘ : {Aₚ : Prop i} → ⊥ₘ → Aₚ
    exfalsoₚₘ ()

  module Sigmaₘ where
    record Σₘ (A : Set i) (B : A → Set j) : Set (i ⊔ j) where
      constructor _,Σₘ_
      field
        π₁ₘ : A
        π₂ₘ : B π₁ₘ
    open Σₘ public

  -- Piₘ is built in

  module Booleanₘ where
    data Boolₘ : Set where
      falseₘ trueₘ : Boolₘ

    iteₘ : {C : Boolₘ → Set i} → (C trueₘ) → (C falseₘ) → (b : Boolₘ) → (C b)
    iteₘ t f trueₘ  = t
    iteₘ t f falseₘ = f

    Iteₘ : {D : Boolₘ → Set i} → (T : D trueₘ → Set j) → (F : D falseₘ → Set k) →
           (b : Boolₘ) → (d : D b) → Set (iteₘ j k b)
    Iteₘ T F trueₘ d = T d
    Iteₘ T F falseₘ d = F d

    Iteₚₘ : {D : Boolₘ → Set i} → (T : D trueₘ → Prop j) → (F : D falseₘ → Prop k) →
            (b : Boolₘ) → (d : D b) → Prop (iteₘ j k b)
    Iteₚₘ T F trueₘ d = T d
    Iteₚₘ T F falseₘ d = F d

module Typeformersₚₘ where
  open Propositionsₘ.Conversionsₘ

  module _ where -- Abbreviations

    module Unitₚₘ {- Topₚₘ -} where
      open Typeformersₘ.Unitₘ

      ⊤ₚₘ : Prop
      ⊤ₚₘ = Truncₘ ⊤ₘ

      ttₚₘ : ⊤ₚₘ
      ttₚₘ = truncₘ ttₘ

    module Emptyₚₘ {- Bottomₚₘ -} where
      open Typeformersₘ.Emptyₘ

      ⊥ₚₘ : Prop
      ⊥ₚₘ = Truncₘ ⊥ₘ

    module Sigmaₚₘ where
      open Typeformersₘ.Sigmaₘ

      Σₚₘ : (Aₚ : Prop i) (Bₚ : Aₚ → Prop j) → Prop (i ⊔ j)
      -- Σₚₘ Aₚ Bₚ = Truncₘ (Σₘ (Embedₘ Aₚ) (λ { (embedₘ aₚ) → Embedₘ (Bₚ aₚ) }))
      Σₚₘ Aₚ Bₚ = Truncₘ (Σₘ (Embedₘ Aₚ) (λ aₚ → Embedₘ (Bₚ (extractₘ aₚ))))

      _,Σₚₘ_ : {Aₚ : Prop i} → {Bₚ : Aₚ → Prop j} → (aₚ : Aₚ) → Bₚ aₚ → Σₚₘ Aₚ Bₚ
      aₚ ,Σₚₘ bₚ = truncₘ (embedₘ aₚ ,Σₘ embedₘ bₚ)

      π₁ₚₘ : {Aₚ : Prop i} → {Bₚ : Aₚ → Prop j} → Σₚₘ Aₚ Bₚ → Aₚ
      -- π₁ₚₘ (truncₘ ab) = extractₘ (π₁ₘ ab)
      -- π₁ₚₘ abₚ = truncRecₘ (λ { (embedₘ a ,Σₘ b) → a }) abₚ
      π₁ₚₘ abₚ = truncRecₘ (λ ab → extractₘ (π₁ₘ ab)) abₚ

      π₂ₚₘ : {Aₚ : Prop i} → {Bₚ : Aₚ → Prop j} → (abₚ : Σₚₘ Aₚ Bₚ) → Bₚ (π₁ₚₘ {Bₚ = Bₚ} abₚ)
      π₂ₚₘ abₚ = truncRecₘ (λ ab → extractₘ (π₂ₘ ab)) abₚ

    -- Piₚₘ is built in
