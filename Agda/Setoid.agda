-- {-# OPTIONS --prop --type-in-type #-}
{-# OPTIONS --prop #-}

open import Agda.Primitive
open import Relation.Binary.PropositionalEquality renaming (subst to Lsubst ; refl to Lrefl)
open import Data.Unit renaming (⊤ to L⊤ ; tt to Ltt)
open import Data.Bool renaming (Bool to LBool ; true to Ltrue ; false to Lfalse)
open import Data.Product renaming (Σ to LΣ ; _,_ to _L,_ ; proj₁ to Lproj₁ ; proj₂ to Lproj₂ )

module Setoid where

module Lib where

  variable
    i j k l m n : Level

  record ↑_ (P : Prop i) : Set i where
    constructor ↑[_]↑
    field
      ↓[_]↓ : P
  open ↑_ public

  infix 4 _≡ₚ_
  data _≡ₚ_ {a} {A : Set a} (x : A) : A → Prop a where
    instance Lreflₚ : x ≡ₚ x

  -- record ⊤ : Set where
  --   constructor tt

  record ⊤ₚ : Prop i where
    constructor ttₚ

  -- const : {A B : Set i} → A → B → A
  -- const a b = a

  -- infixr 4 _,ₚ_

  record Σₛₚ (A : Set i) (B : A → Prop j) : Set (i ⊔ j) where
    constructor _,ₛₚ_
    field
      proj₁ₛₚ : A
      proj₂ₛₚ : B proj₁ₛₚ
  open Σₛₚ public

  record Σₚ (A : Prop i) (B : A → Prop j) : Prop (i ⊔ j) where
    constructor _,ₚ_
    field
      proj₁ₚ : A
      proj₂ₚ : B proj₁ₚ
  open Σₚ public

open Lib renaming
  ( Σₚ to LΣₚ
  ; _,ₚ_ to _L,ₚ_

  ; Σₛₚ to LΣₛₚ
  ; _,ₛₚ_ to _L,ₛₚ_

  ; ⊤ₚ to L⊤ₚ
  ; ttₚ to Lttₚ
  )

{- Category -}

record Con (i : Level) : Set (lsuc i) where
  field
    carrier : Set i
    equality : carrier → carrier → Prop i
    reflexivity : {c : carrier} → equality c c
    symmetry : {c₀ c₁ : carrier} → equality c₀ c₁ → equality c₁ c₀
    transitivity : {c₀ c₁ c₂ : carrier} → equality c₀ c₁ → equality c₁ c₂ → equality c₀ c₂
  -- syntax equality Γ γ₀ γ₁ = γ₀ ~[ Γ ]~  γ₁
open Con renaming
  ( carrier to ¦_¦
  ; equality to infix 5 [_]_~_
  ; reflexivity to [_]⟳
  ; symmetry to [_]↔_
  ; transitivity to [_]_»_
  )

variable
  Γ Δ Θ Ω : Con i

record Sub (Γ : Con i) (Δ : Con j) : Set (i ⊔ j) where
  field
    carrier : ¦ Γ ¦ → ¦ Δ ¦
    preservation : {γ₀ γ₁ : ¦ Γ ¦} → [ Γ ] γ₀ ~ γ₁ → [ Δ ] (carrier γ₀) ~ (carrier γ₁)
open Sub renaming
  ( carrier to ¦_¦
  ; preservation to infix 5 [_]≈_
  )

variable
  σ δ ν : Sub Γ Δ

_∘_ : Sub Δ Θ → Sub Γ Δ → Sub Γ Θ
¦ σ ∘ δ ¦ γ      = ¦ σ ¦ (¦ δ ¦ γ)
[ σ ∘ δ ]≈ γ₀~γ₁ = [ σ ]≈ ([ δ ]≈ γ₀~γ₁)

id : Sub Γ Γ
¦ id ¦ γ      = γ
[ id ]≈ γ₀~γ₁ = γ₀~γ₁

ass : (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = Lrefl

{- Terminal Object -}

∙ : Con lzero
¦ ∙ ¦       = L⊤
[ ∙ ] _ ~ _ = L⊤ₚ
[ ∙ ]⟳      = Lttₚ
[ ∙ ]↔ _    = Lttₚ
[ ∙ ] _ » _ = Lttₚ

ε : Sub Γ ∙
¦ ε ¦ _  = Ltt
[ ε ]≈ _ = Lttₚ

∙η : {σ : Sub Γ ∙} → σ ≡ ε
∙η = Lrefl

{- Families -}

-- Ty variations:
-- + homogeneous / heterogeneous
-- + coe0, coe1 / symmetry, transport (?typo -> transitivity?)
-- + coeRefl?, coeTrans? (strict _fibrancy_) (= / ~)
-- + TyP + TmP / U(P) + El

record Ty (Γ : Con i) (j : Level) : Set (lsuc (i ⊔ j)) where
  field
    carrier : ¦ Γ ¦ → Set j
    equality : {γ₀ γ₁ : ¦ Γ ¦} → [ Γ ] γ₀ ~ γ₁ → carrier γ₀ → carrier γ₁ → Prop j
    reflexivity : {γ : ¦ Γ ¦}{c : carrier γ} → equality ([ Γ ]⟳) c c
    symmetry : {γ₀ γ₁ : ¦ Γ ¦}{c₀ : carrier γ₀}{c₁ : carrier γ₁} →
               (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → equality γ₀~γ₁ c₀ c₁ → equality ([ Γ ]↔ γ₀~γ₁) c₁ c₀
    transitivity : {γ₀ γ₁ γ₂ : ¦ Γ ¦}{c₀ : carrier γ₀}{c₁ : carrier γ₁}{c₂ : carrier γ₂} →
                   (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (γ₁~γ₂ : [ Γ ] γ₁ ~ γ₂) →
                   equality γ₀~γ₁ c₀ c₁ → equality γ₁~γ₂ c₁ c₂ → equality ([ Γ ] γ₀~γ₁ » γ₁~γ₂) c₀ c₂
    coercion : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → carrier γ₀ → carrier γ₁
    coherence : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → (c₀ : carrier γ₀) → equality γ₀~γ₁ c₀ (coercion γ₀~γ₁ c₀)
open Ty renaming
  ( carrier to ¦_¦
  ; equality to infix 5 [_]_⇒_~_
  ; reflexivity to [_]⟳
  ; symmetry to [_]↔_⇒_
  ; transitivity to [_]_»_⇒_»_
  ; coercion to [_]↷_⇒_
  ; coherence to [_]≈_⇒_
  )

variable
  A : Ty Γ i

record Tm (Γ : Con i) (A : Ty Γ j) : Set (lsuc (i ⊔ j)) where
  field
    carrier : (γ : ¦ Γ ¦) → ¦ A ¦ γ
    preservation : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → [ A ] γ₀~γ₁ ⇒ (carrier γ₀) ~ (carrier γ₁)
open Tm renaming
  ( carrier to ¦_¦
  ; preservation to infix 5 [_]≈_
  )

variable
  a : Tm Γ A

_[_]T : {Γ Δ : Con i} → Ty Δ j → Sub Γ Δ → Ty Γ j
¦ A [ σ ]T ¦ γ                             = ¦ A ¦ (¦ σ ¦ γ)
[ A [ σ ]T ] γ₀~γ₁ ⇒ a₀ ~ a₁               = [ A ] ([ σ ]≈ γ₀~γ₁) ⇒ a₀ ~ a₁
[ A [ σ ]T ]⟳                              = [ A ]⟳
[ A [ σ ]T ]↔ γ₀~γ₁ ⇒ a₀~a₁                = [ A ]↔ ([ σ ]≈ γ₀~γ₁) ⇒ a₀~a₁
[ A [ σ ]T ] γ₀~γ₁ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂ = [ A ] ([ σ ]≈ γ₀~γ₁) » ([ σ ]≈ γ₁~γ₂) ⇒ a₀~a₁ » a₁~a₂
[ A [ σ ]T ]↷ γ₀~γ₁ ⇒ a₀                   = [ A ]↷ ([ σ ]≈ γ₀~γ₁) ⇒ a₀
[ A [ σ ]T ]≈ γ₀~γ₁ ⇒ a₀~a₁                = [ A ]≈ ([ σ ]≈ γ₀~γ₁) ⇒ a₀~a₁

_[_]t : Tm Δ A → (σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
¦ t [ σ ]t ¦ γ      = ¦ t ¦ ((¦ σ ¦ γ))
[ t [ σ ]t ]≈ γ₀~γ₁ = [ t ]≈ ([ σ ]≈ γ₀~γ₁)


[∘]T : A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]T = Lrefl

idT : A [ id ]T ≡ A
idT = Lrefl

[∘]t : a [ σ ∘ δ ]t ≡ a [ σ ]t [ δ ]t
[∘]t = Lrefl

idt : {A : Ty Γ i}{a : Tm Γ A} → a [ id ]t ≡ a -- TODO why not remove {} here?
idt = Lrefl

{- FamiliesP -}

record TyP (Γ : Con i) (j : Level) : Set (lsuc (i ⊔ j)) where
  field
    carrier : ¦ Γ ¦ → Set j
    coercion : {γ₀ γ₁ : ¦ Γ ¦} → (γ₀~γ₁ : [ Γ ] γ₀ ~ γ₁) → carrier γ₀ → carrier γ₁
open TyP renaming
  ( carrier to ¦_¦
  ; coercion to [_]↷_⇒_
  )

variable
  P : TyP Γ i

record TmP (Γ : Con i) (A : TyP Γ j) : Set (lsuc (i ⊔ j)) where
  field
    carrier : (γ : ¦ Γ ¦) → ¦ A ¦ γ
open TmP renaming
  ( carrier to ¦_¦
  )


_[_]TP : {Γ Δ : Con i} → TyP Δ j → Sub Γ Δ → TyP Γ j
¦ P [ σ ]TP ¦ γ           = ¦ P ¦ (¦ σ ¦ γ)
[ P [ σ ]TP ]↷ γ₀~γ₁ ⇒ a₀ = [ P ]↷ ([ σ ]≈ γ₀~γ₁) ⇒ a₀

_[_]tP : TmP Δ P → (σ : Sub Γ Δ) → TmP Γ (P [ σ ]TP)
¦ t [ σ ]tP ¦ γ = ¦ t ¦ ((¦ σ ¦ γ))


[∘]TP : P [ σ ∘ δ ]TP ≡ P [ σ ]TP [ δ ]TP
[∘]TP = Lrefl

idTP : P [ id ]TP ≡ P
idTP = Lrefl


-- {- Comprehension -}

_▷_ : (Γ : Con i) → (A : Ty Γ j) → Con (i ⊔ j)
¦ Γ ▷ A ¦                                        = LΣ (¦ Γ ¦) ¦ A ¦
[ Γ ▷ A ]  (γ₀   L,  a₀  ) ~ (γ₁   L,  a₁  )     = LΣₚ ([ Γ ] γ₀ ~ γ₁) (λ γ₀~γ₁ → [ A ] γ₀~γ₁ ⇒ a₀ ~ a₁)
[ Γ ▷ A ]⟳                                       = [ Γ ]⟳ L,ₚ [ A ]⟳
[ Γ ▷ A ]↔ (γ₀~γ₁ L,ₚ a₀~a₁)                     = ([ Γ ]↔ γ₀~γ₁) L,ₚ ([ A ]↔ γ₀~γ₁ ⇒ a₀~a₁)
[ Γ ▷ A ]  (γ₀~γ₁ L,ₚ a₀~a₁) » (γ₁~γ₂ L,ₚ a₁~a₂) = ([ Γ ] γ₀~γ₁ » γ₁~γ₂) L,ₚ ([ A ] γ₀~γ₁ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂)

_,_ : (σ : Sub Γ Δ)(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
¦ σ , t ¦ γ      = (¦ σ ¦ γ)      L,  (¦ t ¦ γ)
[ σ , t ]≈ γ₀~γ₁ = ([ σ ]≈ γ₀~γ₁) L,ₚ ([ t ]≈ γ₀~γ₁)

p : Sub (Γ ▷ A) Γ
¦ p ¦ (γ L, a)           = γ
[ p ]≈ (γ₀~γ₁ L,ₚ a₀~a₁) = γ₀~γ₁

q : {Γ : Con i}{A : Ty Γ i} → Tm (Γ ▷ A) (A [ p {A = A} ]T)
¦ q ¦ (γ L, a)           = a
[ q ]≈ (γ₀~γ₁ L,ₚ a₀~a₁) = a₀~a₁

▷β₁ : {t : Tm Γ (A [ σ ]T)} → (p {A = A}) ∘ (_,_ {A = A} σ t) ≡ σ
▷β₁ = Lrefl

▷β₂ : {t : Tm Γ (A [ σ ]T)} → (q {A = A}) [ (_,_ {A = A} σ t) ]t ≡ t
▷β₂ = Lrefl

-- ▹η : {Γ : Con i}{Δ : Con j}{A : Ty Δ k}{σ : Sub (Γ ▷ (A [ σ ]T)) (Δ ▷ A)} →
--      ((p ∘ σ) , (q [ σ ]t)) ≡ σ
-- ▹η = ?

▹η : (_,_ {A = A} (p {A = A}) (q {A = A})) ≡ id
▹η = Lrefl


{- Unit -}

⊤ : TyP Γ lzero
¦ ⊤ ¦ _      = L⊤
[ ⊤ ]↷ _ ⇒ _ = Ltt

tt : TmP Γ ⊤
¦ tt ¦ _ = Ltt

{- Id -}

-- Spoilers:
-- + Ty problémák?
-- + ID

Id : {A : Ty Γ i} → (a₀ a₁ : Tm Γ A) → Ty Γ i
¦ Id {Γ = Γ} {A = A} a₀ a₁ ¦ γ    = ↑ ([ A ] [ Γ ]⟳ ⇒ (¦ a₀ ¦ γ) ~ (¦ a₁ ¦ γ))
[ Id _ _ ] _ ⇒ _ ~ _              = L⊤ₚ
[ Id _ _ ]⟳                       = Lttₚ
[ Id _ _ ]↔ _ ⇒ _                 = Lttₚ
[ Id _ _ ] _ » _ ⇒ _ » _          = Lttₚ
[ Id {Γ = Γ} {A = A} a₀ a₁ ]↷ γ₀~γ₁ ⇒ ↑[ a₀~₀a₁ ]↑ =
  ↑[
    [ A ] ([ Γ ]↔ γ₀~γ₁)           » γ₀~γ₁                            ⇒
          ([ a₀ ]≈ ([ Γ ]↔ γ₀~γ₁)) » [ A ] [ Γ ]⟳ » γ₀~γ₁             ⇒
                                           a₀~₀a₁ » ([ a₁ ]≈ (γ₀~γ₁))
  ]↑
[ Id _ _ ]≈ _ ⇒ _                 = Lttₚ

refl : {A : Ty Γ i}{a : Tm Γ A} → Tm Γ (Id a a)
¦ refl {A = A} ¦ _ = ↑[ [ A ]⟳ ]↑
[ refl ]≈ _ = Lttₚ

-- subst : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j} →
--         {γ : ¦ Γ ¦} → {a₀ a₁ : Tm Γ A} → ([ A ] [ Γ ]⟳ ⇒ ¦ a₀ ¦ γ ~ ¦ a₁ ¦ γ) →
--         (Tm Γ (B [ _,_ {A = A} id a₀ ]T)) → (Tm Γ (B [ _,_ {A = A} id a₁ ]T))
subst : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j} →
        {a₀ a₁ : Tm Γ A} → (Tm Γ (Id a₀ a₁)) →
        (Tm Γ (B [ _,_ {A = A} id a₀ ]T)) → (Tm Γ (B [ _,_ {A = A} id a₁ ]T))
¦ subst {Γ = Γ} {B = B} id b₀ ¦ γ = [ B ]↷ [ Γ ]⟳ L,ₚ ↓[ ¦ id ¦ γ ]↓ ⇒ ¦ b₀ ¦ γ
[_]≈_ (subst {Γ = Γ} {A = A} {B = B} {a₀ = a₀} {a₁ = a₁} id b₀) {γ₀} {γ₁} γ₀~γ₁ =
  [ B ] [ Γ ]⟳ L,ₚ a₁γ₀~a₀γ₀ » γ₀~γ₁ L,ₚ a₀γ₀~a₁γ₁                              ⇒
        b₀γ₀↷~b₀γ₀           » [ B ] γ₀~γ₁ L,ₚ a₀γ₀~a₀γ₁ » [ Γ ]⟳ L,ₚ a₀γ₁~a₁γ₁ ⇒
                               b₀γ₀~b₀γ₁                 » b₀γ₁~b₀γ₁↷
  where
    a₀γ₀~a₁γ₀ = ↓[ ¦ id ¦ γ₀ ]↓
    a₁γ₀~a₀γ₀ = [ A ]↔ [ Γ ]⟳ ⇒ a₀γ₀~a₁γ₀
    a₀γ₀~a₀γ₁ = [ a₀ ]≈ γ₀~γ₁
    a₀γ₁~a₁γ₁ = ↓[ ¦ id ¦ γ₁ ]↓
    -- a₁γ₁~a₀γ₁ = ↓[ ¦ id ¦ γ₁ ]↓

    a₀γ₀~a₁γ₁ = [ A ] [ Γ ]⟳ » γ₀~γ₁ ⇒ a₀γ₀~a₁γ₀ » ([ a₁ ]≈ γ₀~γ₁)

    b₀γ₀↷~b₀γ₀ = [ B ]↔ [ Γ ]⟳ L,ₚ a₀γ₀~a₁γ₀ ⇒ [ B ]≈ [ Γ ]⟳ L,ₚ a₀γ₀~a₁γ₀ ⇒ ¦ b₀ ¦ γ₀
    b₀γ₀~b₀γ₁ = [ b₀ ]≈ γ₀~γ₁
    b₀γ₁~b₀γ₁↷ = [ B ]≈ [ Γ ]⟳ L,ₚ a₀γ₁~a₁γ₁ ⇒ ¦ b₀ ¦ γ₁


{- Inductive Types -}

-- Sigma

Σ : (A : Ty Γ i) → (B : Ty (Γ ▷ A) j) → Ty Γ (i ⊔ j)
¦ Σ A B ¦ γ                               = LΣ (¦ A ¦ γ) (λ a → ¦ B ¦ (γ L, a))
[ Σ A B ] γ₀~γ₁ ⇒ (a₀ L, b₀) ~ (a₁ L, b₁) = LΣₚ ([ A ] γ₀~γ₁ ⇒ a₀ ~ a₁) λ a₀~a₁ → ([ B ] (γ₀~γ₁ L,ₚ a₀~a₁) ⇒ b₀ ~ b₁)
[ Σ A B ]⟳                                = [ A ]⟳ L,ₚ [ B ]⟳
[ Σ A B ]↔ γ₀~γ₁ ⇒ (a₀~a₁ L,ₚ b₀~b₁)      = ([ A ]↔ γ₀~γ₁ ⇒ a₀~a₁) L,ₚ ([ B ]↔ (γ₀~γ₁ L,ₚ a₀~a₁) ⇒ b₀~b₁)
[ Σ A B ] γ₀~γ₁ » γ₁~γ₂ ⇒ (a₀~a₁ L,ₚ b₀~b₁) » (a₁~a₂ L,ₚ b₁~b₂) =
  ([ A ] γ₀~γ₁ » γ₁~γ₂ ⇒ a₀~a₁ » a₁~a₂) L,ₚ (([ B ] (γ₀~γ₁ L,ₚ a₀~a₁) » (γ₁~γ₂ L,ₚ a₁~a₂) ⇒ b₀~b₁ » b₁~b₂))
[ Σ A B ]↷ γ₀~γ₁ ⇒ (a₀ L, b₀)             = ([ A ]↷ γ₀~γ₁ ⇒ a₀) L, ([ B ]↷ (γ₀~γ₁ L,ₚ ([ A ]≈ γ₀~γ₁ ⇒ a₀)) ⇒ b₀)
[ Σ A B ]≈ γ₀~γ₁ ⇒ (a₀ L, b₀)             = ([ A ]≈ γ₀~γ₁ ⇒ a₀) L,ₚ ([ B ]≈ (γ₀~γ₁ L,ₚ ([ A ]≈ γ₀~γ₁ ⇒ a₀)) ⇒ b₀)

_,Σ_ : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j} →
       (a : Tm Γ A)(b : Tm Γ (B [ _,_ {A = A} id a ]T)) → Tm Γ (Σ A B)
¦ a ,Σ b ¦ γ = ¦ a ¦ γ L, ¦ b ¦ γ
[ a ,Σ b ]≈ γ₀~γ₁ = ([ a ]≈ γ₀~γ₁) L,ₚ ([ b ]≈ γ₀~γ₁)

proj₁ : {A : Ty Γ i}{B : Ty (Γ ▷ A) j} → Tm Γ (Σ A B) → Tm Γ A
¦ proj₁ t ¦ γ = Lproj₁ (¦ t ¦ γ)
[ proj₁ t ]≈ γ₀~γ₁ = proj₁ₚ ([ t ]≈ γ₀~γ₁)

proj₂ : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j} →
        (t : Tm Γ (Σ A B)) →
        Tm Γ (B [ _,_ {A = A} id (proj₁ {B = B} t) ]T)
¦ proj₂ t ¦ γ = Lproj₂ (¦ t ¦ γ)
[ proj₂ t ]≈ γ₀~γ₁ = proj₂ₚ ([ t ]≈ γ₀~γ₁)

Σβ₁ : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j}{a : Tm Γ A}{b : Tm Γ (B [ _,_ {A = A} id a ]T)} →
      proj₁ {B = B} (_,Σ_ {B = B} a b) ≡ a
Σβ₁ = Lrefl

Σβ₂ : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j}{a : Tm Γ A}{b : Tm Γ (B [ _,_ {A = A} id a ]T)} →
      proj₂ {A = A} {B = B} (_,Σ_ {B = B} a b) ≡ b
Σβ₂ = Lrefl

Ση : {Γ : Con i}{A : Ty Γ i}{B : Ty (Γ ▷ A) j}{t : Tm Γ (Σ A B)} →
     _,Σ_ {A = A} {B = B} (proj₁ {B = B} t) (proj₂ {A = A} {B = B} t) ≡ t
Ση = Lrefl

-- Bool

Bool : Ty Γ lzero
¦ Bool ¦ γ = LBool
[ Bool ] γ₀~γ₁ ⇒ b₀ ~ b₁ = b₀ ≡ₚ b₁
[ Bool ]⟳ = Lreflₚ
[ Bool ]↔ γ₀~γ₁ ⇒ Lreflₚ = Lreflₚ
[ Bool ] γ₀~γ₁ » γ₁~γ₂ ⇒ Lreflₚ » Lreflₚ = Lreflₚ
[ Bool ]↷ γ₀~γ₁ ⇒ b₀ = b₀
[ Bool ]≈ γ₀~γ₁ ⇒ b₀ = Lreflₚ

true : Tm Γ Bool
¦ true ¦ γ = Ltrue
[ true ]≈ _ = Lreflₚ

false : Tm Γ Bool
¦ false ¦ γ = Lfalse
[ false ]≈ _ = Lreflₚ

if : {Γ : Con i}{A : Ty Γ i} → (b : Tm Γ Bool) → (Tm Γ A) → (Tm Γ A) → (Tm Γ A)
¦_¦ (if b t f) γ with ¦ b ¦ γ
... | Lfalse = ¦ f ¦ γ
... | Ltrue = ¦ t ¦ γ
[_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with ¦ b ¦ γ₀ | ¦ b ¦ γ₁ | [ b ]≈ γ₀~γ₁
... | Lfalse | .Lfalse | Lreflₚ = [ f ]≈ γ₀~γ₁
... | Ltrue | .Ltrue | Lreflₚ = [ t ]≈ γ₀~γ₁
-- ... | Lfalse | Lfalse | Lreflₚ = [ f ]≈ γ₀~γ₁
-- ... | Lfalse | Ltrue | ()
-- ... | Ltrue | Lfalse | ()
-- ... | Ltrue | Ltrue | Lreflₚ = [ t ]≈ γ₀~γ₁

-- TODO Dependent eliminator
-- if : {Γ : Con i}{C : Ty (Γ ▷ Bool) i j} →
--   (b : Tm Γ Bool) →
--   (Tm Γ (C [ _,_ {A = Bool} id true ]T)) →
--   (Tm Γ (C [ _,_ {A = Bool} id false ]T)) →
--   (Tm Γ (C [ _,_ {A = Bool} id b ]T))
-- ¦_¦ (if b t f) γ with ¦ b ¦ γ
-- ... | Lfalse = ¦ f ¦ γ
-- ... | Ltrue = ¦ t ¦ γ
-- [_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with [ b ]≈ γ₀~γ₁
-- ... | a rewrite a = {! a !}
-- [_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with ¦ b ¦ γ₁ | ¦ b ¦ γ₀
-- ... | a | c = {! a b !}
-- [_]≈_ (if b t f) {γ₀} {γ₁} γ₀~γ₁ with ¦ b ¦ γ₀
-- ... | a = {!  !}

Boolβ₁ : {Γ : Con i}{A : Ty Γ i} → {t : Tm Γ A} → {f : Tm Γ A} → if true t f ≡ t
Boolβ₁ = Lrefl

Boolβ₂ : {Γ : Con i}{A : Ty Γ i} → {t : Tm Γ A} → {f : Tm Γ A} → if false t f ≡ f
Boolβ₂ = Lrefl

{- Quotient Types -}

-- Interval

I : {Γ : Con lzero} → Ty Γ lzero
¦ I ¦ γ             = LBool
[ I ] _ ⇒ _ ~ _     = L⊤ₚ
[ I ]⟳              = Lttₚ
[ I ]↔ _ ⇒ _        = Lttₚ
[ I ] _ » _ ⇒ _ » _ = Lttₚ
[ I ]↷ _ ⇒ i        = i
[ I ]≈ _ ⇒ _        = Lttₚ

left : {Γ : Con lzero} → Tm Γ I
¦ left ¦  _ = Lfalse
[ left ]≈ _ = Lttₚ

right : {Γ : Con lzero} → Tm Γ I
¦ right ¦  _ = Ltrue
[ right ]≈ _ = Lttₚ

Ieq : {Γ : Con lzero} → Tm Γ (Id left right)
-- Ieq = _
¦ Ieq ¦  _ = ↑[ Lttₚ ]↑
[ Ieq ]≈ _ = Lttₚ

Iif : {Γ : Con lzero} {A : Ty Γ i} → Tm Γ I → (l : Tm Γ A) → (r : Tm Γ A) → Tm Γ (Id l r) → Tm Γ A
¦ Iif i l r e ¦ γ with ¦ i ¦ γ
... | Lfalse = ¦ l ¦ γ
... | Ltrue = ¦ r ¦ γ
[_]≈_ (Iif {Γ = Γ} {A = A} i l r e) {γ₀} {γ₁} γ₀~γ₁ with ¦ i ¦ γ₀ | ¦ i ¦ γ₁
... | Lfalse | Lfalse = [ l ]≈ γ₀~γ₁
... | Lfalse | Ltrue = [ A ] γ₀~γ₁ » [ Γ ]⟳ ⇒ [ l ]≈ γ₀~γ₁ » ↓[ ¦ e ¦ γ₁ ]↓
... | Ltrue | Lfalse = [ A ] γ₀~γ₁ » [ Γ ]⟳ ⇒ [ r ]≈ γ₀~γ₁ » ([ A ]↔ [ Γ ]⟳ ⇒ ↓[ ¦ e ¦ γ₁ ]↓)
... | Ltrue | Ltrue = [ r ]≈ γ₀~γ₁

-- {- Coinductive Types -}

-- Pi

Π : {Γ : Con i} → (A : Ty Γ j) → (B : Ty (Γ ▷ A) k) → Ty Γ (j ⊔ k)

¦ Π {Γ = Γ} A B ¦ γ = LΣₛₚ
  ((a : ¦ A ¦ γ) → ¦ B ¦ (γ L, a))
  (λ f → {a₀ a₁ : ¦ A ¦ γ}(a₀~a₁ : [ A ] [ Γ ]⟳ ⇒ a₀ ~ a₁) → [ B ] ([ Γ ]⟳ L,ₚ a₀~a₁) ⇒ f a₀ ~ f a₁)

[_]_⇒_~_ (Π A B) {γ₀} {γ₁} γ₀~γ₁ (f₀ L,ₛₚ f₀~) (f₁ L,ₛₚ f₁~) =
  {a₀ : ¦ A ¦ γ₀}{a₁ : ¦ A ¦ γ₁}(a₀~a₁ : [ A ] γ₀~γ₁ ⇒ a₀ ~ a₁) →
  [ B ] γ₀~γ₁ L,ₚ a₀~a₁ ⇒ f₀ a₀ ~ f₁ a₁

-- [ Π A B ]⟳ {c = c} a₀~a₁ = (proj₂ₛₚ c) a₀~a₁
[ Π A B ]⟳ {c = f L,ₛₚ f~} a₀~a₁ = f~ a₀~a₁

([ Π {Γ = Γ} A B ]↔ γ₀~γ₁ ⇒ f) a₀~a₁ = [ B ]↔ (γ₀~γ₁ L,ₚ a₁~a₀) ⇒ f a₁~a₀
  where
    a₁~a₀ = [ A ]↔ ([ Γ ]↔ γ₀~γ₁) ⇒ a₀~a₁

-- [_]_»_⇒_»_ (Π {Γ = Γ} A B) {c₀ = f₀ L,ₛₚ f₀~} {f₁ L,ₛₚ f₁~} {f₂ L,ₛₚ f₂~} γ₀~γ₁ γ₁~γ₂ f₀~f₁ f₁~f₂ {a₀ = a₀} {a₁ = a₂} a₀~a₂ = ?
([ Π {Γ = Γ} A B ] γ₀~γ₁ » γ₁~γ₂ ⇒ f₀~f₁ » f₁~f₂) {a₀ = a₀} a₀~a₂ =
  [ B ] (γ₀~γ₁ L,ₚ a₀~a₁) » (γ₁~γ₂ L,ₚ a₁~a₂) ⇒ f₀~f₁ a₀~a₁ » f₁~f₂ a₁~a₂
  where
    γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁
    γ₀~γ₂ = [ Γ ] γ₀~γ₁ » γ₁~γ₂
    a₁ = [ A ]↷ γ₀~γ₁ ⇒ a₀
    a₀~a₁ = [ A ]≈ γ₀~γ₁ ⇒ a₀
    a₁~a₂ = [ A ] γ₁~γ₀ » γ₀~γ₂ ⇒ [ A ]↔ γ₀~γ₁ ⇒ a₀~a₁ » a₀~a₂

[ Π {Γ = Γ} A B ]↷ γ₀~γ₁ ⇒ (f L,ₛₚ f~) =
  (λ a → [ B ]↷ γ₀~γ₁ L,ₚ ([ A ]↔ γ₁~γ₀ ⇒ [ A ]≈ γ₁~γ₀ ⇒ a) ⇒ f ([ A ]↷ γ₁~γ₀ ⇒ a))
    L,ₛₚ
  (λ {a₀} {a₁} a₀~a₁ → let
      ↷a₀ = [ A ]↷ γ₁~γ₀ ⇒ a₀
      ↷a₁ = [ A ]↷ γ₁~γ₀ ⇒ a₁

      a₀~↷a₀ = [ A ]≈ γ₁~γ₀ ⇒ a₀
      a₁~↷a₁ = [ A ]≈ γ₁~γ₀ ⇒ a₁

      ↷a₀~a₀ = [ A ]↔ γ₁~γ₀ ⇒ a₀~↷a₀
      ↷a₁~a₁ = [ A ]↔ γ₁~γ₀ ⇒ a₁~↷a₁

      ↷a₀~a₁ = [ A ] γ₀~γ₁ » [ Γ ]⟳ ⇒ ↷a₀~a₀ » a₀~a₁
      a₀~↷a₁ = [ A ] [ Γ ]⟳ » γ₁~γ₀ ⇒ a₀~a₁ » a₁~↷a₁
      ↷a₀~↷a₁ = [ A ] γ₀~γ₁ » γ₁~γ₀ ⇒ ↷a₀~a₀ » a₀~↷a₁

      ↷f↷a₀~f↷a₀ = [ B ]↔ γ₀~γ₁ L,ₚ ↷a₀~a₀ ⇒ [ B ]≈ γ₀~γ₁ L,ₚ ↷a₀~a₀ ⇒ f ↷a₀
      f↷a₀~f↷a₁ = f~ ↷a₀~↷a₁
      f↷a₁~↷f↷a₁ = [ B ]≈ γ₀~γ₁ L,ₚ ↷a₁~a₁ ⇒ f ↷a₁
      ↷f↷a₀~↷f↷a₁ = [ B ] γ₁~γ₀ L,ₚ a₀~↷a₀ » γ₀~γ₁ L,ₚ ↷a₀~a₁ ⇒
                    ↷f↷a₀~f↷a₀             » [ B ] [ Γ ]⟳ L,ₚ ↷a₀~↷a₁ » γ₀~γ₁ L,ₚ ↷a₁~a₁ ⇒
                                             f↷a₀~f↷a₁                 » f↷a₁~↷f↷a₁
    in
      ↷f↷a₀~↷f↷a₁
  )
  where
    γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁

([ Π {Γ = Γ} A B ]≈ γ₀~γ₁ ⇒ (f L,ₛₚ f~)) {a₀} {a₁} a₀~a₁ =
  [ B ] [ Γ ]⟳ L,ₚ a₀~↷a₁ » γ₀~γ₁ L,ₚ ↷a₁~a₁                                 ⇒
  f~ a₀~↷a₁               » [ B ]≈ (γ₀~γ₁ L,ₚ ↷a₁~a₁) ⇒ f ([ A ]↷ γ₁~γ₀ ⇒ a₁)
  where
    γ₁~γ₀ = [ Γ ]↔ γ₀~γ₁
    a₁~↷a₁ = [ A ]≈ γ₁~γ₀ ⇒ a₁
    ↷a₁~a₁ = [ A ]↔ γ₁~γ₀ ⇒ a₁~↷a₁
    a₀~↷a₁ = [ A ] γ₀~γ₁ » γ₁~γ₀ ⇒ a₀~a₁ » a₁~↷a₁

lam : {Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k} → Tm (Γ ▷ A) B → Tm Γ (Π A B)
¦ lam {Γ = Γ} t ¦ γ = (λ a → ¦ t ¦ (γ L, a)) L,ₛₚ (λ a₀~a₁ → [ t ]≈ ([ Γ ]⟳ L,ₚ a₀~a₁))
[ lam {Γ = Γ} t ]≈ γ₀~γ₁ = λ a₀~a₁ → [ t ]≈ (γ₀~γ₁ L,ₚ a₀~a₁)

app : {Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k} → Tm Γ (Π A B) → Tm (Γ ▷ A) B
¦ app t ¦ (γ L, a) = proj₁ₛₚ (¦ t ¦ γ) a
[_]≈_ (app t) {γ₀ L, a₀} {γ₁ L, a₁} (γ₀~γ₁ L,ₚ a₀~a₁) = ([ t ]≈ γ₀~γ₁) a₀~a₁

Πβ : {Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k} → {t : Tm (Γ ▷ A) B} → app {A = A} (lam {A = A} t) ≡ t
Πβ = Lrefl

Πη : {Γ : Con i}{A : Ty Γ j}{B : Ty (Γ ▷ A) k} → {t : Tm Γ (Π A B)} → lam {A = A} {B = B} (app {A = A} t) ≡ t
Πη = Lrefl

{- Universe -}

-- Not in scope of the project
